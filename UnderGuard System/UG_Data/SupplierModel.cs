﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {
    public class SupplierModel {

        //Business Objects
        private SupplierTableAdapter adapter = new SupplierTableAdapter();


        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.SupplierDataTable GetAll() {
            return this.adapter.GetAllSuppliers();
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.SupplierDataTable GetSupplierInfoByName(string fName, string lName) {
            return this.adapter.GetSupplierByName(fName, lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.SupplierDataTable GetSupplierInfoByFName(string fName) {
            return this.adapter.GetSupplierByFName(fName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.SupplierDataTable GetSupplierInfoByLName(string lName) {
            return this.adapter.GetSupplierByLName(lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.SupplierDataTable GetSupplierInfoByID(int currId) {
            return this.adapter.GetSupplierID(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int AddNewSupplier(string firstname, string lastname, string phone, string country, string provState, 
                                  string employer, string email, string zip, string notes, int ohs, int success, 
                                  int failed) {
            return this.adapter.InsertSupplier(firstname, lastname, phone, country, provState, employer, email, zip, 
                                               notes, ohs, success, failed);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int UpdateSupplierByID(int currId, string firstname, string lastname, string phone, string country, string provState, 
                                      string employer, string email, string zip, string notes, int ohs, int success, 
                                      int failed) {
            return this.adapter.UpdateASupplier(currId, firstname, lastname, phone, country, provState, employer, email, zip, notes, 
                                                ohs, success, failed);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int Delete(int currId) {
            return this.adapter.DeleteASupplier(currId);
        
        } //end function

        //=================================================



    } //EOC

} //EON

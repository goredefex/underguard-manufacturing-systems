﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data.UnderGuardDSTableAdapters;


namespace UG_Data {

    [DataObject]
    public class BuildSheetModel {

        //Sheet Objects
        BuildSheetTableAdapter adapter = new BuildSheetTableAdapter();
        
        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public int AddASheet(int material, int prod, int quant) {
            return this.adapter.InsertBuildSheet(material, prod, quant);
        
        } //end function



    } //EOC

} //EON

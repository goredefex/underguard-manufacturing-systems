﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {

    [DataObject]
    public class MaterialModel {
    
        //Business Objects
        private MaterialTableAdapter adapter = new MaterialTableAdapter();


        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.MaterialDataTable GetAll() {
            return this.adapter.GetAllMaterials();
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.MaterialDataTable GetMaterialByID(int currId) {
            return this.adapter.GetMaterial(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.MaterialDataTable GrabMaterialByName(string currName) {
            return this.adapter.GetMaterialByName(currName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.MaterialDataTable GrabMaterialByVendor(int vendorId) {
            return this.adapter.GetMaterialByVendor(vendorId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int Delete(int currId) {
            return this.adapter.DeleteMaterial(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int AddNewMaterial(int vendorId, string name, string desc, int cost,
                                  int count, int weight, string color, string partNum) {

            return this.adapter.InsertMaterial(vendorId, name, desc, cost, count, 
                                               weight, color, partNum);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int UpdateMaterialByID(int currId, int vendorId, string name, string desc, 
                                      int cost, int count, int weight, string color, 
                                      string partNum) {

            return this.adapter.UpdateMaterial(currId, vendorId, name, desc, cost, count, 
                                               weight, color, partNum);
        
        } //end function

        //=================================================

    
    } //EOC

} //EON

﻿using System;

namespace UG_Data {

    public partial class UnderGuardDS {

        //public partial class PersonalInfoDataTable {}  //end partial class

        /// <summary>
        /// Row Object That Contains Methods To 
        /// Find PersonalInformation Queries
        /// </summary>
        public partial class PersonalInfoRow {


            /// <summary>
            /// Takes All Fields In Row To Insert
            /// </summary>
            /// <param name="firstname"></param>
            /// <param name="lastname"></param>
            /// <param name="phone"></param>
            /// <param name="country"></param>
            /// <param name="provState"></param>
            /// <param name="employer"></param>
            /// <param name="email"></param>
            /// <param name="zip"></param>
            /// <param name="notes"></param>
            public int InsertPerson(string firstname, string lastname, string phone, 
                                     string country, string provState, string employer, 
                                     string email, string zip, string notes) {

                //Instantiate Adapter
                UnderGuardDSTableAdapters.PersonalInfoTableAdapter adapter = 
                    new UG_Data.UnderGuardDSTableAdapters.PersonalInfoTableAdapter();
                return adapter.AddPersonalInfoRow(firstname, lastname, phone, country, provState, employer, email, zip, notes);
            
            } //end function


            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            public PersonalInfoDataTable GetInfoById() {
                //Instantiate Adapter
                UnderGuardDSTableAdapters.PersonalInfoTableAdapter adapter = 
                    new UG_Data.UnderGuardDSTableAdapters.PersonalInfoTableAdapter();

                return adapter.GetInfoById(this.info_id);            
            
            } //end function
            

            /// <summary>
            /// Searches Procedure For All Personal Info Objects
            /// </summary>
            /// <returns>PersonalInfoDataTable Object</returns>
            public PersonalInfoDataTable GetAllPeople() {
                //Instantiate Adapter
                UnderGuardDSTableAdapters.PersonalInfoTableAdapter adapter = 
                    new UG_Data.UnderGuardDSTableAdapters.PersonalInfoTableAdapter();

                return adapter.GetAllPeople();

            } //end function        
        
        } //end partial class
        

        public partial class MaterialRow {
            
            /// <summary>
            /// Searches Procedure For All Material Objects
            /// </summary>
            /// <returns>MaterialDataTable Object of Materials</returns>
            public MaterialDataTable GetMaterials() {
                //Instantiate Adapter
                UnderGuardDSTableAdapters.MaterialTableAdapter adapter = 
                    new UG_Data.UnderGuardDSTableAdapters.MaterialTableAdapter();

                return adapter.GetAllMaterials();
            
            } //end function
        
        } //end partial class



        partial class OrderLineItemsRow {
        

        
        } //end partial class
    
    
    } // end partial class
            

} //EON

﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {

    [DataObject]
    public class OrderModel {

        //Order Adapter
        private OrdersTableAdapter adapter = new OrdersTableAdapter();


        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.OrdersDataTable GetAll() {
            return this.adapter.GetAllOrders();
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.OrdersDataTable GetAnOrderById(int currId) {
            return this.adapter.GetOrderByID(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.OrdersDataTable GetAnOrderByPersonalId(int currId) {
            return this.adapter.GetOrderByPersonID(currId);
        
        } //end function

        //==============================================



    } //EOC

} //EON

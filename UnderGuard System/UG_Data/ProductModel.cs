﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {

    [DataObject]
    public class ProductModel {

        //Modelling Objects
        private ProductTableAdapter adapter = new ProductTableAdapter();

        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        //READ
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.ProductDataTable GetAll() {
            return this.adapter.GetAllProducts();
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ProductDataTable GrabProductById(int currId) {
            return this.adapter.GetProductById(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ProductDataTable GrabProductByName(string currName) {
            return this.adapter.GetProductByName(currName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int GetLastID() {
            return Convert.ToInt32(this.adapter.LastIdentity());
        
        } //end function


        //REMOVE
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public int Delete(int currId) {
            return this.adapter.DeleteAProduct(currId);
        
        } //end function


        //CREATE
        [DataObjectMethod(DataObjectMethodType.Insert, false)]
        public int AddNewProduct(string desc, string safety, string name, int weight, int price, 
                              string modelNum, string color, string zones, string img) {
            
            return this.adapter.InsertProduct(desc, safety, name, weight, price, modelNum, 
                                              color, zones, img);
        
        } //end function


        //UPDATE
        [DataObjectMethod(DataObjectMethodType.Update, false)]
        public int UpdateAProduct(int currId, string desc, string safety, string name, int weight, 
                                  int price, string modelNum, string color, string zones, string img) {
            
            return this.adapter.UpdateProduct(currId, desc, safety, name, weight, price, modelNum, 
                                              color, zones, img);
        
        } //end function

        //=================================================


    } //EOC

} //EON

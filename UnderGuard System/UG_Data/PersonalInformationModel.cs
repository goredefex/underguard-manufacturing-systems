﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data;
using UG_Data.UnderGuardDSTableAdapters;


namespace UG_Data {
    
    [DataObject]
    public class PersonalInformationModel {


        //Business Objects
        private PersonalInfoTableAdapter adapter = new PersonalInfoTableAdapter();


        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.PersonalInfoDataTable GetAllPeopleObjs() {
            return this.adapter.GetAllPeople();      
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.PersonalInfoDataTable GetInfo(int id) {
            return this.adapter.GetInfoById(id);   
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.PersonalInfoDataTable GetInfoByName(string fName, string lName) {
            return this.adapter.GetPersonByName(fName, lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.PersonalInfoDataTable GetInfoByFName(string fName) {
            return this.adapter.GetPersonByFName(fName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.PersonalInfoDataTable GetInfoByLName(string lName) {
            return this.adapter.GetPersonByLName(lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.PersonalInfoDataTable GetAllByProcedure() {
            return this.adapter.GetAllPeopleProcedure();   
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int InsertPersonProcedure(string firstname, string lastname, string phone, 
                                     string country, string provState, string employer, 
                                     string email, string zip, string notes) {
            return  this.adapter.AddPersonalInfoRow(firstname, lastname, phone, country, provState, employer, email, zip, notes);

        } //end function

        //=================================================



    } //EOC

} //EON

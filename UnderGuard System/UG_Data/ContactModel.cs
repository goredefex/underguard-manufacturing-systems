﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {

    [DataObject]
    public class ContactModel {


        //Adapter Objects
        private ContactTableAdapter adapter = new ContactTableAdapter();


        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ContactDataTable GetContactInfoByName(string fName, string lName) {
            return this.adapter.GetContactByName(fName, lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ContactDataTable GetContactInfoByFName(string fName) {
            return this.adapter.GetContactByFName(fName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ContactDataTable GetContactInfoByLName(string lName) {
            return this.adapter.GetContactByLName(lName);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.ContactDataTable GetContactInfoByID(int currId) {
            return this.adapter.GetContactByID(currId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.ContactDataTable GetAll() {
            return this.adapter.GetAllContacts();
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int AddNewContact(string firstname, string lastname, string phone, string country, string provState, 
                                 string employer, string email, string zip, string notes, byte compCont, int handlerId,
                                 byte pending) {
            
            return this.adapter.InsertContact(firstname, lastname, phone, country, provState, employer, email, zip, 
                                              notes, compCont, handlerId, pending);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int UpdateContactByID(int currId, string fName, string lName, string phNum, string country,
                                     string provState, string employer, string email, string zip, string notes,
                                     byte companyContact, byte ordersPending, int handlerId) {

            return this.adapter.UpdateAContact(currId, fName, lName, phNum, country, provState, employer, 
                                               email, zip, notes, companyContact, ordersPending, handlerId);
        
        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public int Delete(int currId) {
            return this.adapter.DeleteAContact(currId);
        
        } //end function

        //=================================================



    } //EOC

} //EON

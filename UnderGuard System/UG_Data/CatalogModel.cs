﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data.UnderGuardDSTableAdapters;

namespace UG_Data {

    [DataObject]
    public class CatalogModel {


        //Adapter Objects
        private CatalogPagesTableAdapter adapter = new CatalogPagesTableAdapter();
        

        //=================================================
        //DATA OBJ'S --------------------------------------
        //=================================================

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public UnderGuardDS.CatalogPagesDataTable GetAllPages() {
            return this.adapter.GetAllPages();

        } //end function

        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public UnderGuardDS.CatalogPagesDataTable GetAPageByID(int currId) {
            return this.adapter.GetPageByID(currId);

        } //end function

        [DataObjectMethod(DataObjectMethodType.Insert, false)]
        public int AddAPage(int currId, int location, int catNum) {
            return this.adapter.InsertCatalogPage(currId, location, catNum);

        } //end function

        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public int Delete(int currId) {
            return this.adapter.DeleteProductCatalogPage(currId);

        } //end function

        //=================================================


    } //EOC

} //EON

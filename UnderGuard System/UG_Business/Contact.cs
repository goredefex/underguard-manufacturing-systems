﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {
    public class Contact : PersonalInformation {
        
        //Const Column Names
        private const string CONT_ID_COLUMN = "contact_id";
        private const string COMPANY_COLUMN = "contact_companyContact";
        private const string HANDLERID_COLUMN = "contact_companyHandlerId";
        private const string ORDERS_COLUMN = "contact_ordersPending";
        private const string PERSONID_COLUMN = "contact_infoId";

        //Contact Members
        private string contactId = String.Empty;
        private string ordersPending = String.Empty;
        private string isCompanyHandler = String.Empty;
        private string companyHandlerId = String.Empty;
        private string personId = String.Empty;

        //Error Handles
        string currError = String.Empty;

        //Data Control Object
        ContactModel model = new ContactModel();


        //============================================
        //Constructs ---------------------------------
        //============================================
        
        /// <summary>
        /// Filler Class For Insert/Update
        /// </summary>
        public Contact() { this.Worked = false; } //<--Allow Empty


        /// <summary>
        /// Takes A Persons Name
        /// </summary>
        /// <param name="personId">Integer - Persons First/Last Name</param>
        public Contact(string currFirstName, string currLastName) {
            
            //Instantiate
            UnderGuardDS.ContactDataTable p = 
                new UnderGuardDS.ContactDataTable();
            //--

            //Attempt...
            try {
                if(currFirstName.CompareTo(String.Empty)!=0 && currLastName.CompareTo(String.Empty)!=0) {
                    p = model.GetContactInfoByName(currFirstName, currLastName);
                } else if(currFirstName.CompareTo(String.Empty)==0) {
                    p = model.GetContactInfoByLName(currLastName);
                } else if(currLastName.CompareTo(String.Empty)==0) {
                    p = model.GetContactInfoByFName(currFirstName);
                }
                this.UseTableToFillPersonData(p);
                this.UseTableToFillContactInfo(p);
                this.Worked = true;

            //If not, Send To Error Handling
            } catch(Exception e) { this.currError = e.StackTrace; }
        
        } //end constructor


        /// <summary>
        /// Takes A Person Id
        /// </summary>
        /// <param name="personId">Integer - Persons Id Number</param>
        public Contact(int personId) {
            
            //Instantiate
            UnderGuardDS.ContactDataTable p = 
                new UnderGuardDS.ContactDataTable();
            //--

            //Attempt...
            try {
                //Get Entered Person
                p = this.model.GetContactInfoByID(personId);
                this.UseTableToFillPersonData(p);
                this.UseTableToFillContactInfo(p);
                this.Worked = true;
            
            //If not, Send To Error Handling
            } catch(Exception e) { this.currError = e.StackTrace; }


        } //end constructor

        //============================================
        


        //============================================
        //Fields -------------------------------------
        //============================================
        
        /// <summary>
        /// Gets/Sets Handler Flag Var
        /// </summary>
        public string IsCompanyHandler { get { return this.isCompanyHandler; } 
                                         set { this.isCompanyHandler=value; } } //end field


        /// <summary>
        /// Gets/Sets Company Handler ID Num
        /// </summary>
        public string CompanyHandlerID { get { return this.companyHandlerId; } 
                                      set { this.companyHandlerId=value; } } //end field

        /// <summary>
        /// Gets/Sets Contact Id Var
        /// </summary>
        public string ContactID { get { return this.contactId; } 
                                  set { this.contactId=value; } } //end field


        /// <summary>
        /// Gets/Sets Orders Pending Var
        /// </summary>
        public string OrdersPending { get { return this.ordersPending; } 
                                      set { this.ordersPending=value; } } //end field


        /// <summary>
        /// Gets/Sets Handler Flag Var
        /// </summary>
        public string PersonID { get { return this.personId; } 
                                 set { this.personId=value; } } //end field

        //============================================


        //============================================
        //Functions ----------------------------------
        //============================================

        public List<Contact> GetAllContactsList() {
            UnderGuardDS.ContactDataTable p = this.model.GetAll();
            List<Contact> currFound = new List<Contact>();
            for(int i=0; i<p.Count; i++) {
                Contact c = new Contact();
                c.ID = p[i].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString();
                c.FName = p[i].ItemArray[p.Columns.IndexOf(FNAME_COLUMN)].ToString();
                c.LName = p[i].ItemArray[p.Columns.IndexOf(LNAME_COLUMN)].ToString();
                c.PhNumber = p[i].ItemArray[p.Columns.IndexOf(PHNUM_COLUMN)].ToString();
                c.Country = p[i].ItemArray[p.Columns.IndexOf(COUNTRY_COLUMN)].ToString();
                c.ProvState = p[i].ItemArray[p.Columns.IndexOf(PROVSTATE_COLUMN)].ToString();
                c.Employer = p[i].ItemArray[p.Columns.IndexOf(EMPLOYER_COLUMN)].ToString();
                c.Email = p[i].ItemArray[p.Columns.IndexOf(EMAIL_COLUMN)].ToString();
                c.PostZip = p[i].ItemArray[p.Columns.IndexOf(ZIP_COLUMN)].ToString();
                c.Notes = p[i].ItemArray[p.Columns.IndexOf(NOTES_COLUMN)].ToString();
                c.IsCompanyHandler = p[i].ItemArray[p.Columns.IndexOf(COMPANY_COLUMN)].ToString();
                c.CompanyHandlerID = p[i].ItemArray[p.Columns.IndexOf(HANDLERID_COLUMN)].ToString();
                c.OrdersPending = p[i].ItemArray[p.Columns.IndexOf(ORDERS_COLUMN)].ToString();
                c.ContactID = p[i].ItemArray[p.Columns.IndexOf(CONT_ID_COLUMN)].ToString();
                currFound.Add(c);
            }
        
            return currFound;

        } //end function



        /// <summary>
        /// Takes A Given ContactDataTable
        /// and Enters Its Info To Member Vars
        /// </summary>
        /// <param name="p">ContactDataTable</param>
        public void UseTableToFillContactInfo(UnderGuardDS.ContactDataTable p) {
            //Table To Grab From            
            this.contactId = p[0].ItemArray[p.Columns.IndexOf(CONT_ID_COLUMN)].ToString();
            this.ordersPending = p[0].ItemArray[p.Columns.IndexOf(ORDERS_COLUMN)].ToString();
            this.isCompanyHandler = p[0].ItemArray[p.Columns.IndexOf(COMPANY_COLUMN)].ToString();
            this.companyHandlerId = p[0].ItemArray[p.Columns.IndexOf(HANDLERID_COLUMN)].ToString();
            this.personId = p[0].ItemArray[p.Columns.IndexOf(PERSONID_COLUMN)].ToString();
        
        } //end function



        /// <summary>
        /// 
        /// </summary>
        /// <param name="compCont"></param>
        /// <param name="handlerId"></param>
        /// <param name="pending"></param>
        /// <param name="pId"></param>
        /// <returns></returns>
        public int AddContact() {
            return this.model.AddNewContact(this.FName, this.LName, this.PhNumber, this.Country, this.ProvState, this.Employer, 
                                            this.Email, this.PostZip, this.Notes, byte.Parse(this.isCompanyHandler), 
                                            Int32.Parse(this.companyHandlerId), byte.Parse(this.ordersPending));

        } //end function
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="fName"></param>
        /// <param name="lName"></param>
        /// <param name="phNum"></param>
        /// <param name="country"></param>
        /// <param name="provState"></param>
        /// <param name="employer"></param>
        /// <param name="email"></param>
        /// <param name="zip"></param>
        /// <param name="notes"></param>
        /// <param name="companyContact"></param>
        /// <param name="ordersPending"></param>
        /// <param name="handlerId"></param>
        /// <returns></returns>
        public int UpdateAContactById(int currId, string fName, string lName, string phNum, string country,
                                      string provState, string employer, string email, string zip, string notes,
                                      byte companyContact, byte ordersPending, int handlerId) {
        
            return this.model.UpdateContactByID(currId, fName, lName, phNum, country, provState, employer, 
                                                email, zip, notes, companyContact, ordersPending, handlerId);
        
        } //end function


        /// <summary>
        /// Deletes Contact Given
        /// </summary>
        /// <param name="currId"></param>
        /// <returns></returns>
        public int DeleteContactByID(int currId) {
            return this.model.Delete(currId);
        
        } //end function

        //============================================




    } //EOC

} //EON

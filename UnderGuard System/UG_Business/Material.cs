﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {
    public class Material {
        
        //Const Column Names
        private string MAT_ID_COLUMN = "material_id";
        private string VENDOR_COLUMN = "material_vendorId";
        private string NAME_COLUMN = "material_name";
        private string DESC_COLUMN = "material_description";
        private string COST_COLUMN = "material_cost";
        private string INV_COLUMN = "material_inventoryCount";
        private string WEIGHT_COLUMN = "material_weightKgs";
        private string COLOR_COLUMN = "material_color";
        private string PARTNUM_COLUMN = "material_vendorPartNum";


        //Material Vars
        private int materialId = -1;
        private int vendorId = -1;
        private string name = String.Empty;
        private string description = String.Empty;
        private int cost = -1;
        private int count = -1;
        private string color = String.Empty;
        private int weight = -1;
        private string partNum = String.Empty;
        private bool worked = false;


        //Error Handles
        string currError = String.Empty;
        

        //Business Objects
        private MaterialModel model = new MaterialModel();


        //=================================================
        //Constructs --------------------------------------
        //=================================================
        
        public Material() { this.worked = false; } //<--Allow Empty


        /// <summary>
        /// Creates A Material Object
        /// Based on Name Result Query
        /// </summary>
        /// <param name="name"></param>
        public Material(string name) {
            
            //Instantiate
            UnderGuardDS.MaterialDataTable m 
                = new UnderGuardDS.MaterialDataTable();
            //--

            try {
                //Get Entered Material
                m = this.model.GrabMaterialByName(name);
                this.UseTableToFillMembers(m);
                this.Worked = true;
                
            //If not, Send To Error Handling
            } catch(Exception ex) { this.currError="Error Log: "+ex.StackTrace; }
        
        
        } //end construct


        /// <summary>
        /// Creates A Material Object
        /// Based on Id Result Query
        /// </summary>
        /// <param name="currId"></param>
        public Material(int currId) {
        
            //Instantiate
            UnderGuardDS.MaterialDataTable m 
                = new UnderGuardDS.MaterialDataTable();
            //--

            try {
                //Get Entered Material
                m = this.model.GetMaterialByID(currId);
                this.UseTableToFillMembers(m);
                this.Worked = true;
                
            //If not, Send To Error Handling
            } catch(Exception ex) { this.currError="Error Log: "+ex.StackTrace; }
        
        } //end constructor

        //=================================================




        //=================================================
        //Fields ------------------------------------------
        //=================================================

        /// <summary>
        /// Sets/Gets Material ID
        /// </summary>
        public int MaterialId { get { return materialId; }
                                set { materialId = value; } } //end field

        /// <summary>
        /// Gets/Sets The Vendor ID Number
        /// </summary>
        public int VendorId { get { return vendorId; }
                              set { vendorId = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Material Name
        /// </summary>
        public string Name { get { return name; }
                             set { name = value; } } //end field

        /// <summary>
        /// Gets/Sets The Material Description
        /// </summary>
        public string Description { get { return description; }
                                    set { description = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Material Cost
        /// </summary>
        public int Cost { get { return cost; }
                          set { cost = value; } } //end field

        /// <summary>
        /// Gets/Sets The Material Inventory Count
        /// </summary>
        public int Count { get { return count; }
                           set { count = value; } } //end field

        /// <summary>
        /// Gets/Sets The Material Weight in Kgs
        /// </summary>
        public int Weight { get { return weight; }
                            set { weight = value; } } //end field

        /// <summary>
        /// Gets/Sets The Material Color
        /// </summary>
        public string Color { get { return color; }
                              set { color = value; } } //end field

        /// <summary>
        /// Gets/Sets The Vendor Part Number
        /// </summary>
        public string PartNum { get { return partNum; }
                                set { partNum = value; } } //end field

        /// <summary>
        /// Gets/Sets The Class Worked Status
        /// </summary>
        public bool Worked { get { return worked; }
                             set { worked = value; } } //end field

        //=================================================




        //=================================================
        //Functions ---------------------------------------
        //=================================================
        
        public List<Material> GetAllMaterialsObjs() {
            UnderGuardDS.MaterialDataTable m = this.model.GetAll();
            List<Material> foundMaterials = new List<Material>();

            for(int i=0; i<m.Count; i++) {
                Material ma = new Material();
                ma.materialId = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(MAT_ID_COLUMN)].ToString());
                ma.vendorId = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(VENDOR_COLUMN)].ToString());
                ma.name = m[i].ItemArray[m.Columns.IndexOf(NAME_COLUMN)].ToString();
                ma.description = m[i].ItemArray[m.Columns.IndexOf(DESC_COLUMN)].ToString();
                ma.cost = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(COST_COLUMN)].ToString());
                ma.count = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(INV_COLUMN)].ToString());
                ma.weight = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(WEIGHT_COLUMN)].ToString());
                ma.color = m[i].ItemArray[m.Columns.IndexOf(COLOR_COLUMN)].ToString();
                ma.partNum = m[i].ItemArray[m.Columns.IndexOf(PARTNUM_COLUMN)].ToString();
                foundMaterials.Add(ma);
            }

            return foundMaterials;
        
        } //end function


        public List<Material> GetAllMaterialsByVendorId(int currId) {
            UnderGuardDS.MaterialDataTable m = this.model.GrabMaterialByVendor(currId);
            List<Material> foundMaterials = new List<Material>();

            for(int i=0; i<m.Count; i++) {
                Material ma = new Material();
                ma.materialId = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(MAT_ID_COLUMN)].ToString());
                ma.vendorId = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(VENDOR_COLUMN)].ToString());
                ma.name = m[i].ItemArray[m.Columns.IndexOf(NAME_COLUMN)].ToString();
                ma.description = m[i].ItemArray[m.Columns.IndexOf(DESC_COLUMN)].ToString();
                ma.cost = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(COST_COLUMN)].ToString());
                ma.count = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(INV_COLUMN)].ToString());
                ma.weight = Int32.Parse(m[i].ItemArray[m.Columns.IndexOf(WEIGHT_COLUMN)].ToString());
                ma.color = m[i].ItemArray[m.Columns.IndexOf(COLOR_COLUMN)].ToString();
                ma.partNum = m[i].ItemArray[m.Columns.IndexOf(PARTNUM_COLUMN)].ToString();
                foundMaterials.Add(ma);
            }

            return foundMaterials;
        
        } // end function


        /// <summary>
        /// Takes A Given Material Data Table
        /// & Fills All Properties
        /// </summary>
        /// <param name="m"></param>
        private void UseTableToFillMembers(UnderGuardDS.MaterialDataTable m) {
            this.materialId = Int32.Parse(m[0].ItemArray[m.Columns.IndexOf(MAT_ID_COLUMN)].ToString());
            this.vendorId = Int32.Parse(m[0].ItemArray[m.Columns.IndexOf(VENDOR_COLUMN)].ToString());
            this.name = m[0].ItemArray[m.Columns.IndexOf(NAME_COLUMN)].ToString();
            this.description = m[0].ItemArray[m.Columns.IndexOf(DESC_COLUMN)].ToString();
            this.cost = Int32.Parse(m[0].ItemArray[m.Columns.IndexOf(COST_COLUMN)].ToString());
            this.count = Int32.Parse(m[0].ItemArray[m.Columns.IndexOf(INV_COLUMN)].ToString());
            this.weight = Int32.Parse(m[0].ItemArray[m.Columns.IndexOf(WEIGHT_COLUMN)].ToString());
            this.color = m[0].ItemArray[m.Columns.IndexOf(COLOR_COLUMN)].ToString();
            this.partNum = m[0].ItemArray[m.Columns.IndexOf(PARTNUM_COLUMN)].ToString();
        
        } //end function


        /// <summary>
        /// Update A Material By Given ID
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="vendorId"></param>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <param name="cost"></param>
        /// <param name="count"></param>
        /// <param name="weight"></param>
        /// <param name="color"></param>
        /// <param name="partNum"></param>
        /// <returns></returns>
        public int UpdateMaterial(int currId, int vendorId, string name, string desc, int cost, 
                                  int count, int weight, string color, string partNum) {
            
            return this.model.UpdateMaterialByID(currId, vendorId, name, desc, cost, count, 
                                                 weight, color, partNum);
        
        } //end function


        /// <summary>
        /// Adds Current Material To DBAS
        /// </summary>
        /// <returns></returns>
        public int AddMaterial() {
            return this.model.AddNewMaterial(this.VendorId, this.Name, this.Description, this.Cost, 
                                             this.Count, this.Weight, this.Color, this.PartNum);
        
        } //end function


        /// <summary>
        /// Deletes A Material By Current ID
        /// </summary>
        /// <param name="currId"></param>
        /// <returns></returns>
        public int DeleteMaterial(int currId) { return this.model.Delete(currId); } //end function

        //=================================================




    } //EOC

} //EON

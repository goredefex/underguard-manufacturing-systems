﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {
    public class Product {
        
        //Cont Column Names
        private const string PROD_ID_COLUMN = "product_id";
        private const string DESC_COLUMN = "product_description";
        private const string SAFETY_COLUMN = "product_safetyCode";
        private const string NAME_COLUMN = "product_name";
        private const string WEIGHT_COLUMN = "product_totalWeight";
        private const string PRICE_COLUMN = "product_retailPrice";
        private const string MODELNUM_COLUMN = "product_modelNumber";
        private const string COLOR_COLUMN = "product_color";
        private const string ZONES_COLUMN = "product_shippingZones";
        private const string IMG_COLUMN = "product_imgPath";

        //Product Properties
        private int id = -1;
        private string description = String.Empty;
        private string safetyCode = String.Empty;
        private string name = String.Empty;
        private int weight = -1;
        private int price = -1;
        private string modelNum = String.Empty;
        private string color = String.Empty;
        private string zones = String.Empty;
        private string img = String.Empty;
        private bool worked = false;
        
        //Error Handling
        private string currError = String.Empty;

        //Product Objects
        private ProductModel model = new ProductModel();
        private BuildSheet[] sheets;
        
        
    
        //============================================
        //Constructs ---------------------------------
        //============================================

        public Product() { this.worked=false; } //<--Allow Empty

        /// <summary>
        /// Takes A Product ID For Query/Building
        /// </summary>
        /// <param name="currId"></param>
        public Product(int currId) {
        
            //Instantiate
            UnderGuardDS.ProductDataTable pD 
                = new UnderGuardDS.ProductDataTable();
            //--

            try {
                this.id = currId;
                pD = this.model.GrabProductById(currId);
                this.UseTableToFillMembers(pD);
                this.worked = true;

            } catch(Exception ee) { this.currError=ee.StackTrace; this.worked=false; }
        
        } //end constructor

        /// <summary>
        /// Takes A Product Name For Query/Building
        /// </summary>
        /// <param name="currName"></param>
        public Product(string currName) {
            
            //Instantiate
            UnderGuardDS.ProductDataTable pD 
                = new UnderGuardDS.ProductDataTable();
            //--

            try {
                pD = this.model.GrabProductByName(currName);
                this.UseTableToFillMembers(pD);
                this.worked = true;

            } catch { this.currError="Could Not Create Class"; this.worked=false; }
        
        } //end constructor

        //============================================



        //============================================
        //Fields -------------------------------------
        //============================================

        /// <summary>
        /// Gets/Sets The Product ID
        /// </summary>
        public int Id { get { return id; }
                        set { id = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Product Description
        /// </summary>
        public string Description { get { return description; }
                                    set { description = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Product Safety Code
        /// </summary>
        public string SafetyCode { get { return safetyCode; }
                                   set { safetyCode = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Name
        /// </summary>
        public string Name { get { return name; }
                             set { name = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Product Weight
        /// </summary>
        public int Weight { get { return weight; }
                            set { weight = value; } } //end field

        /// <summary>
        /// Gets/Sets The Prodct Price
        /// </summary>
        public int Price { get { return price; }
                           set { price = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Model Num
        /// </summary>
        public string ModelNum { get { return modelNum; }
                                 set { modelNum = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Color
        /// </summary>
        public string Color { get { return color; }
                              set { color = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Shipping Zones
        /// </summary>
        public string Zones { get { return zones; }
                              set { zones = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Img Path
        /// </summary>
        public string Img { get { return img; }
                            set { img = value; } } //end field

        /// <summary>
        /// Gets/Sets The Class Worked Status
        /// </summary>
        public bool Worked { get { return worked; }
                             set { worked = value; } } //end field

        /// <summary>
        /// Gets/Sets The Build Sheets
        /// </summary>
        public BuildSheet[] Sheets { get { return sheets; }
                                     set { sheets = value; } } //end field

        //============================================




        //============================================
        //Functions ----------------------------------
        //============================================
        
        /// <summary>
        /// Finds Last Used Row Identity
        /// </summary>
        /// <returns></returns>
        public int LastID() { return this.model.GetLastID(); } //end function


        /// <summary>
        /// Returns A list of all Products in DBAS
        /// </summary>
        /// <returns></returns>
        public List<Product> GetAllProductsList() {
            //Gets Products
            UnderGuardDS.ProductDataTable p = this.model.GetAll();
            List<Product> foundProducts = new List<Product>();
            
            for(int i=0; i<p.Count; i++) {
                Product pp = 
                    new Product(Int32.Parse(p[i].ItemArray[
                        p.Columns.IndexOf(PROD_ID_COLUMN)].ToString()));
                foundProducts.Add(pp);
            }
                       
            return foundProducts;
                    
        } //end function

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        public void UseCatalogToFillMembers(UnderGuardDS.CatalogPagesDataTable p) {
            this.Id = Int32.Parse(p[0].ItemArray[p.Columns.IndexOf(PROD_ID_COLUMN)].ToString());
            this.Description = p[0].ItemArray[p.Columns.IndexOf(DESC_COLUMN)].ToString();
            this.SafetyCode = p[0].ItemArray[p.Columns.IndexOf(SAFETY_COLUMN)].ToString();
            this.Name = p[0].ItemArray[p.Columns.IndexOf(NAME_COLUMN)].ToString();
            this.Weight = Int32.Parse(p[0].ItemArray[p.Columns.IndexOf(WEIGHT_COLUMN)].ToString());
            this.Price = Int32.Parse(p[0].ItemArray[p.Columns.IndexOf(PRICE_COLUMN)].ToString());
            this.ModelNum = p[0].ItemArray[p.Columns.IndexOf(MODELNUM_COLUMN)].ToString();
            this.Color = p[0].ItemArray[p.Columns.IndexOf(COLOR_COLUMN)].ToString();
            this.Zones = p[0].ItemArray[p.Columns.IndexOf(ZONES_COLUMN)].ToString();
            this.Img = p[0].ItemArray[p.Columns.IndexOf(IMG_COLUMN)].ToString();
        
        } //end function


        /// <summary>
        /// Takes A Given Product Data Table &
        /// Fills All Member Variables
        /// </summary>
        /// <param name="p"></param>
        public void UseTableToFillMembers(UnderGuardDS.ProductDataTable p) {
            this.Description = p[0].ItemArray[p.Columns.IndexOf(DESC_COLUMN)].ToString();
            this.SafetyCode = p[0].ItemArray[p.Columns.IndexOf(SAFETY_COLUMN)].ToString();
            this.Name = p[0].ItemArray[p.Columns.IndexOf(NAME_COLUMN)].ToString();
            this.Weight = Int32.Parse(p[0].ItemArray[p.Columns.IndexOf(WEIGHT_COLUMN)].ToString());
            this.Price = Int32.Parse(p[0].ItemArray[p.Columns.IndexOf(PRICE_COLUMN)].ToString());
            this.ModelNum = p[0].ItemArray[p.Columns.IndexOf(MODELNUM_COLUMN)].ToString();
            this.Color = p[0].ItemArray[p.Columns.IndexOf(COLOR_COLUMN)].ToString();
            this.Zones = p[0].ItemArray[p.Columns.IndexOf(ZONES_COLUMN)].ToString();
            this.Img = p[0].ItemArray[p.Columns.IndexOf(IMG_COLUMN)].ToString();
            
            //Build Sheets
            this.Sheets = new BuildSheet[p.Count]; //<--Create Sheets
            for(int i=0; i<p.Count; i++) {
                BuildSheet b = new BuildSheet();
                b.SheetID = Int32.Parse(p[i].ItemArray[p.Columns.IndexOf(
                    b.SHEETID_COL)].ToString());
                b.MaterialID = Int32.Parse(p[i].ItemArray[p.Columns.IndexOf(
                    b.MATID_COLU)].ToString());
                b.ProductID = Int32.Parse(p[i].ItemArray[p.Columns.IndexOf(
                    b.PRODID_COL)].ToString());
                b.Quantity = Int32.Parse(p[i].ItemArray[p.Columns.IndexOf(
                    b.QUANT_COL)].ToString());
                this.Sheets[i] = b;
            }
        
        } //end function


        /// <summary>
        /// Uses Member Vars To Insert A New Product
        /// </summary>
        /// <returns></returns>
        public int AddProduct() {
            return this.model.AddNewProduct(this.Description, this.SafetyCode, this.Name, 
                                            this.Weight, this.Price, this.ModelNum, this.Color, 
                                            this.Zones, this.Img);

        } //end function


        /// <summary>
        /// Updates A Product By Given Product ID
        /// </summary>
        /// <param name="currId"></param>
        /// <param name="desc"></param>
        /// <param name="safety"></param>
        /// <param name="name"></param>
        /// <param name="weight"></param>
        /// <param name="price"></param>
        /// <param name="modelNum"></param>
        /// <param name="color"></param>
        /// <param name="zones"></param>
        /// <param name="img"></param>
        /// <returns></returns>
        public int UpdateProduct(int currId, string desc, string safety, string name, int weight, 
                                 int price, string modelNum, string color, string zones, string img) {
            
            return this.model.UpdateAProduct(currId, desc, safety, name, weight, price, modelNum, 
                                             color, zones, img);
        
        } //end function


        /// <summary>
        /// Delets A Product By Given Product ID
        /// </summary>
        /// <param name="currId"></param>
        /// <returns></returns>
        public int DeleteProduct(int currId) {
            return this.model.Delete(currId);
        
        } //end function

        //============================================





    } //EOC

} //EON

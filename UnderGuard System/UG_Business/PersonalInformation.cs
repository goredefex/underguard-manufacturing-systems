﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Data;
using UG_Data;

namespace UG_Business {
    public class PersonalInformation {

        //Column Name Constants
        public const string ID_COLUMN = "info_id";
        public const string FNAME_COLUMN = "info_firstname";
        public const string LNAME_COLUMN = "info_lastname";
        public const string PHNUM_COLUMN = "info_phonenum";
        public const string COUNTRY_COLUMN = "info_country";
        public const string PROVSTATE_COLUMN = "info_provstate";
        public const string EMPLOYER_COLUMN = "info_employer";
        public const string EMAIL_COLUMN = "info_email";
        public const string ZIP_COLUMN = "info_zip";
        public const string NOTES_COLUMN = "info_notes";

        //Member Vars
        private string personalId = String.Empty;
        private string firstName = String.Empty;
        private string lastName = String.Empty;
        private string phoneNum = String.Empty;
        private string country = String.Empty;
        private string provState = String.Empty;
        private string employer = String.Empty;
        private string email = String.Empty;
        private string postZip = String.Empty;
        private string notes = String.Empty;

        //Data Control Object
        private PersonalInformationModel model = new PersonalInformationModel();

        //Control Vars
        private bool worked = false;
                

        //============================================
        //Fields -------------------------------------
        //============================================

        /// <summary>
        /// Gets/Sets Person Id Num
        /// </summary>
        public string ID { get { return personalId; } 
                           set { personalId=value; } }

        /// <summary>
        /// Gets/Sets First Name
        /// </summary>
        public string FName { get { return firstName; } 
                              set { firstName=value; } }

        /// <summary>
        /// Gets/Sets Last name
        /// </summary>
        public string LName { get { return lastName; } 
                              set { lastName=value; } }

        /// <summary>
        /// Gets/Sets Phone Number
        /// </summary>
        public string PhNumber { get { return phoneNum; } 
                                 set { phoneNum=value; } }

        /// <summary>
        /// Gets/Sets Country
        /// </summary>
        public string Country { get { return country; } 
                                set { country=value; } }

        /// <summary>
        /// Gets/Sets Province or State Name
        /// </summary>
        public string ProvState { get { return provState; } 
                                  set { provState=value; } }

        /// <summary>
        /// Gets/Sets Employer If Any
        /// </summary>
        public string Employer { get { return employer; } 
                                 set { employer=value; } }

        /// <summary>
        /// Gets/Sets Email If Any
        /// </summary>
        public string Email { get { return email; } 
                              set { email=value; } } 

        /// <summary>
        /// Gets/Sets Postal Code or Zip Code
        /// </summary>
        public string PostZip { get { return postZip; } 
                                set { postZip=value; } }

        /// <summary>
        /// Gets/Sets Related Comments On File
        /// </summary>
        public string Notes { get { return notes; } 
                              set { notes=value; } }

        /// <summary>
        /// Gets/Sets The Worked Status
        /// </summary>
        public bool Worked { get { return this.worked; } 
                             set { this.worked=value; } } //end field



        //============================================



        //============================================
        //Actions ------------------------------------
        //============================================

        /// <summary>
        /// Takes Order Table and Fills Member Vars
        /// </summary>
        /// <param name="p">ContactDataTable</param>
        public void UseTableToFillPersonData(UnderGuardDS.OrdersDataTable p) {
            //Fill Members
            this.personalId = p[0].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString();
            this.firstName = p[0].ItemArray[p.Columns.IndexOf(FNAME_COLUMN)].ToString();
            this.lastName = p[0].ItemArray[p.Columns.IndexOf(LNAME_COLUMN)].ToString();
            this.phoneNum = p[0].ItemArray[p.Columns.IndexOf(PHNUM_COLUMN)].ToString();
            this.country = p[0].ItemArray[p.Columns.IndexOf(COUNTRY_COLUMN)].ToString();
            this.provState = p[0].ItemArray[p.Columns.IndexOf(PROVSTATE_COLUMN)].ToString();
            this.employer = p[0].ItemArray[p.Columns.IndexOf(EMPLOYER_COLUMN)].ToString();
            this.email = p[0].ItemArray[p.Columns.IndexOf(EMAIL_COLUMN)].ToString();
            this.postZip = p[0].ItemArray[p.Columns.IndexOf(ZIP_COLUMN)].ToString();
            this.notes = p[0].ItemArray[p.Columns.IndexOf(NOTES_COLUMN)].ToString();
        } //end function


        /// <summary>
        /// Takes Contact Table and Fills Member Vars
        /// </summary>
        /// <param name="p">ContactDataTable</param>
        public void UseTableToFillPersonData(UnderGuardDS.PersonalInfoDataTable p) {
            //Fill Members
            this.personalId = p[0].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString();
            this.firstName = p[0].ItemArray[p.Columns.IndexOf(FNAME_COLUMN)].ToString();
            this.lastName = p[0].ItemArray[p.Columns.IndexOf(LNAME_COLUMN)].ToString();
            this.phoneNum = p[0].ItemArray[p.Columns.IndexOf(PHNUM_COLUMN)].ToString();
            this.country = p[0].ItemArray[p.Columns.IndexOf(COUNTRY_COLUMN)].ToString();
            this.provState = p[0].ItemArray[p.Columns.IndexOf(PROVSTATE_COLUMN)].ToString();
            this.employer = p[0].ItemArray[p.Columns.IndexOf(EMPLOYER_COLUMN)].ToString();
            this.email = p[0].ItemArray[p.Columns.IndexOf(EMAIL_COLUMN)].ToString();
            this.postZip = p[0].ItemArray[p.Columns.IndexOf(ZIP_COLUMN)].ToString();
            this.notes = p[0].ItemArray[p.Columns.IndexOf(NOTES_COLUMN)].ToString();
        } //end function


        /// <summary>
        /// Takes Contact Table and Fills Member Vars
        /// </summary>
        /// <param name="p">ContactDataTable</param>
        public void UseTableToFillPersonData(UnderGuardDS.ContactDataTable p) {
            //Fill Members
            this.personalId = p[0].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString();
            this.firstName = p[0].ItemArray[p.Columns.IndexOf(FNAME_COLUMN)].ToString();
            this.lastName = p[0].ItemArray[p.Columns.IndexOf(LNAME_COLUMN)].ToString();
            this.phoneNum = p[0].ItemArray[p.Columns.IndexOf(PHNUM_COLUMN)].ToString();
            this.country = p[0].ItemArray[p.Columns.IndexOf(COUNTRY_COLUMN)].ToString();
            this.provState = p[0].ItemArray[p.Columns.IndexOf(PROVSTATE_COLUMN)].ToString();
            this.employer = p[0].ItemArray[p.Columns.IndexOf(EMPLOYER_COLUMN)].ToString();
            this.email = p[0].ItemArray[p.Columns.IndexOf(EMAIL_COLUMN)].ToString();
            this.postZip = p[0].ItemArray[p.Columns.IndexOf(ZIP_COLUMN)].ToString();
            this.notes = p[0].ItemArray[p.Columns.IndexOf(NOTES_COLUMN)].ToString();
        } //end function


        /// <summary>
        /// Takes Supplier Table and Fills Member Vars
        /// </summary>
        /// <param name="p">SupplierDataTable</param>
        public void UseTableToFillPersonData(UnderGuardDS.SupplierDataTable p) {
            //Fill Members
            this.personalId = p[0].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString();
            this.firstName = p[0].ItemArray[p.Columns.IndexOf(FNAME_COLUMN)].ToString();
            this.lastName = p[0].ItemArray[p.Columns.IndexOf(LNAME_COLUMN)].ToString();
            this.phoneNum = p[0].ItemArray[p.Columns.IndexOf(PHNUM_COLUMN)].ToString();
            this.country = p[0].ItemArray[p.Columns.IndexOf(COUNTRY_COLUMN)].ToString();
            this.provState = p[0].ItemArray[p.Columns.IndexOf(PROVSTATE_COLUMN)].ToString();
            this.employer = p[0].ItemArray[p.Columns.IndexOf(EMPLOYER_COLUMN)].ToString();
            this.email = p[0].ItemArray[p.Columns.IndexOf(EMAIL_COLUMN)].ToString();
            this.postZip = p[0].ItemArray[p.Columns.IndexOf(ZIP_COLUMN)].ToString();
            this.notes = p[0].ItemArray[p.Columns.IndexOf(NOTES_COLUMN)].ToString();
        } //end function


        /// <summary>
        /// Person Object Insertion
        /// </summary>
        /// <returns></returns>
        public int AddPersonToDB() {
            return this.model.InsertPersonProcedure(this.firstName, this.lastName, this.phoneNum, 
                                                    this.country, this.provState, this.employer, 
                                                    this.email, this.postZip, this.notes);

        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PersonalInformationModel GetInfoModel() { return this.model; } //end function

        
        //============================================


    } //EOC

} //EON

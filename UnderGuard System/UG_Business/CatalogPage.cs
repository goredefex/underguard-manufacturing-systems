﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UG_Data;

namespace UG_Business {
    public class CatalogPage {


        //Const Column Names
        private const string CAT_ID_COLUMN = "page_id";
        private const string PROD_ID_COLUMN = "page_productId";
        private const string LOC_COLUMN = "page_placementLocation";
        private const string CATNUM_COLUMN = "page_catalogNum";

        //Catalog Members
        private int pageId = -1;
        private int productId = -1;
        private int location = 0;
        private int catalogNum = 1;
        private bool worked = false;
        
        //Error Reporting
        private string currError = String.Empty;
        
        //Modelling Objects
        private CatalogModel model = new CatalogModel();
        private Product prod = new Product();
               


        //============================================
        //Constructs ---------------------------------
        //============================================
        public CatalogPage() {} //<--Allow Empty
        
        public CatalogPage(int currId) {
            
            //Instantiate
            UnderGuardDS.CatalogPagesDataTable c = 
                new UnderGuardDS.CatalogPagesDataTable();
            //--

            try {
                
                c = this.model.GetAPageByID(currId);
                this.FillMembersUsingTable(c);

            } catch { this.currError = "Could Not Find Page"; }
        
        } //end constructor

        //============================================

        

        //============================================
        //Functions ----------------------------------
        //============================================

        /// <summary>
        /// Returns A List Of All Catalog Page Items
        /// </summary>
        /// <returns></returns>
        public List<CatalogPage> GetAllPagesList() {
            UnderGuardDS.CatalogPagesDataTable c = 
                this.model.GetAllPages();
            List<CatalogPage> foundPages = new List<CatalogPage>();

            for(int i=0; i<c.Count; i++) {
                CatalogPage cc = new CatalogPage(
                    Int32.Parse(c[i].ItemArray[
                        c.Columns.IndexOf(CAT_ID_COLUMN)].ToString()));
                foundPages.Add(cc);
            }

            return foundPages;

        } //end function


        /// <summary>
        /// Deletes A Page by Given ID
        /// </summary>
        /// <param name="currId"></param>
        /// <returns></returns>
        public int DeletePage(int currId) {
            return this.model.Delete(currId);
        
        } //end function


        /// <summary>
        /// Uses Member Vars To Insert Page To DBAS
        /// </summary>
        /// <param name="prod"></param>
        /// <param name="loc"></param>
        /// <param name="catNum"></param>
        /// <returns></returns>
        public int AddPage(int prod, int loc, int catNum) {
            return this.model.AddAPage(prod, loc, catNum);
        
        } //end function


        /// <summary>
        /// Fills Member Vars With Given Table
        /// </summary>
        /// <param name="c"></param>
        public void FillMembersUsingTable(UnderGuardDS.CatalogPagesDataTable c) {
            this.pageId = Int32.Parse(c[0].ItemArray[c.Columns.IndexOf(CAT_ID_COLUMN)].ToString()); 
            this.productId = Int32.Parse(c[0].ItemArray[c.Columns.IndexOf(PROD_ID_COLUMN)].ToString()); 
            this.location = Int32.Parse(c[0].ItemArray[c.Columns.IndexOf(LOC_COLUMN)].ToString()); 
            this.catalogNum = Int32.Parse(c[0].ItemArray[c.Columns.IndexOf(CATNUM_COLUMN)].ToString());
            this.prod.UseCatalogToFillMembers(c);

        } //end function

        //============================================



        //============================================
        //Fields -------------------------------------
        //============================================
        
        /// <summary>
        /// Gets/Sets The Catalog Page Id
        /// </summary>
        public int PageId { get { return pageId; }
                            set { pageId = value; } } //end field

        /// <summary>
        /// Gets/Sets The Page Product Id
        /// </summary>
        public int ProductId { get { return productId; }
                               set { productId = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Page Product Location
        /// </summary>
        public int Location { get { return location; }
                              set { location = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Catalog That Page Belongs To
        /// </summary>
        public int CatalogNum { get { return catalogNum; }
                                set { catalogNum = value; } } //end field

        /// <summary>
        /// Gets/Sets The Class Worked Status
        /// </summary>
        public bool Worked { get { return worked; }
                             set { worked = value; } } //end field

        /// <summary>
        /// Gets/Sets The Product Within Catalog Page
        /// </summary>
        public Product Prod { get { return prod; }
                              set { prod = value; } } //end field

        //============================================


    } //EOC

} //EON

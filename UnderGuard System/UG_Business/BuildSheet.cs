﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {
    public class BuildSheet {
        
        
        //Const Column Names
        private const string SHEET_ID_COLUMN = "sheet_id";
        private const string MAT_ID_COLUMN = "sheet_materialId"; 
        private const string PROD_ID_COLUMN = "sheet_productId";
        private const string QUANT_COLUMN = "sheet_quantity";

        //Build Sheet Properties
        private int sheetID = -1;
        private int materialID = -1;
        private int productID = -1;
        private int quantity = -1;
        
        //Data/Business Obj's
        private BuildSheetModel model = new BuildSheetModel();

        public BuildSheet() {}

        //=================================================
        //Fields ------------------------------------------
        //=================================================

        /// <summary>
        /// Gets/Sets The Sheets's ID
        /// </summary>
        public int SheetID { get { return this.sheetID; } 
                             set { sheetID=value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Material's ID
        /// </summary>
        public int MaterialID { get { return this.materialID; } 
                                set { materialID=value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Material Amount
        /// </summary>
        public int Quantity { get { return this.quantity; } 
                              set { quantity=value; } } //end field

        /// <summary>
        /// Gets/Sets The Product's ID
        /// </summary>
        public int ProductID { get { return this.productID; } 
                               set { productID=value; } } //end field

        /// <summary>
        /// Gets sheet_id column
        /// </summary>
        public string SHEETID_COL { get { return SHEET_ID_COLUMN; } } //end field

        /// <summary>
        /// Gets The sheet_materialId Column
        /// </summary>
        public string MATID_COLU { get { return MAT_ID_COLUMN; } } //end field

        /// <summary>
        /// Gets The sheet_productId Column
        /// </summary>
        public string PRODID_COL { get { return PROD_ID_COLUMN; } } //end field

        /// <summary>
        /// Gets The sheet_quantity Column
        /// </summary>
        public string QUANT_COL { get { return QUANT_COLUMN; } } //end field 
        
        //=================================================


        /// <summary>
        /// Adds A Sheet & Returns Status
        /// </summary>
        /// <returns></returns>
        public int AddSheet() {
            return this.model.AddASheet(this.materialID, this.productID, this.quantity);
        
        } //end function

        
    } //EOC

} //EON

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {
    public class Order {
           
        //Const Column Names
        private const string ORDER_ID_COLUMN = "order_id";
        private const string SENDER_COLUMN = "order_sender";
        private const string RECEIVER_COLUMN = "order_receiver";
        private const string DATE_COLUMN = "order_dateOfOrder";
        private const string TOTAL_COLUMN = "order_netTotal";
        private const string PRETAX_COLUMN = "order_totalPreTax";
        private const string COMPLETE_COLUMN = "order_orderComplete";
        private const string PAYMENT_COLUMN = "order_methodPayment";
        private const string SHIPPING_COLUMN = "order_shippingFees";
        private const string TAXPERC_COLUMN = "order_applicableTaxPercentage";
        private const string OWNER_COLUMN = "order_owner";

        //Order Members
        private int orderId = -1;
        private string sender = String.Empty;
        private string receiver = String.Empty;
        private DateTime date = new DateTime();
        private int total = -1;
        private int preTax = -1;
        private bool complete = false;
        private string payment = String.Empty;
        private int shippingFees = -1;
        private int taxPercent = -1;
        private int ownerId = -1;
        private bool worked = false;        

        //Error Handling
        public string currError = String.Empty;
        
        //Order Modelling
        private OrderModel model = new OrderModel();
        private PersonalInformation person = new PersonalInformation();
               


        //==============================================
        //Constructs -----------------------------------
        //==============================================

        /// <summary>
        /// Empty Construction
        /// </summary>
        public Order() { this.worked = false; } //<--Allow Empty

        /// <summary>
        /// Takes A Given Order ID
        /// </summary>
        /// <param name="currId"></param>
        public Order(int currId) {
            
            //Instantiate
            UnderGuardDS.OrdersDataTable o = 
                new UnderGuardDS.OrdersDataTable();
            //--

            try {
                o = this.model.GetAnOrderByPersonalId(currId);
                this.FillMembersUsingTable(o);
                this.worked = true;
            
            } catch { this.currError = "Could Not Find Order"; }
        
        } //end constructor

        //==============================================




        //==============================================
        //Fields ---------------------------------------
        //==============================================

        /// <summary>
        /// Gets/Sets The Orders ID
        /// </summary>
        public int OrderId { get { return orderId; }
                             set { orderId = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Sender
        /// </summary>
        public string Sender { get { return sender; }
                               set { sender = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Receiver
        /// </summary>
        public string Receiver { get { return receiver; }
                                 set { receiver = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Date
        /// </summary>
        public DateTime Date { get { return date; }
                               set { date = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Total
        /// </summary>
        public int Total { get { return total; }
                           set { total = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Pre Tax Value
        /// </summary>
        public int PreTax { get { return preTax; }
                            set { preTax = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Completion Status
        /// </summary>
        public bool Complete { get { return complete; }
                               set { complete = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Payment Method
        /// </summary>
        public string Payment { get { return payment; }
                                set { payment = value; } } //end field

        /// <summary>
        /// Gets/Sets The Orders Shipping Fees
        /// </summary>
        public int ShippingFees { get { return shippingFees; }
                                  set { shippingFees = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Tax Percentage
        /// </summary>
        public int TaxPercent { get { return taxPercent; }
                                set { taxPercent = value; } } //end field
        
        /// <summary>
        /// Gets/Sets The Orders Owner Id
        /// </summary>
        public int OwnerId { get { return ownerId; }
                             set { ownerId = value; } } //end field

        /// <summary>
        /// Gets/Sets The Class Worked Status
        /// </summary>
        public bool Worked { get { return worked; }
                             set { worked = value; } } //end field

        /// <summary>
        /// Gets/Sets The Person Object
        /// </summary>
        public PersonalInformation Person { get { return person; }
                                            set { person = value; } } //end field

        //==============================================




        //==============================================
        //Functions ------------------------------------
        //==============================================

        public List<Order> GetAllOrdersList() {
            //Get Orders
            UnderGuardDS.OrdersDataTable o = this.model.GetAll();
            List<Order> foundOrders = new List<Order>();
            
            for(int i=0; i<o.Count; i++) {
                Order ord = new Order(Int32.Parse(o[0].ItemArray[
                    o.Columns.IndexOf(ORDER_ID_COLUMN)].ToString()));
                foundOrders.Add(ord);
            }

            return foundOrders;
        
        } //end function


        /// <summary>
        /// Fills Members From Given Table
        /// </summary>
        /// <param name="o"></param>
        public void FillMembersUsingTable(UnderGuardDS.OrdersDataTable o) {
            this.orderId = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(ORDER_ID_COLUMN)].ToString());
            this.sender = o[0].ItemArray[o.Columns.IndexOf(SENDER_COLUMN)].ToString();
            this.receiver = o[0].ItemArray[o.Columns.IndexOf(RECEIVER_COLUMN)].ToString();
            this.date = Convert.ToDateTime(o[0].ItemArray[o.Columns.IndexOf(DATE_COLUMN)].ToString());
            this.total = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(TOTAL_COLUMN)].ToString());
            this.preTax = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(PRETAX_COLUMN)].ToString());
            this.payment = o[0].ItemArray[o.Columns.IndexOf(PAYMENT_COLUMN)].ToString();
            this.shippingFees = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(SHIPPING_COLUMN)].ToString());
            this.taxPercent = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(TAXPERC_COLUMN)].ToString());
            this.ownerId = Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(OWNER_COLUMN)].ToString());
            if(Int32.Parse(o[0].ItemArray[o.Columns.IndexOf(COMPLETE_COLUMN)].ToString())==1) {
                this.complete = true;
            
            } else { this.complete = false; }
            this.Person.UseTableToFillPersonData(o);
        
        } //end function

        //==============================================

        
    } //EOC

} //EON

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UG_Data;

namespace UG_Business {

    public class Supplier : PersonalInformation {
        
        //Const Column Names
        private const string SUP_ID_COLUMN = "supplier_id";
        private const string OHS_COLUMN = "supplier_OHS";
        private const string SUCCESS_COLUMN = "supplier_successfulDeliveries";
        private const string FAILED_COLUMN = "supplier_failedDeliveries";

        //Supplier Controls
        private string successfulDeliveries = "0";
        private string failedDeliveries = "0";
        private string ohsLevel = String.Empty;
        private string supplierId = string.Empty;
                     
        //Error Handles
        string currError = String.Empty;
       
        //Data Control Object
        SupplierModel model = new SupplierModel();


        //============================================
        //Constructs ---------------------------------
        //============================================
        
        /// <summary>
        /// Filler Class For Insert/Update
        /// </summary>
        public Supplier() { this.Worked = false; } //<--Allow Empty


        /// <summary>
        /// Takes A Persons Name
        /// </summary>
        /// <param name="personId">Integer - Persons ID</param>
        public Supplier(string currFirstName, string currLastName) {
            
            //Instantiate
            UnderGuardDS.SupplierDataTable p = 
                new UnderGuardDS.SupplierDataTable();
            //--

            //Attempt...
            try {
                if(currFirstName.CompareTo(String.Empty)!=0 && currLastName.CompareTo(String.Empty)!=0) {
                    p = model.GetSupplierInfoByName(currFirstName, currLastName);
                } else if(currFirstName.CompareTo(String.Empty)==0) {
                    p = model.GetSupplierInfoByLName(currLastName);
                } else if(currLastName.CompareTo(String.Empty)==0) {
                    p = model.GetSupplierInfoByFName(currFirstName);
                }
                this.UseTableToFillPersonData(p);
                this.Worked = true;

            //If not, Send To Error Handling
            } catch(Exception e) { this.currError = e.StackTrace; }
        
        } //end constructor


        /// <summary>
        /// Takes A Person Id
        /// </summary>
        /// <param name="personId">Integer - Persons Id Number</param>
        public Supplier(int personId) {
            
            //Instantiate
            UnderGuardDS.SupplierDataTable p = 
                new UnderGuardDS.SupplierDataTable();
            //--

            //Attempt...
            try {
                //Get Entered Person
                p = this.model.GetSupplierInfoByID(personId);
                this.UseTableToFillPersonData(p);
                this.UseTableToFillSupplierData(p);
                this.Worked = true;
            
            //If not, Send To Error Handling
            } catch(Exception e) { this.currError = e.StackTrace; }


        } //end constructor

        //============================================
        


        //============================================
        //Fields -------------------------------------
        //============================================

        /// <summary>
        /// Gets/Sets Successfull Delivery Count
        /// </summary>
        public string SuccessfulDeliveries { get { return successfulDeliveries; }
                                             set { successfulDeliveries = value; } } //end field

        /// <summary>
        /// Gets/Sets Failed Delivery Count
        /// </summary>
        public string FailedDeliveries { get { return failedDeliveries; }
                                         set { failedDeliveries = value; } } //end field

        /// <summary>
        /// Gets/Sets OHS Level
        /// </summary>
        public string OhsLevel { get { return ohsLevel; }
                                 set { ohsLevel = value; } } //end field
        
        /// <summary>
        /// Gets/Sets Supplier ID Num
        /// </summary>
        public string SupplierId { get { return supplierId; }
                                   set { supplierId = value; } } //end field
        
        //============================================




        //============================================
        //Functions ----------------------------------
        //============================================

        /// <summary>
        /// Serializes & Returns A List Of All Suppliers
        /// </summary>
        /// <returns></returns>
        public List<Supplier> GetAllSuppliersList() {
            UnderGuardDS.SupplierDataTable p = this.model.GetAll();
            List<Supplier> currFound = new List<Supplier>();

            for(int i=0; i<p.Count; i++) {
                Supplier s = new Supplier(Int32.Parse(p[i].ItemArray[p.Columns.IndexOf(ID_COLUMN)].ToString()));
                currFound.Add(s);
            }
        
            return currFound;

        } //end function


        /// <summary>
        /// Fills Member Vars From Given Supplier Table
        /// </summary>
        /// <param name="p"></param>
        public void UseTableToFillSupplierData(UnderGuardDS.SupplierDataTable p) {
            this.supplierId = p[0].ItemArray[p.Columns.IndexOf(SUP_ID_COLUMN)].ToString();
            this.ohsLevel = p[0].ItemArray[p.Columns.IndexOf(OHS_COLUMN)].ToString();
            this.successfulDeliveries = p[0].ItemArray[p.Columns.IndexOf(SUP_ID_COLUMN)].ToString();
            this.failedDeliveries = p[0].ItemArray[p.Columns.IndexOf(SUP_ID_COLUMN)].ToString();
        
        } //end function


        /// <summary>
        /// Updates A Contact By Given ID
        /// </summary>
        /// <returns></returns>
        public int UpdateASupplierByID(int currId, string firstname, string lastname, string phone, string country, string provState, 
                                       string employer, string email, string zip, string notes, int ohs, int success, 
                                       int failed) {

            return this.model.UpdateSupplierByID(currId, firstname, lastname, phone, country, provState, employer, email, zip, 
                                                 notes, ohs, success, failed);
        
        } //end function


        /// <summary>
        /// Inserts A Supplier To DBAS
        /// </summary>
        /// <returns></returns>
        public int AddSupplier() {
            return this.model.AddNewSupplier(this.FName, this.LName, this.PhNumber, this.Country, this.ProvState, this.Employer, 
                                             this.Email, this.PostZip, this.Notes, Int32.Parse(this.OhsLevel), 
                                             Int32.Parse(this.SuccessfulDeliveries), Int32.Parse(this.FailedDeliveries));

        } //end function


        /// <summary>
        /// Deletes A Supplier By Given ID
        /// </summary>
        /// <param name="currID"></param>
        /// <returns></returns>
        public int DeleteSupplierByID(int currID) {
            return this.model.Delete(currID);
        
        } //end function

        //============================================



    } //EOC

} //EON

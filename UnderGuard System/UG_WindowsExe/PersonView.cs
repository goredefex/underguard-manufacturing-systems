﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class PersonView : Form {

        
        //Members
        private PersonalInformation pp;
        private Contact cp;
        private Supplier sp;
        

        //====================================================
        //Constructs -----------------------------------------
        //====================================================

        public PersonView() {
            InitializeComponent();

        } //end construct

        //====================================================




        //====================================================
        //Events ---------------------------------------------
        //====================================================

                
        /// <summary>
        /// sends Id To Stored Procedure Through
        /// Modeling Classes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idSearchButton_Click(object sender, EventArgs e) {
            //Contact / Default
            if(searchTypeComboBox.SelectedIndex==-1 || searchTypeComboBox.SelectedIndex==0) {
                if(idInputBox.Text.CompareTo("")!=0) {
                    
                    //Instantiate
                    this.cp = new Contact(Convert.ToInt32(idInputBox.Text));
                    //--

                    if(this.cp.Worked) {
                        searchResultsListBox.Items.Clear(); //<--Clean Box
                        searchResultsListBox.Items.Add("ID: " + cp.ID);
                        searchResultsListBox.Items.Add("Name: " + cp.FName + " " + cp.LName);
                        searchResultsListBox.Items.Add("Phone: " + cp.PhNumber);
                        searchResultsListBox.Items.Add("Location: " + cp.Country + ", " + cp.ProvState);
                        searchResultsListBox.Items.Add("Email: " + cp.Email);
                        searchResultsListBox.Items.Add("Employer: " + cp.Employer);
                        searchResultsListBox.Items.Add("Notes: " + cp.Notes);
                                        
                        if(cp.IsCompanyHandler.CompareTo("1")==0) { searchResultsListBox.Items.Add("Handler Num: " + cp.CompanyHandlerID); } 
                        else { searchResultsListBox.Items.Add("Not A Handler"); }

                        searchFNameInputBox.Clear(); //<--Remove FName Input
                        searchLNameInputBox.Clear(); //<--Remove LName Input

                    } else { searchResultsListBox.Items.Clear(); this.cp=null; }
            
                } else { 
                    searchResultsListBox.Items.Clear(); 
                    MessageBox.Show("Please Enter A Valid ID Number First!"); 
                    this.cp=null;
                }
            
            //Supplier
            } else if(searchTypeComboBox.SelectedIndex==1) {
                if(idInputBox.Text.CompareTo("")!=0) {

                    //Instantiate
                    this.sp = new Supplier(Convert.ToInt32(idInputBox.Text));
                    //--

                    if(this.sp.Worked) {
                        searchResultsListBox.Items.Clear(); //<--Clean Box
                        searchResultsListBox.Items.Add("ID: " + sp.ID);
                        searchResultsListBox.Items.Add("Name: " + sp.FName + " " + sp.LName);
                        searchResultsListBox.Items.Add("Phone: " + sp.PhNumber);
                        searchResultsListBox.Items.Add("Location: " + sp.Country + ", " + sp.ProvState);
                        searchResultsListBox.Items.Add("Email: " + sp.Email);
                        searchResultsListBox.Items.Add("Employer: " + sp.Employer);
                        searchResultsListBox.Items.Add("Notes: " + sp.Notes);

                        searchFNameInputBox.Clear(); //<--Remove FName Input
                        searchLNameInputBox.Clear(); //<--Remove LName Input

                    } else { searchResultsListBox.Items.Clear(); this.sp=null; }
            
                } else { 
                    searchResultsListBox.Items.Clear(); //<--Clean Box
                    MessageBox.Show("Please Enter A Valid ID Number First!"); 
                    this.sp=null;
                }
            
            }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nameSearchButton_Click(object sender, EventArgs e) {
            //Contact / Default
            if(searchTypeComboBox.SelectedIndex==-1 || searchTypeComboBox.SelectedIndex==0) {
                //Validate
                if(searchFNameInputBox.Text.CompareTo("")==0 && searchLNameInputBox.Text.CompareTo("")==0) {
                    searchResultsListBox.Items.Clear(); //<--Clean Box
                    MessageBox.Show("Please Enter A Valid Name First!");
                
                } else { 
                    //Prep Names
                    string[] names = {String.Empty, String.Empty};

                    if(searchFNameInputBox.Text.CompareTo("")==0) { names[1] = searchLNameInputBox.Text; }
                    if(searchLNameInputBox.Text.CompareTo("")==0) { names[0] = searchFNameInputBox.Text; }
                    if(searchFNameInputBox.Text.CompareTo("")!=0 && searchLNameInputBox.Text.CompareTo("")!=0) {
                        names[0] = searchFNameInputBox.Text;
                        names[1] = searchLNameInputBox.Text;
                    }

                    //Instantiate
                    this.cp = new Contact(names[0], names[1]);
                    //--

                    if(this.cp.Worked) {
                        searchResultsListBox.Items.Clear(); //<--Clean Box
                        searchResultsListBox.Items.Add("ID: " + cp.ID);
                        searchResultsListBox.Items.Add("Name: " + cp.FName + " " + cp.LName);
                        searchResultsListBox.Items.Add("Phone: " + cp.PhNumber);
                        searchResultsListBox.Items.Add("Location: " + cp.Country + ", " + cp.ProvState);
                        searchResultsListBox.Items.Add("Email: " + cp.Email);
                        searchResultsListBox.Items.Add("Employer: " + cp.Employer);
                        searchResultsListBox.Items.Add("Notes: " + cp.Notes);
                                        
                        if(cp.IsCompanyHandler.CompareTo("1")==0) { searchResultsListBox.Items.Add("Handler Num: " + cp.CompanyHandlerID); } 
                        else { searchResultsListBox.Items.Add("Not A Handler"); }

                        idInputBox.Clear(); //<--Remove Id Input

                    } else { searchResultsListBox.Items.Clear(); this.cp=null; }
                }
            
            //Supplier
            } else if(searchTypeComboBox.SelectedIndex==1) {
                if(searchFNameInputBox.Text.CompareTo("")==0 && searchLNameInputBox.Text.CompareTo("")==0) {
                    MessageBox.Show("Please Enter A Valid Name First!");
                
                } else { 
                    string[] names = {String.Empty, String.Empty};

                    if(searchFNameInputBox.Text.CompareTo("")==0) { names[1] = searchLNameInputBox.Text; }
                    if(searchLNameInputBox.Text.CompareTo("")==0) { names[0] = searchFNameInputBox.Text; }
                    if(searchFNameInputBox.Text.CompareTo("")!=0 && searchLNameInputBox.Text.CompareTo("")!=0) {
                        names[0] = searchFNameInputBox.Text;
                        names[1] = searchLNameInputBox.Text;
                    }

                    //Make Person Obj
                    this.sp = new Supplier(names[0], names[1]);

                    if(this.sp.Worked) {
                        searchResultsListBox.Items.Clear(); //<--Clean Box
                        searchResultsListBox.Items.Add("ID: " + sp.ID);
                        searchResultsListBox.Items.Add("Name: " + sp.FName + " " + sp.LName);
                        searchResultsListBox.Items.Add("Phone: " + sp.PhNumber);
                        searchResultsListBox.Items.Add("Location: " + sp.Country + ", " + sp.ProvState);
                        searchResultsListBox.Items.Add("Email: " + sp.Email);
                        searchResultsListBox.Items.Add("Employer: " + sp.Employer);
                        searchResultsListBox.Items.Add("Notes: " + sp.Notes);
                    
                        idInputBox.Clear(); //<--Remove Id Input

                    } else { searchResultsListBox.Items.Clear(); this.sp=null; }
            
                }
            
            }

        } //end event
        

        /// <summary>
        /// Handles The Insertion Of A Person To The DBAS
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addContactButton_Click(object sender, EventArgs e) {
            
            bool valid = true;
            string msg = "You Are Missing: ";

            //Validation
            if(fNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| First Name |"; }
            if(lNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Last Name |"; }
            if(phNumInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Phone Num |"; }
            if(countryInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Country |"; }
            if(provStateInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Province/State |"; }
            if(employerInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Employer |"; }
            if(emailInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Email |"; }
            if(zipInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Post/Zip |"; }
            if(notesInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Notes |"; }

            if(contactHandlerCheckBox.Checked && handlerIDInputBox.Text.CompareTo("")==0) { 
                valid=false; msg+="| Company ID# |"; 
            }

            //ADD CONTACT
            if(valid) {
                //Instantiate
                this.cp = new Contact();
                //--

                //Update Person Info
                this.cp.FName = fNameInputBox.Text;
                this.cp.LName = lNameInputBox.Text;
                this.cp.PhNumber = phNumInputBox.Text;
                this.cp.Country = countryInputBox.Text;
                this.cp.ProvState = provStateInputBox.Text;
                this.cp.Employer = employerInputBox.Text;
                this.cp.Email = emailInputBox.Text;
                this.cp.PostZip = zipInputBox.Text;
                this.cp.Notes = notesInputBox.Text;

                //Update Contact Info
                if(orderPendingCheckBox.Checked) { this.cp.OrdersPending = "1"; } 
                else { this.cp.OrdersPending = "0"; }

                if(contactHandlerCheckBox.Checked) { this.cp.IsCompanyHandler = "1"; }
                else { this.cp.IsCompanyHandler = "0"; }

                this.cp.CompanyHandlerID = handlerIDInputBox.Text;

                //INSERT CONTACT!
                int ok = this.cp.AddContact();
                //--

                if(ok<0) {
                    fNameInputBox.Clear();
                    lNameInputBox.Clear();
                    phNumInputBox.Clear();
                    countryInputBox.Clear();
                    provStateInputBox.Clear();
                    employerInputBox.Clear();
                    emailInputBox.Clear();
                    zipInputBox.Clear();
                    notesInputBox.Clear();
                    handlerIDInputBox.Clear();
                    orderPendingCheckBox.Checked = false;
                    contactHandlerCheckBox.Checked = false;
                    MessageBox.Show("Contact Added!"); //<--UI MSG

                } else { MessageBox.Show("Contact Could Not Be Successfully Added... "+
                                            "Check Information Entered Is Correct"); }
                     
            } else {
                msg += " Fields, Please Enter And Continue";
                MessageBox.Show(msg);
            }
        
        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addSupplierButton_Click(object sender, EventArgs e) {

            bool valid = true;
            string msg = "You Are Missing: ";

            //Validation
            if(fNameSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| First Name |"; }
            if(lNameSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Last Name |"; }
            if(phNumSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Phone Num |"; }
            if(countrySupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Country |"; }
            if(provStateSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Province/State |"; }
            if(employerSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Employer |"; }
            if(emailSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Email |"; }
            if(zipSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Post/Zip |"; }
            if(commentsSupplierInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Notes |"; }
            if(OHSInputBox.Text.CompareTo("")==0) { valid=false; msg+="| OHS Code Level |"; }
            if(successCheckBox.Checked==false && failedCheckBox.Checked==false 
                && noOrdersCheckBox.Checked==false) { 
                valid=false; msg+="| Order Status |"; 
            }

            //ADD CONTACT
            if(valid) {
                //Instantiate
                this.sp = new Supplier();
                //--

                //Update Person Info
                this.sp.FName = fNameSupplierInputBox.Text;
                this.sp.LName = lNameSupplierInputBox.Text;
                this.sp.PhNumber = phNumSupplierInputBox.Text;
                this.sp.Country = countrySupplierInputBox.Text;
                this.sp.ProvState = provStateSupplierInputBox.Text;
                this.sp.Employer = employerSupplierInputBox.Text;
                this.sp.Email = emailSupplierInputBox.Text;
                this.sp.PostZip = zipSupplierInputBox.Text;
                this.sp.Notes = commentsSupplierInputBox.Text;
                this.sp.OhsLevel = OHSInputBox.Text;
                if(successCheckBox.Checked) {
                    this.sp.SuccessfulDeliveries = "1";
                }

                //INSERT SUPPLIER!
                int ok = this.sp.AddSupplier();
                //--

                if(ok<0) {
                    fNameSupplierInputBox.Clear();
                    lNameSupplierInputBox.Clear();
                    phNumSupplierInputBox.Clear();
                    countrySupplierInputBox.Clear();
                    provStateSupplierInputBox.Clear();
                    employerSupplierInputBox.Clear();
                    emailSupplierInputBox.Clear();
                    zipSupplierInputBox.Clear();
                    commentsSupplierInputBox.Clear();
                    successCheckBox.Checked = false;
                    failedCheckBox.Checked = false;
                    noOrdersCheckBox.Checked = false;
                    MessageBox.Show("Supplier Added!"); //<--UI MSG

                } else { MessageBox.Show("Supplier Could Not Be Successfully Added... "+
                                            "Check Information Entered Is Correct"); }
                     
            } else {
                msg += " Fields, Please Enter And Continue";
                MessageBox.Show(msg);
            }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateButton_Click(object sender, EventArgs e) {
            if(searchTypeComboBox.SelectedIndex<=0) {
                if(this.cp.Worked) {
                    UpdatePersonView upI = new UpdatePersonView(this.cp);
                    upI.ShowDialog();
                
                } else { MessageBox.Show("Please Select A Single Contact First!"); }
            
            } else {
                if(this.sp.Worked) {
                    UpdatePersonView upI = new UpdatePersonView(this.sp);
                    upI.ShowDialog();
                } else { MessageBox.Show("Please Select A Single Supplier First!"); }
            }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchAllButton_Click(object sender, EventArgs e) {
            string spacings = "--------------------";

            if(searchTypeComboBox.SelectedIndex<=0) {
                //Instantiate
                if(this.cp==null) { this.cp = new Contact(); }
                //--
                List<Contact> currC = this.cp.GetAllContactsList();
                if(currC!=null) {
                    searchResultsListBox.Items.Clear(); //<--Clean Box
                    for(int i=0; i<currC.Count; i++) {
                        searchResultsListBox.Items.Add(spacings);
                        searchResultsListBox.Items.Add("ID: " + currC[i].ID);
                        searchResultsListBox.Items.Add("Firstname: " + currC[i].FName);
                        searchResultsListBox.Items.Add("Lastname: " + currC[i].LName);
                        searchResultsListBox.Items.Add("Phone #: " + currC[i].PhNumber);
                        searchResultsListBox.Items.Add("Country: " + currC[i].Country);
                        searchResultsListBox.Items.Add("Prov/State: " + currC[i].ProvState);
                        searchResultsListBox.Items.Add("Employer: " + currC[i].Employer);
                        searchResultsListBox.Items.Add("Email: " + currC[i].Email);
                        searchResultsListBox.Items.Add("Post/Zip: " + currC[i].PostZip);
                        searchResultsListBox.Items.Add("Notes: " + currC[i].Notes);
                        if(currC[i].OrdersPending.CompareTo("0")==0) { 
                            searchResultsListBox.Items.Add("Orders Pending: No"); 
                       
                        } else { searchResultsListBox.Items.Add("Orders Pending: Yes"); }
                        
                        if(currC[i].IsCompanyHandler.CompareTo("1")==0) { 
                            searchResultsListBox.Items.Add("Handler ID: " + currC[i].CompanyHandlerID); 
                        
                        } else { searchResultsListBox.Items.Add("Not A Handler"); }
                        searchResultsListBox.Items.Add("Cotact ID: " + currC[i].ContactID);
                        searchResultsListBox.Items.Add(spacings + "\n");
                    }
                }
    
            } else {
                //Instantiate
                if(this.sp==null) { this.sp = new Supplier(); }
                //--
                List<Supplier> currS = this.sp.GetAllSuppliersList();
                if(currS!=null) {
                    searchResultsListBox.Items.Clear(); //<--Clean Box
                    for(int i=0; i<currS.Count; i++) {
                        searchResultsListBox.Items.Add(spacings);
                        searchResultsListBox.Items.Add("ID: " + currS[i].ID);
                        searchResultsListBox.Items.Add("Supplier ID: " + currS[i].SupplierId);
                        searchResultsListBox.Items.Add("Firstname: " + currS[i].FName);
                        searchResultsListBox.Items.Add("Lastname: " + currS[i].LName);
                        searchResultsListBox.Items.Add("Phone #: " + currS[i].PhNumber);
                        searchResultsListBox.Items.Add("Country: " + currS[i].Country);
                        searchResultsListBox.Items.Add("Prov/State: " + currS[i].ProvState);
                        searchResultsListBox.Items.Add("Employer: " + currS[i].Employer);
                        searchResultsListBox.Items.Add("Email: " + currS[i].Email);
                        searchResultsListBox.Items.Add("Post/Zip: " + currS[i].PostZip);
                        searchResultsListBox.Items.Add("Notes: " + currS[i].Notes);
                        searchResultsListBox.Items.Add("OHS Level: " + currS[i].OhsLevel);
                        searchResultsListBox.Items.Add("Successful Deliveries: " 
                            + currS[i].SuccessfulDeliveries);
                        searchResultsListBox.Items.Add("Failed Deliveries: " 
                            + currS[i].FailedDeliveries);
                        searchResultsListBox.Items.Add(spacings + "\n");
                    }
                }
            }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e) {

            if(searchTypeComboBox.SelectedIndex<=0) {
                if(this.cp!=null && this.cp.Worked) {
                    int ok = this.cp.DeleteContactByID(Int32.Parse(this.cp.ID));

                    if(ok < 0) {
                        MessageBox.Show("Contact Removed!!");
                        searchResultsListBox.Items.Clear();
                        searchResultsListBox.Items.Add("Contact " + 
                            this.cp.FName + " " + this.cp.LName + " Removed");
                        //Safety
                        this.cp = null;
                    }
                }
            
            } else {
                if(this.sp!=null && this.sp.Worked) {
                    int ok = this.sp.DeleteSupplierByID(Int32.Parse(this.sp.SupplierId));

                    if(ok < 0) {
                        MessageBox.Show("Supplier Removed!!");
                        searchResultsListBox.Items.Clear();
                        searchResultsListBox.Items.Add("Supplier " + 
                            this.sp.FName + " " + this.sp.LName + " Removed");
                        //Safety
                        this.sp = null;
                    }
                }
            }

        } //end event
                

        
    } //EOC

} //EON

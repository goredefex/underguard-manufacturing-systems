﻿namespace UG_WindowsExe {
    partial class UpdateProductView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.updateProductGroupBox = new System.Windows.Forms.GroupBox();
            this.pAddMaterialsButton = new System.Windows.Forms.Button();
            this.pColorComboBox = new System.Windows.Forms.ComboBox();
            this.staticPColorLabel = new System.Windows.Forms.Label();
            this.pImgPathInputBox = new System.Windows.Forms.TextBox();
            this.staticPImgPathLabel = new System.Windows.Forms.Label();
            this.pShipZonesInputBox = new System.Windows.Forms.TextBox();
            this.updateProductButton = new System.Windows.Forms.Button();
            this.staticPDescLabel = new System.Windows.Forms.Label();
            this.pDescriptionInputBox = new System.Windows.Forms.TextBox();
            this.staticPShipZonesLabel = new System.Windows.Forms.Label();
            this.pModelNumInputBox = new System.Windows.Forms.TextBox();
            this.staticModelNumLabel = new System.Windows.Forms.Label();
            this.staticPPriceLabel = new System.Windows.Forms.Label();
            this.pPriceInputBox = new System.Windows.Forms.TextBox();
            this.staticWeightLabel = new System.Windows.Forms.Label();
            this.pWeightInputBox = new System.Windows.Forms.TextBox();
            this.pSafetyInputBox = new System.Windows.Forms.TextBox();
            this.staticSafetyLabel = new System.Windows.Forms.Label();
            this.staticProductNameLabel = new System.Windows.Forms.Label();
            this.pNameInputBox = new System.Windows.Forms.TextBox();
            this.updateProductGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateProductGroupBox
            // 
            this.updateProductGroupBox.Controls.Add(this.pAddMaterialsButton);
            this.updateProductGroupBox.Controls.Add(this.pColorComboBox);
            this.updateProductGroupBox.Controls.Add(this.staticPColorLabel);
            this.updateProductGroupBox.Controls.Add(this.pImgPathInputBox);
            this.updateProductGroupBox.Controls.Add(this.staticPImgPathLabel);
            this.updateProductGroupBox.Controls.Add(this.pShipZonesInputBox);
            this.updateProductGroupBox.Controls.Add(this.updateProductButton);
            this.updateProductGroupBox.Controls.Add(this.staticPDescLabel);
            this.updateProductGroupBox.Controls.Add(this.pDescriptionInputBox);
            this.updateProductGroupBox.Controls.Add(this.staticPShipZonesLabel);
            this.updateProductGroupBox.Controls.Add(this.pModelNumInputBox);
            this.updateProductGroupBox.Controls.Add(this.staticModelNumLabel);
            this.updateProductGroupBox.Controls.Add(this.staticPPriceLabel);
            this.updateProductGroupBox.Controls.Add(this.pPriceInputBox);
            this.updateProductGroupBox.Controls.Add(this.staticWeightLabel);
            this.updateProductGroupBox.Controls.Add(this.pWeightInputBox);
            this.updateProductGroupBox.Controls.Add(this.pSafetyInputBox);
            this.updateProductGroupBox.Controls.Add(this.staticSafetyLabel);
            this.updateProductGroupBox.Controls.Add(this.staticProductNameLabel);
            this.updateProductGroupBox.Controls.Add(this.pNameInputBox);
            this.updateProductGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateProductGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateProductGroupBox.Location = new System.Drawing.Point(0, 0);
            this.updateProductGroupBox.Name = "updateProductGroupBox";
            this.updateProductGroupBox.Size = new System.Drawing.Size(412, 426);
            this.updateProductGroupBox.TabIndex = 4;
            this.updateProductGroupBox.TabStop = false;
            this.updateProductGroupBox.Text = "Update A Product:";
            // 
            // pAddMaterialsButton
            // 
            this.pAddMaterialsButton.BackColor = System.Drawing.Color.DarkRed;
            this.pAddMaterialsButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pAddMaterialsButton.Location = new System.Drawing.Point(33, 282);
            this.pAddMaterialsButton.Name = "pAddMaterialsButton";
            this.pAddMaterialsButton.Size = new System.Drawing.Size(88, 61);
            this.pAddMaterialsButton.TabIndex = 49;
            this.pAddMaterialsButton.Text = "Materials Needed";
            this.pAddMaterialsButton.UseVisualStyleBackColor = false;
            this.pAddMaterialsButton.Click += new System.EventHandler(this.pAddMaterialsButton_Click);
            // 
            // pColorComboBox
            // 
            this.pColorComboBox.FormattingEnabled = true;
            this.pColorComboBox.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "Chrome",
            "Black",
            "White"});
            this.pColorComboBox.Location = new System.Drawing.Point(171, 213);
            this.pColorComboBox.Name = "pColorComboBox";
            this.pColorComboBox.Size = new System.Drawing.Size(195, 23);
            this.pColorComboBox.TabIndex = 48;
            this.pColorComboBox.Text = "Choose Color:";
            // 
            // staticPColorLabel
            // 
            this.staticPColorLabel.AutoSize = true;
            this.staticPColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPColorLabel.Location = new System.Drawing.Point(13, 218);
            this.staticPColorLabel.Name = "staticPColorLabel";
            this.staticPColorLabel.Size = new System.Drawing.Size(40, 13);
            this.staticPColorLabel.TabIndex = 47;
            this.staticPColorLabel.Text = "Color:";
            // 
            // pImgPathInputBox
            // 
            this.pImgPathInputBox.Location = new System.Drawing.Point(171, 186);
            this.pImgPathInputBox.Name = "pImgPathInputBox";
            this.pImgPathInputBox.Size = new System.Drawing.Size(194, 21);
            this.pImgPathInputBox.TabIndex = 46;
            // 
            // staticPImgPathLabel
            // 
            this.staticPImgPathLabel.AutoSize = true;
            this.staticPImgPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPImgPathLabel.Location = new System.Drawing.Point(13, 191);
            this.staticPImgPathLabel.Name = "staticPImgPathLabel";
            this.staticPImgPathLabel.Size = new System.Drawing.Size(61, 13);
            this.staticPImgPathLabel.TabIndex = 45;
            this.staticPImgPathLabel.Text = "Img Path:";
            // 
            // pShipZonesInputBox
            // 
            this.pShipZonesInputBox.Location = new System.Drawing.Point(172, 159);
            this.pShipZonesInputBox.Name = "pShipZonesInputBox";
            this.pShipZonesInputBox.Size = new System.Drawing.Size(194, 21);
            this.pShipZonesInputBox.TabIndex = 44;
            // 
            // updateProductButton
            // 
            this.updateProductButton.BackColor = System.Drawing.Color.Aquamarine;
            this.updateProductButton.Location = new System.Drawing.Point(33, 359);
            this.updateProductButton.Name = "updateProductButton";
            this.updateProductButton.Size = new System.Drawing.Size(88, 48);
            this.updateProductButton.TabIndex = 43;
            this.updateProductButton.Text = "Update!";
            this.updateProductButton.UseVisualStyleBackColor = false;
            this.updateProductButton.Click += new System.EventHandler(this.updateProductButton_Click);
            // 
            // staticPDescLabel
            // 
            this.staticPDescLabel.AutoSize = true;
            this.staticPDescLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPDescLabel.Location = new System.Drawing.Point(13, 247);
            this.staticPDescLabel.Name = "staticPDescLabel";
            this.staticPDescLabel.Size = new System.Drawing.Size(123, 13);
            this.staticPDescLabel.TabIndex = 42;
            this.staticPDescLabel.Text = "Product Description:";
            // 
            // pDescriptionInputBox
            // 
            this.pDescriptionInputBox.Location = new System.Drawing.Point(146, 242);
            this.pDescriptionInputBox.Multiline = true;
            this.pDescriptionInputBox.Name = "pDescriptionInputBox";
            this.pDescriptionInputBox.Size = new System.Drawing.Size(219, 169);
            this.pDescriptionInputBox.TabIndex = 41;
            // 
            // staticPShipZonesLabel
            // 
            this.staticPShipZonesLabel.AutoSize = true;
            this.staticPShipZonesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPShipZonesLabel.Location = new System.Drawing.Point(14, 164);
            this.staticPShipZonesLabel.Name = "staticPShipZonesLabel";
            this.staticPShipZonesLabel.Size = new System.Drawing.Size(134, 13);
            this.staticPShipZonesLabel.TabIndex = 40;
            this.staticPShipZonesLabel.Text = "ALTA-Shipping Zones:";
            // 
            // pModelNumInputBox
            // 
            this.pModelNumInputBox.Location = new System.Drawing.Point(172, 132);
            this.pModelNumInputBox.Name = "pModelNumInputBox";
            this.pModelNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.pModelNumInputBox.TabIndex = 39;
            // 
            // staticModelNumLabel
            // 
            this.staticModelNumLabel.AutoSize = true;
            this.staticModelNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticModelNumLabel.Location = new System.Drawing.Point(14, 137);
            this.staticModelNumLabel.Name = "staticModelNumLabel";
            this.staticModelNumLabel.Size = new System.Drawing.Size(92, 13);
            this.staticModelNumLabel.TabIndex = 38;
            this.staticModelNumLabel.Text = "Model Number:";
            // 
            // staticPPriceLabel
            // 
            this.staticPPriceLabel.AutoSize = true;
            this.staticPPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPPriceLabel.Location = new System.Drawing.Point(14, 110);
            this.staticPPriceLabel.Name = "staticPPriceLabel";
            this.staticPPriceLabel.Size = new System.Drawing.Size(77, 13);
            this.staticPPriceLabel.TabIndex = 37;
            this.staticPPriceLabel.Text = "Retail Price:";
            // 
            // pPriceInputBox
            // 
            this.pPriceInputBox.Location = new System.Drawing.Point(172, 105);
            this.pPriceInputBox.Name = "pPriceInputBox";
            this.pPriceInputBox.Size = new System.Drawing.Size(195, 21);
            this.pPriceInputBox.TabIndex = 36;
            // 
            // staticWeightLabel
            // 
            this.staticWeightLabel.AutoSize = true;
            this.staticWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticWeightLabel.Location = new System.Drawing.Point(15, 83);
            this.staticWeightLabel.Name = "staticWeightLabel";
            this.staticWeightLabel.Size = new System.Drawing.Size(90, 13);
            this.staticWeightLabel.TabIndex = 35;
            this.staticWeightLabel.Text = "Weight in Kgs:";
            // 
            // pWeightInputBox
            // 
            this.pWeightInputBox.Location = new System.Drawing.Point(172, 78);
            this.pWeightInputBox.Name = "pWeightInputBox";
            this.pWeightInputBox.Size = new System.Drawing.Size(194, 21);
            this.pWeightInputBox.TabIndex = 34;
            // 
            // pSafetyInputBox
            // 
            this.pSafetyInputBox.Location = new System.Drawing.Point(172, 51);
            this.pSafetyInputBox.Name = "pSafetyInputBox";
            this.pSafetyInputBox.Size = new System.Drawing.Size(194, 21);
            this.pSafetyInputBox.TabIndex = 32;
            // 
            // staticSafetyLabel
            // 
            this.staticSafetyLabel.AutoSize = true;
            this.staticSafetyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSafetyLabel.Location = new System.Drawing.Point(14, 56);
            this.staticSafetyLabel.Name = "staticSafetyLabel";
            this.staticSafetyLabel.Size = new System.Drawing.Size(79, 13);
            this.staticSafetyLabel.TabIndex = 31;
            this.staticSafetyLabel.Text = "Safety code:";
            // 
            // staticProductNameLabel
            // 
            this.staticProductNameLabel.AutoSize = true;
            this.staticProductNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticProductNameLabel.Location = new System.Drawing.Point(14, 29);
            this.staticProductNameLabel.Name = "staticProductNameLabel";
            this.staticProductNameLabel.Size = new System.Drawing.Size(43, 13);
            this.staticProductNameLabel.TabIndex = 30;
            this.staticProductNameLabel.Text = "Name:";
            // 
            // pNameInputBox
            // 
            this.pNameInputBox.Location = new System.Drawing.Point(171, 24);
            this.pNameInputBox.Name = "pNameInputBox";
            this.pNameInputBox.Size = new System.Drawing.Size(195, 21);
            this.pNameInputBox.TabIndex = 29;
            // 
            // UpdateProductView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 426);
            this.Controls.Add(this.updateProductGroupBox);
            this.Name = "UpdateProductView";
            this.Text = "ProductUpdateView";
            this.updateProductGroupBox.ResumeLayout(false);
            this.updateProductGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox updateProductGroupBox;
        private System.Windows.Forms.Button pAddMaterialsButton;
        private System.Windows.Forms.ComboBox pColorComboBox;
        private System.Windows.Forms.Label staticPColorLabel;
        private System.Windows.Forms.TextBox pImgPathInputBox;
        private System.Windows.Forms.Label staticPImgPathLabel;
        private System.Windows.Forms.TextBox pShipZonesInputBox;
        private System.Windows.Forms.Button updateProductButton;
        private System.Windows.Forms.Label staticPDescLabel;
        private System.Windows.Forms.TextBox pDescriptionInputBox;
        private System.Windows.Forms.Label staticPShipZonesLabel;
        private System.Windows.Forms.TextBox pModelNumInputBox;
        private System.Windows.Forms.Label staticModelNumLabel;
        private System.Windows.Forms.Label staticPPriceLabel;
        private System.Windows.Forms.TextBox pPriceInputBox;
        private System.Windows.Forms.Label staticWeightLabel;
        private System.Windows.Forms.TextBox pWeightInputBox;
        private System.Windows.Forms.TextBox pSafetyInputBox;
        private System.Windows.Forms.Label staticSafetyLabel;
        private System.Windows.Forms.Label staticProductNameLabel;
        private System.Windows.Forms.TextBox pNameInputBox;

    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UG_WindowsExe {
    public partial class MainUI : Form {
        public MainUI() {
            InitializeComponent();
        }

        private void manageContactBtn_Click(object sender, EventArgs e) {
            PersonView pp = new PersonView();
            pp.ShowDialog();
        }

        private void exitBtn_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void productManageButton_Click(object sender, EventArgs e) {
            ProductView pp = new ProductView();
            pp.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e) {
            CatalogView cc = new CatalogView();
            cc.Show();

        }

        private void button1_Click(object sender, EventArgs e) {
            OrdersView oV = new OrdersView();
            oV.Show();
        }
    }
}

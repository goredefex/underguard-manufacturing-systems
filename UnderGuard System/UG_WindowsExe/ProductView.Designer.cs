﻿namespace UG_WindowsExe {
    partial class ProductView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.mainBodySplitContainer = new System.Windows.Forms.SplitContainer();
            this.searchSideSplitContainer = new System.Windows.Forms.SplitContainer();
            this.productsGroupBox = new System.Windows.Forms.GroupBox();
            this.productSearchListBox = new System.Windows.Forms.ListBox();
            this.searchingGroupBox = new System.Windows.Forms.GroupBox();
            this.searchAllButton = new System.Windows.Forms.Button();
            this.searchIdInputBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.enterProductGroupBox = new System.Windows.Forms.GroupBox();
            this.pAddMaterialsButton = new System.Windows.Forms.Button();
            this.pColorComboBox = new System.Windows.Forms.ComboBox();
            this.staticPColorLabel = new System.Windows.Forms.Label();
            this.pImgPathInputBox = new System.Windows.Forms.TextBox();
            this.staticPImgPathLabel = new System.Windows.Forms.Label();
            this.pShipZonesInputBox = new System.Windows.Forms.TextBox();
            this.addProductButton = new System.Windows.Forms.Button();
            this.staticPDescLabel = new System.Windows.Forms.Label();
            this.pDescriptionInputBox = new System.Windows.Forms.TextBox();
            this.staticPShipZonesLabel = new System.Windows.Forms.Label();
            this.pModelNumInputBox = new System.Windows.Forms.TextBox();
            this.staticModelNumLabel = new System.Windows.Forms.Label();
            this.staticPPriceLabel = new System.Windows.Forms.Label();
            this.pPriceInputBox = new System.Windows.Forms.TextBox();
            this.staticWeightLabel = new System.Windows.Forms.Label();
            this.pWeightInputBox = new System.Windows.Forms.TextBox();
            this.pSafetyInputBox = new System.Windows.Forms.TextBox();
            this.staticSafetyLabel = new System.Windows.Forms.Label();
            this.staticProductNameLabel = new System.Windows.Forms.Label();
            this.pNameInputBox = new System.Windows.Forms.TextBox();
            this.productResultsGroupBox = new System.Windows.Forms.GroupBox();
            this.productOutputBox = new System.Windows.Forms.TextBox();
            this.removeProductButton = new System.Windows.Forms.Button();
            this.editMaterialsButton = new System.Windows.Forms.Button();
            this.productPictureBox = new System.Windows.Forms.PictureBox();
            this.editProductButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mainBodySplitContainer)).BeginInit();
            this.mainBodySplitContainer.Panel1.SuspendLayout();
            this.mainBodySplitContainer.Panel2.SuspendLayout();
            this.mainBodySplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchSideSplitContainer)).BeginInit();
            this.searchSideSplitContainer.Panel1.SuspendLayout();
            this.searchSideSplitContainer.Panel2.SuspendLayout();
            this.searchSideSplitContainer.SuspendLayout();
            this.productsGroupBox.SuspendLayout();
            this.searchingGroupBox.SuspendLayout();
            this.enterProductGroupBox.SuspendLayout();
            this.productResultsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // mainBodySplitContainer
            // 
            this.mainBodySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainBodySplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainBodySplitContainer.Name = "mainBodySplitContainer";
            // 
            // mainBodySplitContainer.Panel1
            // 
            this.mainBodySplitContainer.Panel1.Controls.Add(this.searchSideSplitContainer);
            // 
            // mainBodySplitContainer.Panel2
            // 
            this.mainBodySplitContainer.Panel2.Controls.Add(this.enterProductGroupBox);
            this.mainBodySplitContainer.Panel2.Controls.Add(this.productResultsGroupBox);
            this.mainBodySplitContainer.Size = new System.Drawing.Size(956, 423);
            this.mainBodySplitContainer.SplitterDistance = 279;
            this.mainBodySplitContainer.TabIndex = 0;
            // 
            // searchSideSplitContainer
            // 
            this.searchSideSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchSideSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.searchSideSplitContainer.Name = "searchSideSplitContainer";
            this.searchSideSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // searchSideSplitContainer.Panel1
            // 
            this.searchSideSplitContainer.Panel1.Controls.Add(this.productsGroupBox);
            // 
            // searchSideSplitContainer.Panel2
            // 
            this.searchSideSplitContainer.Panel2.Controls.Add(this.searchingGroupBox);
            this.searchSideSplitContainer.Size = new System.Drawing.Size(279, 423);
            this.searchSideSplitContainer.SplitterDistance = 343;
            this.searchSideSplitContainer.TabIndex = 0;
            // 
            // productsGroupBox
            // 
            this.productsGroupBox.Controls.Add(this.productSearchListBox);
            this.productsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productsGroupBox.Location = new System.Drawing.Point(0, 0);
            this.productsGroupBox.Name = "productsGroupBox";
            this.productsGroupBox.Size = new System.Drawing.Size(279, 343);
            this.productsGroupBox.TabIndex = 1;
            this.productsGroupBox.TabStop = false;
            this.productsGroupBox.Text = "Products";
            // 
            // productSearchListBox
            // 
            this.productSearchListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productSearchListBox.FormattingEnabled = true;
            this.productSearchListBox.ItemHeight = 15;
            this.productSearchListBox.Location = new System.Drawing.Point(3, 17);
            this.productSearchListBox.Name = "productSearchListBox";
            this.productSearchListBox.Size = new System.Drawing.Size(273, 323);
            this.productSearchListBox.TabIndex = 0;
            this.productSearchListBox.SelectedIndexChanged += new System.EventHandler(this.productSearchListBox_SelectedIndexChanged);
            // 
            // searchingGroupBox
            // 
            this.searchingGroupBox.Controls.Add(this.searchAllButton);
            this.searchingGroupBox.Controls.Add(this.searchIdInputBox);
            this.searchingGroupBox.Controls.Add(this.searchButton);
            this.searchingGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchingGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchingGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchingGroupBox.Name = "searchingGroupBox";
            this.searchingGroupBox.Size = new System.Drawing.Size(279, 76);
            this.searchingGroupBox.TabIndex = 0;
            this.searchingGroupBox.TabStop = false;
            this.searchingGroupBox.Text = "Search By ID:";
            // 
            // searchAllButton
            // 
            this.searchAllButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.searchAllButton.Location = new System.Drawing.Point(203, 26);
            this.searchAllButton.Name = "searchAllButton";
            this.searchAllButton.Size = new System.Drawing.Size(60, 34);
            this.searchAllButton.TabIndex = 2;
            this.searchAllButton.Text = "ALL!";
            this.searchAllButton.UseVisualStyleBackColor = false;
            this.searchAllButton.Click += new System.EventHandler(this.searchAllButton_Click);
            // 
            // searchIdInputBox
            // 
            this.searchIdInputBox.Location = new System.Drawing.Point(21, 33);
            this.searchIdInputBox.Name = "searchIdInputBox";
            this.searchIdInputBox.Size = new System.Drawing.Size(129, 21);
            this.searchIdInputBox.TabIndex = 1;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(157, 32);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(40, 23);
            this.searchButton.TabIndex = 0;
            this.searchButton.Text = "GO!";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // enterProductGroupBox
            // 
            this.enterProductGroupBox.Controls.Add(this.pAddMaterialsButton);
            this.enterProductGroupBox.Controls.Add(this.pColorComboBox);
            this.enterProductGroupBox.Controls.Add(this.staticPColorLabel);
            this.enterProductGroupBox.Controls.Add(this.pImgPathInputBox);
            this.enterProductGroupBox.Controls.Add(this.staticPImgPathLabel);
            this.enterProductGroupBox.Controls.Add(this.pShipZonesInputBox);
            this.enterProductGroupBox.Controls.Add(this.addProductButton);
            this.enterProductGroupBox.Controls.Add(this.staticPDescLabel);
            this.enterProductGroupBox.Controls.Add(this.pDescriptionInputBox);
            this.enterProductGroupBox.Controls.Add(this.staticPShipZonesLabel);
            this.enterProductGroupBox.Controls.Add(this.pModelNumInputBox);
            this.enterProductGroupBox.Controls.Add(this.staticModelNumLabel);
            this.enterProductGroupBox.Controls.Add(this.staticPPriceLabel);
            this.enterProductGroupBox.Controls.Add(this.pPriceInputBox);
            this.enterProductGroupBox.Controls.Add(this.staticWeightLabel);
            this.enterProductGroupBox.Controls.Add(this.pWeightInputBox);
            this.enterProductGroupBox.Controls.Add(this.pSafetyInputBox);
            this.enterProductGroupBox.Controls.Add(this.staticSafetyLabel);
            this.enterProductGroupBox.Controls.Add(this.staticProductNameLabel);
            this.enterProductGroupBox.Controls.Add(this.pNameInputBox);
            this.enterProductGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enterProductGroupBox.Location = new System.Drawing.Point(287, 0);
            this.enterProductGroupBox.Name = "enterProductGroupBox";
            this.enterProductGroupBox.Size = new System.Drawing.Size(386, 417);
            this.enterProductGroupBox.TabIndex = 3;
            this.enterProductGroupBox.TabStop = false;
            this.enterProductGroupBox.Text = "Enter A Product:";
            // 
            // pAddMaterialsButton
            // 
            this.pAddMaterialsButton.BackColor = System.Drawing.Color.DarkRed;
            this.pAddMaterialsButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.pAddMaterialsButton.Location = new System.Drawing.Point(33, 282);
            this.pAddMaterialsButton.Name = "pAddMaterialsButton";
            this.pAddMaterialsButton.Size = new System.Drawing.Size(88, 61);
            this.pAddMaterialsButton.TabIndex = 49;
            this.pAddMaterialsButton.Text = "Materials Needed";
            this.pAddMaterialsButton.UseVisualStyleBackColor = false;
            this.pAddMaterialsButton.Click += new System.EventHandler(this.pAddMaterialsButton_Click);
            // 
            // pColorComboBox
            // 
            this.pColorComboBox.FormattingEnabled = true;
            this.pColorComboBox.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "Chrome",
            "Black",
            "White"});
            this.pColorComboBox.Location = new System.Drawing.Point(171, 213);
            this.pColorComboBox.Name = "pColorComboBox";
            this.pColorComboBox.Size = new System.Drawing.Size(195, 23);
            this.pColorComboBox.TabIndex = 48;
            this.pColorComboBox.Text = "Choose Color:";
            // 
            // staticPColorLabel
            // 
            this.staticPColorLabel.AutoSize = true;
            this.staticPColorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPColorLabel.Location = new System.Drawing.Point(13, 218);
            this.staticPColorLabel.Name = "staticPColorLabel";
            this.staticPColorLabel.Size = new System.Drawing.Size(40, 13);
            this.staticPColorLabel.TabIndex = 47;
            this.staticPColorLabel.Text = "Color:";
            // 
            // pImgPathInputBox
            // 
            this.pImgPathInputBox.Location = new System.Drawing.Point(171, 186);
            this.pImgPathInputBox.Name = "pImgPathInputBox";
            this.pImgPathInputBox.Size = new System.Drawing.Size(194, 21);
            this.pImgPathInputBox.TabIndex = 46;
            // 
            // staticPImgPathLabel
            // 
            this.staticPImgPathLabel.AutoSize = true;
            this.staticPImgPathLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPImgPathLabel.Location = new System.Drawing.Point(13, 191);
            this.staticPImgPathLabel.Name = "staticPImgPathLabel";
            this.staticPImgPathLabel.Size = new System.Drawing.Size(61, 13);
            this.staticPImgPathLabel.TabIndex = 45;
            this.staticPImgPathLabel.Text = "Img Path:";
            // 
            // pShipZonesInputBox
            // 
            this.pShipZonesInputBox.Location = new System.Drawing.Point(172, 159);
            this.pShipZonesInputBox.Name = "pShipZonesInputBox";
            this.pShipZonesInputBox.Size = new System.Drawing.Size(194, 21);
            this.pShipZonesInputBox.TabIndex = 44;
            // 
            // addProductButton
            // 
            this.addProductButton.BackColor = System.Drawing.Color.Aquamarine;
            this.addProductButton.Location = new System.Drawing.Point(33, 359);
            this.addProductButton.Name = "addProductButton";
            this.addProductButton.Size = new System.Drawing.Size(88, 48);
            this.addProductButton.TabIndex = 43;
            this.addProductButton.Text = "Add!";
            this.addProductButton.UseVisualStyleBackColor = false;
            this.addProductButton.Click += new System.EventHandler(this.addProductButton_Click);
            // 
            // staticPDescLabel
            // 
            this.staticPDescLabel.AutoSize = true;
            this.staticPDescLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPDescLabel.Location = new System.Drawing.Point(13, 247);
            this.staticPDescLabel.Name = "staticPDescLabel";
            this.staticPDescLabel.Size = new System.Drawing.Size(123, 13);
            this.staticPDescLabel.TabIndex = 42;
            this.staticPDescLabel.Text = "Product Description:";
            // 
            // pDescriptionInputBox
            // 
            this.pDescriptionInputBox.Location = new System.Drawing.Point(146, 242);
            this.pDescriptionInputBox.Multiline = true;
            this.pDescriptionInputBox.Name = "pDescriptionInputBox";
            this.pDescriptionInputBox.Size = new System.Drawing.Size(219, 169);
            this.pDescriptionInputBox.TabIndex = 41;
            // 
            // staticPShipZonesLabel
            // 
            this.staticPShipZonesLabel.AutoSize = true;
            this.staticPShipZonesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPShipZonesLabel.Location = new System.Drawing.Point(14, 164);
            this.staticPShipZonesLabel.Name = "staticPShipZonesLabel";
            this.staticPShipZonesLabel.Size = new System.Drawing.Size(134, 13);
            this.staticPShipZonesLabel.TabIndex = 40;
            this.staticPShipZonesLabel.Text = "ALTA-Shipping Zones:";
            // 
            // pModelNumInputBox
            // 
            this.pModelNumInputBox.Location = new System.Drawing.Point(172, 132);
            this.pModelNumInputBox.Name = "pModelNumInputBox";
            this.pModelNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.pModelNumInputBox.TabIndex = 39;
            // 
            // staticModelNumLabel
            // 
            this.staticModelNumLabel.AutoSize = true;
            this.staticModelNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticModelNumLabel.Location = new System.Drawing.Point(14, 137);
            this.staticModelNumLabel.Name = "staticModelNumLabel";
            this.staticModelNumLabel.Size = new System.Drawing.Size(92, 13);
            this.staticModelNumLabel.TabIndex = 38;
            this.staticModelNumLabel.Text = "Model Number:";
            // 
            // staticPPriceLabel
            // 
            this.staticPPriceLabel.AutoSize = true;
            this.staticPPriceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPPriceLabel.Location = new System.Drawing.Point(14, 110);
            this.staticPPriceLabel.Name = "staticPPriceLabel";
            this.staticPPriceLabel.Size = new System.Drawing.Size(77, 13);
            this.staticPPriceLabel.TabIndex = 37;
            this.staticPPriceLabel.Text = "Retail Price:";
            // 
            // pPriceInputBox
            // 
            this.pPriceInputBox.Location = new System.Drawing.Point(172, 105);
            this.pPriceInputBox.Name = "pPriceInputBox";
            this.pPriceInputBox.Size = new System.Drawing.Size(195, 21);
            this.pPriceInputBox.TabIndex = 36;
            // 
            // staticWeightLabel
            // 
            this.staticWeightLabel.AutoSize = true;
            this.staticWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticWeightLabel.Location = new System.Drawing.Point(15, 83);
            this.staticWeightLabel.Name = "staticWeightLabel";
            this.staticWeightLabel.Size = new System.Drawing.Size(90, 13);
            this.staticWeightLabel.TabIndex = 35;
            this.staticWeightLabel.Text = "Weight in Kgs:";
            // 
            // pWeightInputBox
            // 
            this.pWeightInputBox.Location = new System.Drawing.Point(172, 78);
            this.pWeightInputBox.Name = "pWeightInputBox";
            this.pWeightInputBox.Size = new System.Drawing.Size(194, 21);
            this.pWeightInputBox.TabIndex = 34;
            // 
            // pSafetyInputBox
            // 
            this.pSafetyInputBox.Location = new System.Drawing.Point(172, 51);
            this.pSafetyInputBox.Name = "pSafetyInputBox";
            this.pSafetyInputBox.Size = new System.Drawing.Size(194, 21);
            this.pSafetyInputBox.TabIndex = 32;
            // 
            // staticSafetyLabel
            // 
            this.staticSafetyLabel.AutoSize = true;
            this.staticSafetyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSafetyLabel.Location = new System.Drawing.Point(14, 56);
            this.staticSafetyLabel.Name = "staticSafetyLabel";
            this.staticSafetyLabel.Size = new System.Drawing.Size(79, 13);
            this.staticSafetyLabel.TabIndex = 31;
            this.staticSafetyLabel.Text = "Safety code:";
            // 
            // staticProductNameLabel
            // 
            this.staticProductNameLabel.AutoSize = true;
            this.staticProductNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticProductNameLabel.Location = new System.Drawing.Point(14, 29);
            this.staticProductNameLabel.Name = "staticProductNameLabel";
            this.staticProductNameLabel.Size = new System.Drawing.Size(43, 13);
            this.staticProductNameLabel.TabIndex = 30;
            this.staticProductNameLabel.Text = "Name:";
            // 
            // pNameInputBox
            // 
            this.pNameInputBox.Location = new System.Drawing.Point(171, 24);
            this.pNameInputBox.Name = "pNameInputBox";
            this.pNameInputBox.Size = new System.Drawing.Size(195, 21);
            this.pNameInputBox.TabIndex = 29;
            // 
            // productResultsGroupBox
            // 
            this.productResultsGroupBox.Controls.Add(this.productOutputBox);
            this.productResultsGroupBox.Controls.Add(this.removeProductButton);
            this.productResultsGroupBox.Controls.Add(this.editMaterialsButton);
            this.productResultsGroupBox.Controls.Add(this.productPictureBox);
            this.productResultsGroupBox.Controls.Add(this.editProductButton);
            this.productResultsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productResultsGroupBox.Location = new System.Drawing.Point(2, 0);
            this.productResultsGroupBox.Name = "productResultsGroupBox";
            this.productResultsGroupBox.Size = new System.Drawing.Size(285, 417);
            this.productResultsGroupBox.TabIndex = 2;
            this.productResultsGroupBox.TabStop = false;
            this.productResultsGroupBox.Text = "Results:";
            // 
            // productOutputBox
            // 
            this.productOutputBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.productOutputBox.Location = new System.Drawing.Point(6, 162);
            this.productOutputBox.Multiline = true;
            this.productOutputBox.Name = "productOutputBox";
            this.productOutputBox.ReadOnly = true;
            this.productOutputBox.Size = new System.Drawing.Size(268, 249);
            this.productOutputBox.TabIndex = 5;
            // 
            // removeProductButton
            // 
            this.removeProductButton.BackColor = System.Drawing.Color.DarkRed;
            this.removeProductButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeProductButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.removeProductButton.Location = new System.Drawing.Point(146, 17);
            this.removeProductButton.Name = "removeProductButton";
            this.removeProductButton.Size = new System.Drawing.Size(128, 35);
            this.removeProductButton.TabIndex = 4;
            this.removeProductButton.Text = "REMOVE";
            this.removeProductButton.UseVisualStyleBackColor = false;
            this.removeProductButton.Click += new System.EventHandler(this.removeProductButton_Click);
            // 
            // editMaterialsButton
            // 
            this.editMaterialsButton.BackColor = System.Drawing.Color.Green;
            this.editMaterialsButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editMaterialsButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.editMaterialsButton.Location = new System.Drawing.Point(146, 69);
            this.editMaterialsButton.Name = "editMaterialsButton";
            this.editMaterialsButton.Size = new System.Drawing.Size(128, 35);
            this.editMaterialsButton.TabIndex = 3;
            this.editMaterialsButton.Text = "MATERIALS";
            this.editMaterialsButton.UseVisualStyleBackColor = false;
            this.editMaterialsButton.Click += new System.EventHandler(this.editMaterialsButton_Click);
            // 
            // productPictureBox
            // 
            this.productPictureBox.Location = new System.Drawing.Point(6, 17);
            this.productPictureBox.Name = "productPictureBox";
            this.productPictureBox.Size = new System.Drawing.Size(134, 138);
            this.productPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.productPictureBox.TabIndex = 1;
            this.productPictureBox.TabStop = false;
            // 
            // editProductButton
            // 
            this.editProductButton.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.editProductButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editProductButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.editProductButton.Location = new System.Drawing.Point(146, 119);
            this.editProductButton.Name = "editProductButton";
            this.editProductButton.Size = new System.Drawing.Size(128, 35);
            this.editProductButton.TabIndex = 2;
            this.editProductButton.Text = "EDIT!";
            this.editProductButton.UseVisualStyleBackColor = false;
            this.editProductButton.Click += new System.EventHandler(this.editProductButton_Click);
            // 
            // ProductView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 423);
            this.Controls.Add(this.mainBodySplitContainer);
            this.Name = "ProductView";
            this.Text = "ProductView";
            this.mainBodySplitContainer.Panel1.ResumeLayout(false);
            this.mainBodySplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainBodySplitContainer)).EndInit();
            this.mainBodySplitContainer.ResumeLayout(false);
            this.searchSideSplitContainer.Panel1.ResumeLayout(false);
            this.searchSideSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchSideSplitContainer)).EndInit();
            this.searchSideSplitContainer.ResumeLayout(false);
            this.productsGroupBox.ResumeLayout(false);
            this.searchingGroupBox.ResumeLayout(false);
            this.searchingGroupBox.PerformLayout();
            this.enterProductGroupBox.ResumeLayout(false);
            this.enterProductGroupBox.PerformLayout();
            this.productResultsGroupBox.ResumeLayout(false);
            this.productResultsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.productPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainBodySplitContainer;
        private System.Windows.Forms.SplitContainer searchSideSplitContainer;
        private System.Windows.Forms.GroupBox productsGroupBox;
        private System.Windows.Forms.ListBox productSearchListBox;
        private System.Windows.Forms.GroupBox searchingGroupBox;
        private System.Windows.Forms.TextBox searchIdInputBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.GroupBox productResultsGroupBox;
        private System.Windows.Forms.PictureBox productPictureBox;
        private System.Windows.Forms.GroupBox enterProductGroupBox;
        private System.Windows.Forms.Button removeProductButton;
        private System.Windows.Forms.Button editMaterialsButton;
        private System.Windows.Forms.Button editProductButton;
        private System.Windows.Forms.TextBox productOutputBox;
        private System.Windows.Forms.Button addProductButton;
        private System.Windows.Forms.Label staticPDescLabel;
        private System.Windows.Forms.TextBox pDescriptionInputBox;
        private System.Windows.Forms.Label staticPShipZonesLabel;
        private System.Windows.Forms.TextBox pModelNumInputBox;
        private System.Windows.Forms.Label staticModelNumLabel;
        private System.Windows.Forms.Label staticPPriceLabel;
        private System.Windows.Forms.TextBox pPriceInputBox;
        private System.Windows.Forms.Label staticWeightLabel;
        private System.Windows.Forms.TextBox pWeightInputBox;
        private System.Windows.Forms.TextBox pSafetyInputBox;
        private System.Windows.Forms.Label staticSafetyLabel;
        private System.Windows.Forms.Label staticProductNameLabel;
        private System.Windows.Forms.TextBox pNameInputBox;
        private System.Windows.Forms.Button searchAllButton;
        private System.Windows.Forms.TextBox pShipZonesInputBox;
        private System.Windows.Forms.TextBox pImgPathInputBox;
        private System.Windows.Forms.Label staticPImgPathLabel;
        private System.Windows.Forms.Label staticPColorLabel;
        private System.Windows.Forms.ComboBox pColorComboBox;
        private System.Windows.Forms.Button pAddMaterialsButton;
    }
}
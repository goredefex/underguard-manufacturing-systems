﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class AddProductChangeMaterialView : Form {

        //Product Objects
        private List<Material> shownMaterials = null;
        private List<Material> pickedMaterials = new List<Material>();
        private List<int> quant = new List<int>();
                

        public AddProductChangeMaterialView() {
            InitializeComponent();
            this.ShowAllProducts();

        }


        /// <summary>
        /// 
        /// </summary>
        public List<int> Quant { get { return quant; }
                                 set { quant = value; } } //end field

        
        /// <summary>
        /// 
        /// </summary>
        public List<Material> PickedMaterials { get { return pickedMaterials; }
                                                set { pickedMaterials = value; } } //end field



        //==============================================
        //Functions ------------------------------------
        //==============================================

        private void ShowAllProducts() {
            notAddedListBox.Items.Clear();
            Material m = new Material(); //Make Empty
            this.shownMaterials = m.GetAllMaterialsObjs();

            for(int i=0; i<this.shownMaterials.Count; i++) {
                notAddedListBox.Items.Add(this.shownMaterials[i].Name 
                    + "   Part Number:" + this.shownMaterials[i].PartNum);
            }
        
        }

        private void addButton_Click(object sender, EventArgs e) {
            int found = notAddedListBox.SelectedIndex;
            if(found>=0) {
                if(quantControl.Value>=1) {
                    this.Quant.Add(Convert.ToInt32(quantControl.Value));
                    this.pickedMaterials.Add(this.shownMaterials[found]);
                    addedListBox.Items.Add(this.pickedMaterials[this.pickedMaterials.Count-1].Name 
                        + "   Part Number:" + this.pickedMaterials[this.pickedMaterials.Count-1].PartNum);
                    notAddedListBox.Items.RemoveAt(found);
                    this.shownMaterials.RemoveAt(found);
                
                } else { MessageBox.Show("Quantity Must Be 1 Or Higher"); }
            
            } else { MessageBox.Show("Please Choose A Material First"); }

        }

        private void removeButton_Click(object sender, EventArgs e) {
            int found = addedListBox.SelectedIndex;
            if(found>=0) {
                    this.shownMaterials.Add(this.PickedMaterials[found]);
                    notAddedListBox.Items.Add(this.shownMaterials[this.shownMaterials.Count-1].Name 
                        + "   Part Number:" + this.shownMaterials[this.shownMaterials.Count-1].PartNum);
                    addedListBox.Items.RemoveAt(found);
                    this.PickedMaterials.RemoveAt(found);
                    this.quant.RemoveAt(found);
                            
            } else { MessageBox.Show("Please Choose A Material First"); }

        }

        private void doneButton_Click(object sender, EventArgs e) {
            this.Close();

        } //end function

        //==============================================


    }
}

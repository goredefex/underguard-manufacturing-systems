﻿namespace UG_WindowsExe {
    partial class UpdateMaterialView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.materialTabGroupBox = new System.Windows.Forms.GroupBox();
            this.materialColorComboBox = new System.Windows.Forms.ComboBox();
            this.vendorComboBox = new System.Windows.Forms.ComboBox();
            this.updateMaterialButton = new System.Windows.Forms.Button();
            this.staticMaterialDescLabel = new System.Windows.Forms.Label();
            this.mDescriptionInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialPartNumLabel = new System.Windows.Forms.Label();
            this.partNumInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialColorsLabel = new System.Windows.Forms.Label();
            this.staticMaterialWeightLabel = new System.Windows.Forms.Label();
            this.materialWeightInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialInventoryLabel = new System.Windows.Forms.Label();
            this.materialInvInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialCostLabel = new System.Windows.Forms.Label();
            this.materialCostInputBox = new System.Windows.Forms.TextBox();
            this.staticVendorLabel = new System.Windows.Forms.Label();
            this.staticMaterialNameLabel = new System.Windows.Forms.Label();
            this.mNameInputBox = new System.Windows.Forms.TextBox();
            this.materialTabGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // materialTabGroupBox
            // 
            this.materialTabGroupBox.Controls.Add(this.materialColorComboBox);
            this.materialTabGroupBox.Controls.Add(this.vendorComboBox);
            this.materialTabGroupBox.Controls.Add(this.updateMaterialButton);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialDescLabel);
            this.materialTabGroupBox.Controls.Add(this.mDescriptionInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialPartNumLabel);
            this.materialTabGroupBox.Controls.Add(this.partNumInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialColorsLabel);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialWeightLabel);
            this.materialTabGroupBox.Controls.Add(this.materialWeightInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialInventoryLabel);
            this.materialTabGroupBox.Controls.Add(this.materialInvInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialCostLabel);
            this.materialTabGroupBox.Controls.Add(this.materialCostInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticVendorLabel);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialNameLabel);
            this.materialTabGroupBox.Controls.Add(this.mNameInputBox);
            this.materialTabGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialTabGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialTabGroupBox.Location = new System.Drawing.Point(0, 0);
            this.materialTabGroupBox.Name = "materialTabGroupBox";
            this.materialTabGroupBox.Size = new System.Drawing.Size(394, 374);
            this.materialTabGroupBox.TabIndex = 8;
            this.materialTabGroupBox.TabStop = false;
            this.materialTabGroupBox.Text = "Update A Material";
            // 
            // materialColorComboBox
            // 
            this.materialColorComboBox.FormattingEnabled = true;
            this.materialColorComboBox.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "Chrome",
            "Black",
            "White"});
            this.materialColorComboBox.Location = new System.Drawing.Point(181, 169);
            this.materialColorComboBox.Name = "materialColorComboBox";
            this.materialColorComboBox.Size = new System.Drawing.Size(195, 23);
            this.materialColorComboBox.TabIndex = 28;
            this.materialColorComboBox.Text = "Choose Color:";
            // 
            // vendorComboBox
            // 
            this.vendorComboBox.FormattingEnabled = true;
            this.vendorComboBox.Location = new System.Drawing.Point(183, 59);
            this.vendorComboBox.Name = "vendorComboBox";
            this.vendorComboBox.Size = new System.Drawing.Size(193, 23);
            this.vendorComboBox.TabIndex = 27;
            this.vendorComboBox.Text = "Choose Vendor:";
            // 
            // updateMaterialButton
            // 
            this.updateMaterialButton.BackColor = System.Drawing.Color.Aquamarine;
            this.updateMaterialButton.Location = new System.Drawing.Point(28, 291);
            this.updateMaterialButton.Name = "updateMaterialButton";
            this.updateMaterialButton.Size = new System.Drawing.Size(88, 48);
            this.updateMaterialButton.TabIndex = 18;
            this.updateMaterialButton.Text = "UPDATE!";
            this.updateMaterialButton.UseVisualStyleBackColor = false;
            this.updateMaterialButton.Click += new System.EventHandler(this.updateMaterialButton_Click);
            // 
            // staticMaterialDescLabel
            // 
            this.staticMaterialDescLabel.AutoSize = true;
            this.staticMaterialDescLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialDescLabel.Location = new System.Drawing.Point(16, 238);
            this.staticMaterialDescLabel.Name = "staticMaterialDescLabel";
            this.staticMaterialDescLabel.Size = new System.Drawing.Size(124, 13);
            this.staticMaterialDescLabel.TabIndex = 17;
            this.staticMaterialDescLabel.Text = "Material Description:";
            // 
            // mDescriptionInputBox
            // 
            this.mDescriptionInputBox.Location = new System.Drawing.Point(157, 233);
            this.mDescriptionInputBox.Multiline = true;
            this.mDescriptionInputBox.Name = "mDescriptionInputBox";
            this.mDescriptionInputBox.Size = new System.Drawing.Size(219, 131);
            this.mDescriptionInputBox.TabIndex = 16;
            // 
            // staticMaterialPartNumLabel
            // 
            this.staticMaterialPartNumLabel.AutoSize = true;
            this.staticMaterialPartNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialPartNumLabel.Location = new System.Drawing.Point(15, 204);
            this.staticMaterialPartNumLabel.Name = "staticMaterialPartNumLabel";
            this.staticMaterialPartNumLabel.Size = new System.Drawing.Size(117, 13);
            this.staticMaterialPartNumLabel.TabIndex = 13;
            this.staticMaterialPartNumLabel.Text = "Material Vendor ID:";
            // 
            // partNumInputBox
            // 
            this.partNumInputBox.Location = new System.Drawing.Point(181, 199);
            this.partNumInputBox.Name = "partNumInputBox";
            this.partNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.partNumInputBox.TabIndex = 12;
            // 
            // staticMaterialColorsLabel
            // 
            this.staticMaterialColorsLabel.AutoSize = true;
            this.staticMaterialColorsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialColorsLabel.Location = new System.Drawing.Point(16, 174);
            this.staticMaterialColorsLabel.Name = "staticMaterialColorsLabel";
            this.staticMaterialColorsLabel.Size = new System.Drawing.Size(89, 13);
            this.staticMaterialColorsLabel.TabIndex = 11;
            this.staticMaterialColorsLabel.Text = "Material Color:";
            // 
            // staticMaterialWeightLabel
            // 
            this.staticMaterialWeightLabel.AutoSize = true;
            this.staticMaterialWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialWeightLabel.Location = new System.Drawing.Point(16, 147);
            this.staticMaterialWeightLabel.Name = "staticMaterialWeightLabel";
            this.staticMaterialWeightLabel.Size = new System.Drawing.Size(100, 13);
            this.staticMaterialWeightLabel.TabIndex = 9;
            this.staticMaterialWeightLabel.Text = "Material Weight:";
            // 
            // materialWeightInputBox
            // 
            this.materialWeightInputBox.Location = new System.Drawing.Point(181, 142);
            this.materialWeightInputBox.Name = "materialWeightInputBox";
            this.materialWeightInputBox.Size = new System.Drawing.Size(195, 21);
            this.materialWeightInputBox.TabIndex = 8;
            // 
            // staticMaterialInventoryLabel
            // 
            this.staticMaterialInventoryLabel.AutoSize = true;
            this.staticMaterialInventoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialInventoryLabel.Location = new System.Drawing.Point(16, 120);
            this.staticMaterialInventoryLabel.Name = "staticMaterialInventoryLabel";
            this.staticMaterialInventoryLabel.Size = new System.Drawing.Size(93, 13);
            this.staticMaterialInventoryLabel.TabIndex = 7;
            this.staticMaterialInventoryLabel.Text = "Material Count:";
            // 
            // materialInvInputBox
            // 
            this.materialInvInputBox.Location = new System.Drawing.Point(181, 115);
            this.materialInvInputBox.Name = "materialInvInputBox";
            this.materialInvInputBox.Size = new System.Drawing.Size(194, 21);
            this.materialInvInputBox.TabIndex = 6;
            // 
            // staticMaterialCostLabel
            // 
            this.staticMaterialCostLabel.AutoSize = true;
            this.staticMaterialCostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialCostLabel.Location = new System.Drawing.Point(16, 93);
            this.staticMaterialCostLabel.Name = "staticMaterialCostLabel";
            this.staticMaterialCostLabel.Size = new System.Drawing.Size(85, 13);
            this.staticMaterialCostLabel.TabIndex = 5;
            this.staticMaterialCostLabel.Text = "Material Cost:";
            // 
            // materialCostInputBox
            // 
            this.materialCostInputBox.Location = new System.Drawing.Point(182, 88);
            this.materialCostInputBox.Name = "materialCostInputBox";
            this.materialCostInputBox.Size = new System.Drawing.Size(194, 21);
            this.materialCostInputBox.TabIndex = 4;
            // 
            // staticVendorLabel
            // 
            this.staticVendorLabel.AutoSize = true;
            this.staticVendorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticVendorLabel.Location = new System.Drawing.Point(16, 64);
            this.staticVendorLabel.Name = "staticVendorLabel";
            this.staticVendorLabel.Size = new System.Drawing.Size(100, 13);
            this.staticVendorLabel.TabIndex = 3;
            this.staticVendorLabel.Text = "Material Vendor:";
            // 
            // staticMaterialNameLabel
            // 
            this.staticMaterialNameLabel.AutoSize = true;
            this.staticMaterialNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialNameLabel.Location = new System.Drawing.Point(16, 37);
            this.staticMaterialNameLabel.Name = "staticMaterialNameLabel";
            this.staticMaterialNameLabel.Size = new System.Drawing.Size(92, 13);
            this.staticMaterialNameLabel.TabIndex = 1;
            this.staticMaterialNameLabel.Text = "Material Name:";
            // 
            // mNameInputBox
            // 
            this.mNameInputBox.Location = new System.Drawing.Point(183, 32);
            this.mNameInputBox.Name = "mNameInputBox";
            this.mNameInputBox.Size = new System.Drawing.Size(193, 21);
            this.mNameInputBox.TabIndex = 0;
            // 
            // UpdateMaterialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 374);
            this.Controls.Add(this.materialTabGroupBox);
            this.Name = "UpdateMaterialView";
            this.Text = "UpdateMaterialView";
            this.materialTabGroupBox.ResumeLayout(false);
            this.materialTabGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox materialTabGroupBox;
        private System.Windows.Forms.ComboBox materialColorComboBox;
        private System.Windows.Forms.ComboBox vendorComboBox;
        private System.Windows.Forms.Button updateMaterialButton;
        private System.Windows.Forms.Label staticMaterialDescLabel;
        private System.Windows.Forms.TextBox mDescriptionInputBox;
        private System.Windows.Forms.Label staticMaterialPartNumLabel;
        private System.Windows.Forms.TextBox partNumInputBox;
        private System.Windows.Forms.Label staticMaterialColorsLabel;
        private System.Windows.Forms.Label staticMaterialWeightLabel;
        private System.Windows.Forms.TextBox materialWeightInputBox;
        private System.Windows.Forms.Label staticMaterialInventoryLabel;
        private System.Windows.Forms.TextBox materialInvInputBox;
        private System.Windows.Forms.Label staticMaterialCostLabel;
        private System.Windows.Forms.TextBox materialCostInputBox;
        private System.Windows.Forms.Label staticVendorLabel;
        private System.Windows.Forms.Label staticMaterialNameLabel;
        private System.Windows.Forms.TextBox mNameInputBox;

    }
}
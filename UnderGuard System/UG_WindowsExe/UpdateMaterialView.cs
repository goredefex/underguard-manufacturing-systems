﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class UpdateMaterialView : Form {


        //Stored Entry Objects
        private List<Supplier> dropDownVendors = null;
        private Material m = null;


        //====================================================
        //Constructs -----------------------------------------
        //====================================================

        public UpdateMaterialView(Material currM) {
            InitializeComponent();
            m = currM; //<--Set
            this.ShowAllVendors(vendorComboBox);
            this.PopulateUIControls();
        
        } //end constructor

        //====================================================


        /// <summary>
        /// Display All Vendors In Drop Down
        /// </summary>
        public void ShowAllVendors(ComboBox inCC) {
            Supplier ss = new Supplier(); //<--Make Empty
            this.dropDownVendors = ss.GetAllSuppliersList();

            for(int i=0; i<dropDownVendors.Count; i++) {
                inCC.Items.Add(dropDownVendors[i].FName + " " + dropDownVendors[i].LName);
            }
        
        } //end function


        /// <summary>
        /// 
        /// </summary>
        private void PopulateUIControls() {
            
            //Find Vendor Source
            for(int i=0; i<this.dropDownVendors.Count; i++) {
                if(Int32.Parse(this.dropDownVendors[i].SupplierId) == this.m.VendorId) {
                    vendorComboBox.SelectedIndex = i;
                } 
            }

            //Find Color 
            for(int i=0; i<materialColorComboBox.Items.Count; i++) {
                if(materialColorComboBox.Items[i].ToString().CompareTo(this.m.Color)==0) {
                    materialColorComboBox.SelectedIndex = i;
                }
            }

            mNameInputBox.Text = this.m.Name;
            mDescriptionInputBox.Text = this.m.Description;
            materialCostInputBox.Text = this.m.Cost.ToString();
            materialInvInputBox.Text = this.m.Count.ToString();
            materialWeightInputBox.Text = this.m.Weight.ToString();
            partNumInputBox.Text = this.m.PartNum;
        
        } //end function



        /// <summary>
        /// Translates Color Drop Downs
        /// Into String Types
        /// </summary>
        /// <param name="currIndex"></param>
        /// <returns></returns>
        public string GetColor(int currIndex) {
            string color = String.Empty;
            switch(currIndex) {
                case 0:
                    color = "Red";
                    break;
                case 1:
                    color = "Green";
                    break;
                case 2:
                    color = "Blue";
                    break;
                case 3:
                    color = "Yellow";
                    break;
                case 4:
                    color = "Chrome";
                    break;
                case 5:
                    color = "Black";
                    break;
                case 6:
                    color = "White";
                    break;
                default:
                    color = "None";
                    break;
            }
        
            return color;

        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateMaterialButton_Click(object sender, EventArgs e) {
            bool valid = true;
            string msg = "You Are Missing: ";

            //Validation
            if(mNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Name |"; }
            if(vendorComboBox.SelectedIndex<0) { valid=false; msg+="| Vendor Id |"; }
            if(materialCostInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Cost |"; }
            if(materialInvInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Inventory Count |"; }
            if(materialWeightInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Weight |"; }
            if(materialColorComboBox.SelectedIndex<0) { valid=false; msg+="| Material Color |"; }
            if(partNumInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Part Number |"; }
            if(mDescriptionInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Description |"; }
            
            if(valid) {
                Material currM = new Material();
                int ok = currM.UpdateMaterial(this.m.MaterialId, 
                                              Int32.Parse(this.dropDownVendors[vendorComboBox.SelectedIndex].SupplierId), 
                                              mNameInputBox.Text, mDescriptionInputBox.Text, 
                                              Int32.Parse(materialCostInputBox.Text), Int32.Parse(materialInvInputBox.Text), 
                                              Int32.Parse(materialWeightInputBox.Text), 
                                              this.GetColor(materialColorComboBox.SelectedIndex), partNumInputBox.Text);
                
                if(ok < 0) { MessageBox.Show("Material Updated");  this.Close(); }
            
            } else {
                msg += " Fields, Please Enter And Continue";
                MessageBox.Show(msg);
            }

        } //end function


    } //EOC

} //EON

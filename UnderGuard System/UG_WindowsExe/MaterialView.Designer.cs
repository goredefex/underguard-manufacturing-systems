﻿namespace UG_WindowsExe {
    partial class MaterialView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.materialTabGroupBox = new System.Windows.Forms.GroupBox();
            this.materialColorComboBox = new System.Windows.Forms.ComboBox();
            this.vendorComboBox = new System.Windows.Forms.ComboBox();
            this.addMaterialButton = new System.Windows.Forms.Button();
            this.staticMaterialDescLabel = new System.Windows.Forms.Label();
            this.mDescriptionInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialPartNumLabel = new System.Windows.Forms.Label();
            this.partNumInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialColorsLabel = new System.Windows.Forms.Label();
            this.staticMaterialWeightLabel = new System.Windows.Forms.Label();
            this.materialWeightInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialInventoryLabel = new System.Windows.Forms.Label();
            this.materialInvInputBox = new System.Windows.Forms.TextBox();
            this.staticMaterialCostLabel = new System.Windows.Forms.Label();
            this.materialCostInputBox = new System.Windows.Forms.TextBox();
            this.staticVendorLabel = new System.Windows.Forms.Label();
            this.staticMaterialNameLabel = new System.Windows.Forms.Label();
            this.mNameInputBox = new System.Windows.Forms.TextBox();
            this.searchingSplitContainer = new System.Windows.Forms.SplitContainer();
            this.searchResultsGroupBox = new System.Windows.Forms.GroupBox();
            this.searchResultsListBox = new System.Windows.Forms.ListBox();
            this.searchPersonGroupBox = new System.Windows.Forms.GroupBox();
            this.vendorSearchComboBox = new System.Windows.Forms.ComboBox();
            this.searchByVendorButton = new System.Windows.Forms.Button();
            this.staticByVendorLabel = new System.Windows.Forms.Label();
            this.searchAllButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.searchNameInputBox = new System.Windows.Forms.TextBox();
            this.nameSearchButton = new System.Windows.Forms.Button();
            this.staticByNameLabel = new System.Windows.Forms.Label();
            this.idInputBox = new System.Windows.Forms.TextBox();
            this.idSearchButton = new System.Windows.Forms.Button();
            this.staticByIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.materialTabGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).BeginInit();
            this.searchingSplitContainer.Panel1.SuspendLayout();
            this.searchingSplitContainer.Panel2.SuspendLayout();
            this.searchingSplitContainer.SuspendLayout();
            this.searchResultsGroupBox.SuspendLayout();
            this.searchPersonGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.materialTabGroupBox);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.searchingSplitContainer);
            this.mainSplitContainer.Size = new System.Drawing.Size(757, 467);
            this.mainSplitContainer.SplitterDistance = 392;
            this.mainSplitContainer.TabIndex = 1;
            // 
            // materialTabGroupBox
            // 
            this.materialTabGroupBox.Controls.Add(this.materialColorComboBox);
            this.materialTabGroupBox.Controls.Add(this.vendorComboBox);
            this.materialTabGroupBox.Controls.Add(this.addMaterialButton);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialDescLabel);
            this.materialTabGroupBox.Controls.Add(this.mDescriptionInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialPartNumLabel);
            this.materialTabGroupBox.Controls.Add(this.partNumInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialColorsLabel);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialWeightLabel);
            this.materialTabGroupBox.Controls.Add(this.materialWeightInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialInventoryLabel);
            this.materialTabGroupBox.Controls.Add(this.materialInvInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialCostLabel);
            this.materialTabGroupBox.Controls.Add(this.materialCostInputBox);
            this.materialTabGroupBox.Controls.Add(this.staticVendorLabel);
            this.materialTabGroupBox.Controls.Add(this.staticMaterialNameLabel);
            this.materialTabGroupBox.Controls.Add(this.mNameInputBox);
            this.materialTabGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialTabGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialTabGroupBox.Location = new System.Drawing.Point(0, 0);
            this.materialTabGroupBox.Name = "materialTabGroupBox";
            this.materialTabGroupBox.Size = new System.Drawing.Size(392, 467);
            this.materialTabGroupBox.TabIndex = 7;
            this.materialTabGroupBox.TabStop = false;
            this.materialTabGroupBox.Text = "Enter A Material";
            // 
            // materialColorComboBox
            // 
            this.materialColorComboBox.FormattingEnabled = true;
            this.materialColorComboBox.Items.AddRange(new object[] {
            "Red",
            "Green",
            "Blue",
            "Yellow",
            "Chrome",
            "Black",
            "White"});
            this.materialColorComboBox.Location = new System.Drawing.Point(181, 169);
            this.materialColorComboBox.Name = "materialColorComboBox";
            this.materialColorComboBox.Size = new System.Drawing.Size(195, 23);
            this.materialColorComboBox.TabIndex = 28;
            this.materialColorComboBox.Text = "Choose Color:";
            // 
            // vendorComboBox
            // 
            this.vendorComboBox.FormattingEnabled = true;
            this.vendorComboBox.Location = new System.Drawing.Point(183, 59);
            this.vendorComboBox.Name = "vendorComboBox";
            this.vendorComboBox.Size = new System.Drawing.Size(193, 23);
            this.vendorComboBox.TabIndex = 27;
            this.vendorComboBox.Text = "Choose Vendor:";
            // 
            // addMaterialButton
            // 
            this.addMaterialButton.BackColor = System.Drawing.Color.Aquamarine;
            this.addMaterialButton.Location = new System.Drawing.Point(28, 291);
            this.addMaterialButton.Name = "addMaterialButton";
            this.addMaterialButton.Size = new System.Drawing.Size(88, 48);
            this.addMaterialButton.TabIndex = 18;
            this.addMaterialButton.Text = "Add!";
            this.addMaterialButton.UseVisualStyleBackColor = false;
            this.addMaterialButton.Click += new System.EventHandler(this.addMaterialButton_Click);
            // 
            // staticMaterialDescLabel
            // 
            this.staticMaterialDescLabel.AutoSize = true;
            this.staticMaterialDescLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialDescLabel.Location = new System.Drawing.Point(16, 238);
            this.staticMaterialDescLabel.Name = "staticMaterialDescLabel";
            this.staticMaterialDescLabel.Size = new System.Drawing.Size(124, 13);
            this.staticMaterialDescLabel.TabIndex = 17;
            this.staticMaterialDescLabel.Text = "Material Description:";
            // 
            // mDescriptionInputBox
            // 
            this.mDescriptionInputBox.Location = new System.Drawing.Point(157, 233);
            this.mDescriptionInputBox.Multiline = true;
            this.mDescriptionInputBox.Name = "mDescriptionInputBox";
            this.mDescriptionInputBox.Size = new System.Drawing.Size(219, 131);
            this.mDescriptionInputBox.TabIndex = 16;
            // 
            // staticMaterialPartNumLabel
            // 
            this.staticMaterialPartNumLabel.AutoSize = true;
            this.staticMaterialPartNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialPartNumLabel.Location = new System.Drawing.Point(15, 204);
            this.staticMaterialPartNumLabel.Name = "staticMaterialPartNumLabel";
            this.staticMaterialPartNumLabel.Size = new System.Drawing.Size(117, 13);
            this.staticMaterialPartNumLabel.TabIndex = 13;
            this.staticMaterialPartNumLabel.Text = "Material Vendor ID:";
            // 
            // partNumInputBox
            // 
            this.partNumInputBox.Location = new System.Drawing.Point(181, 199);
            this.partNumInputBox.Name = "partNumInputBox";
            this.partNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.partNumInputBox.TabIndex = 12;
            // 
            // staticMaterialColorsLabel
            // 
            this.staticMaterialColorsLabel.AutoSize = true;
            this.staticMaterialColorsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialColorsLabel.Location = new System.Drawing.Point(16, 174);
            this.staticMaterialColorsLabel.Name = "staticMaterialColorsLabel";
            this.staticMaterialColorsLabel.Size = new System.Drawing.Size(89, 13);
            this.staticMaterialColorsLabel.TabIndex = 11;
            this.staticMaterialColorsLabel.Text = "Material Color:";
            // 
            // staticMaterialWeightLabel
            // 
            this.staticMaterialWeightLabel.AutoSize = true;
            this.staticMaterialWeightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialWeightLabel.Location = new System.Drawing.Point(16, 147);
            this.staticMaterialWeightLabel.Name = "staticMaterialWeightLabel";
            this.staticMaterialWeightLabel.Size = new System.Drawing.Size(100, 13);
            this.staticMaterialWeightLabel.TabIndex = 9;
            this.staticMaterialWeightLabel.Text = "Material Weight:";
            // 
            // materialWeightInputBox
            // 
            this.materialWeightInputBox.Location = new System.Drawing.Point(181, 142);
            this.materialWeightInputBox.Name = "materialWeightInputBox";
            this.materialWeightInputBox.Size = new System.Drawing.Size(195, 21);
            this.materialWeightInputBox.TabIndex = 8;
            // 
            // staticMaterialInventoryLabel
            // 
            this.staticMaterialInventoryLabel.AutoSize = true;
            this.staticMaterialInventoryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialInventoryLabel.Location = new System.Drawing.Point(16, 120);
            this.staticMaterialInventoryLabel.Name = "staticMaterialInventoryLabel";
            this.staticMaterialInventoryLabel.Size = new System.Drawing.Size(93, 13);
            this.staticMaterialInventoryLabel.TabIndex = 7;
            this.staticMaterialInventoryLabel.Text = "Material Count:";
            // 
            // materialInvInputBox
            // 
            this.materialInvInputBox.Location = new System.Drawing.Point(181, 115);
            this.materialInvInputBox.Name = "materialInvInputBox";
            this.materialInvInputBox.Size = new System.Drawing.Size(194, 21);
            this.materialInvInputBox.TabIndex = 6;
            // 
            // staticMaterialCostLabel
            // 
            this.staticMaterialCostLabel.AutoSize = true;
            this.staticMaterialCostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialCostLabel.Location = new System.Drawing.Point(16, 93);
            this.staticMaterialCostLabel.Name = "staticMaterialCostLabel";
            this.staticMaterialCostLabel.Size = new System.Drawing.Size(85, 13);
            this.staticMaterialCostLabel.TabIndex = 5;
            this.staticMaterialCostLabel.Text = "Material Cost:";
            // 
            // materialCostInputBox
            // 
            this.materialCostInputBox.Location = new System.Drawing.Point(182, 88);
            this.materialCostInputBox.Name = "materialCostInputBox";
            this.materialCostInputBox.Size = new System.Drawing.Size(194, 21);
            this.materialCostInputBox.TabIndex = 4;
            // 
            // staticVendorLabel
            // 
            this.staticVendorLabel.AutoSize = true;
            this.staticVendorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticVendorLabel.Location = new System.Drawing.Point(16, 64);
            this.staticVendorLabel.Name = "staticVendorLabel";
            this.staticVendorLabel.Size = new System.Drawing.Size(100, 13);
            this.staticVendorLabel.TabIndex = 3;
            this.staticVendorLabel.Text = "Material Vendor:";
            // 
            // staticMaterialNameLabel
            // 
            this.staticMaterialNameLabel.AutoSize = true;
            this.staticMaterialNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialNameLabel.Location = new System.Drawing.Point(16, 37);
            this.staticMaterialNameLabel.Name = "staticMaterialNameLabel";
            this.staticMaterialNameLabel.Size = new System.Drawing.Size(92, 13);
            this.staticMaterialNameLabel.TabIndex = 1;
            this.staticMaterialNameLabel.Text = "Material Name:";
            // 
            // mNameInputBox
            // 
            this.mNameInputBox.Location = new System.Drawing.Point(183, 32);
            this.mNameInputBox.Name = "mNameInputBox";
            this.mNameInputBox.Size = new System.Drawing.Size(193, 21);
            this.mNameInputBox.TabIndex = 0;
            // 
            // searchingSplitContainer
            // 
            this.searchingSplitContainer.Location = new System.Drawing.Point(1, 0);
            this.searchingSplitContainer.Name = "searchingSplitContainer";
            this.searchingSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // searchingSplitContainer.Panel1
            // 
            this.searchingSplitContainer.Panel1.Controls.Add(this.searchResultsGroupBox);
            // 
            // searchingSplitContainer.Panel2
            // 
            this.searchingSplitContainer.Panel2.Controls.Add(this.searchPersonGroupBox);
            this.searchingSplitContainer.Size = new System.Drawing.Size(360, 473);
            this.searchingSplitContainer.SplitterDistance = 213;
            this.searchingSplitContainer.TabIndex = 0;
            // 
            // searchResultsGroupBox
            // 
            this.searchResultsGroupBox.Controls.Add(this.searchResultsListBox);
            this.searchResultsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchResultsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchResultsGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchResultsGroupBox.Name = "searchResultsGroupBox";
            this.searchResultsGroupBox.Size = new System.Drawing.Size(360, 213);
            this.searchResultsGroupBox.TabIndex = 0;
            this.searchResultsGroupBox.TabStop = false;
            this.searchResultsGroupBox.Text = "Search Results:";
            // 
            // searchResultsListBox
            // 
            this.searchResultsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchResultsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchResultsListBox.FormattingEnabled = true;
            this.searchResultsListBox.HorizontalScrollbar = true;
            this.searchResultsListBox.ItemHeight = 15;
            this.searchResultsListBox.Location = new System.Drawing.Point(3, 17);
            this.searchResultsListBox.Name = "searchResultsListBox";
            this.searchResultsListBox.Size = new System.Drawing.Size(354, 193);
            this.searchResultsListBox.TabIndex = 1;
            // 
            // searchPersonGroupBox
            // 
            this.searchPersonGroupBox.Controls.Add(this.vendorSearchComboBox);
            this.searchPersonGroupBox.Controls.Add(this.searchByVendorButton);
            this.searchPersonGroupBox.Controls.Add(this.staticByVendorLabel);
            this.searchPersonGroupBox.Controls.Add(this.searchAllButton);
            this.searchPersonGroupBox.Controls.Add(this.deleteButton);
            this.searchPersonGroupBox.Controls.Add(this.updateButton);
            this.searchPersonGroupBox.Controls.Add(this.searchNameInputBox);
            this.searchPersonGroupBox.Controls.Add(this.nameSearchButton);
            this.searchPersonGroupBox.Controls.Add(this.staticByNameLabel);
            this.searchPersonGroupBox.Controls.Add(this.idInputBox);
            this.searchPersonGroupBox.Controls.Add(this.idSearchButton);
            this.searchPersonGroupBox.Controls.Add(this.staticByIdLabel);
            this.searchPersonGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPersonGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPersonGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchPersonGroupBox.Name = "searchPersonGroupBox";
            this.searchPersonGroupBox.Size = new System.Drawing.Size(360, 256);
            this.searchPersonGroupBox.TabIndex = 19;
            this.searchPersonGroupBox.TabStop = false;
            this.searchPersonGroupBox.Text = "Search For Material";
            // 
            // vendorSearchComboBox
            // 
            this.vendorSearchComboBox.FormattingEnabled = true;
            this.vendorSearchComboBox.Location = new System.Drawing.Point(78, 117);
            this.vendorSearchComboBox.Name = "vendorSearchComboBox";
            this.vendorSearchComboBox.Size = new System.Drawing.Size(106, 23);
            this.vendorSearchComboBox.TabIndex = 20;
            this.vendorSearchComboBox.Text = "Pick:";
            // 
            // searchByVendorButton
            // 
            this.searchByVendorButton.Location = new System.Drawing.Point(192, 117);
            this.searchByVendorButton.Name = "searchByVendorButton";
            this.searchByVendorButton.Size = new System.Drawing.Size(53, 23);
            this.searchByVendorButton.TabIndex = 19;
            this.searchByVendorButton.Text = "GO!";
            this.searchByVendorButton.UseVisualStyleBackColor = true;
            this.searchByVendorButton.Click += new System.EventHandler(this.searchByVendor_Click);
            // 
            // staticByVendorLabel
            // 
            this.staticByVendorLabel.AutoSize = true;
            this.staticByVendorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByVendorLabel.Location = new System.Drawing.Point(6, 122);
            this.staticByVendorLabel.Name = "staticByVendorLabel";
            this.staticByVendorLabel.Size = new System.Drawing.Size(69, 13);
            this.staticByVendorLabel.TabIndex = 18;
            this.staticByVendorLabel.Text = "By Vendor:";
            // 
            // searchAllButton
            // 
            this.searchAllButton.BackColor = System.Drawing.Color.Goldenrod;
            this.searchAllButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.searchAllButton.Location = new System.Drawing.Point(253, 111);
            this.searchAllButton.Name = "searchAllButton";
            this.searchAllButton.Size = new System.Drawing.Size(95, 36);
            this.searchAllButton.TabIndex = 17;
            this.searchAllButton.Text = "ALL!";
            this.searchAllButton.UseVisualStyleBackColor = false;
            this.searchAllButton.Click += new System.EventHandler(this.searchAllButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.DarkRed;
            this.deleteButton.ForeColor = System.Drawing.Color.AntiqueWhite;
            this.deleteButton.Location = new System.Drawing.Point(253, 68);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(95, 36);
            this.deleteButton.TabIndex = 16;
            this.deleteButton.Text = "REMOVE!";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.ForestGreen;
            this.updateButton.Location = new System.Drawing.Point(253, 25);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(95, 36);
            this.updateButton.TabIndex = 15;
            this.updateButton.Text = "UPDATE!";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // searchNameInputBox
            // 
            this.searchNameInputBox.Location = new System.Drawing.Point(78, 76);
            this.searchNameInputBox.Name = "searchNameInputBox";
            this.searchNameInputBox.Size = new System.Drawing.Size(106, 21);
            this.searchNameInputBox.TabIndex = 9;
            // 
            // nameSearchButton
            // 
            this.nameSearchButton.Location = new System.Drawing.Point(191, 74);
            this.nameSearchButton.Name = "nameSearchButton";
            this.nameSearchButton.Size = new System.Drawing.Size(53, 23);
            this.nameSearchButton.TabIndex = 8;
            this.nameSearchButton.Text = "GO!";
            this.nameSearchButton.UseVisualStyleBackColor = true;
            this.nameSearchButton.Click += new System.EventHandler(this.nameSearchButton_Click);
            // 
            // staticByNameLabel
            // 
            this.staticByNameLabel.AutoSize = true;
            this.staticByNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByNameLabel.Location = new System.Drawing.Point(13, 80);
            this.staticByNameLabel.Name = "staticByNameLabel";
            this.staticByNameLabel.Size = new System.Drawing.Size(61, 13);
            this.staticByNameLabel.TabIndex = 7;
            this.staticByNameLabel.Text = "By Name:";
            // 
            // idInputBox
            // 
            this.idInputBox.Location = new System.Drawing.Point(78, 35);
            this.idInputBox.Name = "idInputBox";
            this.idInputBox.Size = new System.Drawing.Size(106, 21);
            this.idInputBox.TabIndex = 6;
            // 
            // idSearchButton
            // 
            this.idSearchButton.Location = new System.Drawing.Point(191, 34);
            this.idSearchButton.Name = "idSearchButton";
            this.idSearchButton.Size = new System.Drawing.Size(53, 23);
            this.idSearchButton.TabIndex = 5;
            this.idSearchButton.Text = "GO!";
            this.idSearchButton.UseVisualStyleBackColor = true;
            // 
            // staticByIdLabel
            // 
            this.staticByIdLabel.AutoSize = true;
            this.staticByIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByIdLabel.Location = new System.Drawing.Point(32, 37);
            this.staticByIdLabel.Name = "staticByIdLabel";
            this.staticByIdLabel.Size = new System.Drawing.Size(42, 13);
            this.staticByIdLabel.TabIndex = 0;
            this.staticByIdLabel.Text = "By ID:";
            // 
            // MaterialView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(762, 376);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "MaterialView";
            this.Text = "Materials";
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.materialTabGroupBox.ResumeLayout(false);
            this.materialTabGroupBox.PerformLayout();
            this.searchingSplitContainer.Panel1.ResumeLayout(false);
            this.searchingSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).EndInit();
            this.searchingSplitContainer.ResumeLayout(false);
            this.searchResultsGroupBox.ResumeLayout(false);
            this.searchPersonGroupBox.ResumeLayout(false);
            this.searchPersonGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer searchingSplitContainer;
        private System.Windows.Forms.GroupBox searchResultsGroupBox;
        private System.Windows.Forms.ListBox searchResultsListBox;
        private System.Windows.Forms.GroupBox searchPersonGroupBox;
        private System.Windows.Forms.Button searchAllButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.TextBox searchNameInputBox;
        private System.Windows.Forms.Button nameSearchButton;
        private System.Windows.Forms.Label staticByNameLabel;
        private System.Windows.Forms.TextBox idInputBox;
        private System.Windows.Forms.Button idSearchButton;
        private System.Windows.Forms.Label staticByIdLabel;
        private System.Windows.Forms.GroupBox materialTabGroupBox;
        private System.Windows.Forms.Button addMaterialButton;
        private System.Windows.Forms.Label staticMaterialDescLabel;
        private System.Windows.Forms.TextBox mDescriptionInputBox;
        private System.Windows.Forms.Label staticMaterialPartNumLabel;
        private System.Windows.Forms.TextBox partNumInputBox;
        private System.Windows.Forms.Label staticMaterialColorsLabel;
        private System.Windows.Forms.Label staticMaterialWeightLabel;
        private System.Windows.Forms.TextBox materialWeightInputBox;
        private System.Windows.Forms.Label staticMaterialInventoryLabel;
        private System.Windows.Forms.TextBox materialInvInputBox;
        private System.Windows.Forms.Label staticMaterialCostLabel;
        private System.Windows.Forms.TextBox materialCostInputBox;
        private System.Windows.Forms.Label staticVendorLabel;
        private System.Windows.Forms.Label staticMaterialNameLabel;
        private System.Windows.Forms.TextBox mNameInputBox;
        private System.Windows.Forms.ComboBox vendorComboBox;
        private System.Windows.Forms.ComboBox materialColorComboBox;
        private System.Windows.Forms.Button searchByVendorButton;
        private System.Windows.Forms.Label staticByVendorLabel;
        private System.Windows.Forms.ComboBox vendorSearchComboBox;
    }
}
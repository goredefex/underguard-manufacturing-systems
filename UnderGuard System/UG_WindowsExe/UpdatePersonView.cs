﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class UpdatePersonView : Form {

        //Update Controls
        private Contact cp;
        private Supplier sp;
        
        private bool isAContact = true;

        //====================================================
        //Constructs -----------------------------------------
        //====================================================

        public UpdatePersonView(Supplier ss) {
            InitializeComponent();
            this.sp = ss;

            if(this.sp!=null && this.sp.Worked) {
                fNameInputBox.Text = this.sp.FName;
                lNameInputBox.Text = this.sp.LName;
                countryInputBox.Text = this.sp.Country;
                provStateInputBox.Text = this.sp.ProvState;
                phNumInputBox.Text = this.sp.PhNumber;
                employerInputBox.Text = this.sp.Employer;
                zipInputBox.Text = this.sp.PostZip;
                emailInputBox.Text = this.sp.Email;
                notesInputBox.Text = this.sp.Notes;
                this.isAContact = false;
            }

        } //end constructor


        public UpdatePersonView(Contact cc) {
            InitializeComponent();
            this.cp = cc;

            if(this.cp!=null && this.cp.Worked) {
                fNameInputBox.Text = this.cp.FName;
                lNameInputBox.Text = this.cp.LName;
                countryInputBox.Text = this.cp.Country;
                provStateInputBox.Text = this.cp.ProvState;
                phNumInputBox.Text = this.cp.PhNumber;
                employerInputBox.Text = this.cp.Employer;
                zipInputBox.Text = this.cp.PostZip;
                emailInputBox.Text = this.cp.Email;
                notesInputBox.Text = this.cp.Notes;
            }

        } //end constructor

        //====================================================



        //====================================================
        //Functions ------------------------------------------
        //====================================================

        private void updateButton_Click(object sender, EventArgs e) {
            bool valid = true;
            string msg = "Your Missing: ";

            //Validation
            if(fNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| First Name |"; }
            if(lNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Last Name |"; }
            if(phNumInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Phone Num |"; }
            if(countryInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Country |"; }
            if(provStateInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Province/State |"; }
            if(employerInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Employer |"; }
            if(emailInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Email |"; }
            if(zipInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Post/Zip |"; }
            if(notesInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Notes |"; }

            //UPDATE PERSON
            if(valid) {
                
                int ok = 1;
                if(this.isAContact) {
                    ok = this.cp.UpdateAContactById(Int32.Parse(this.cp.ID), fNameInputBox.Text, lNameInputBox.Text, phNumInputBox.Text,
                                                    countryInputBox.Text, provStateInputBox.Text, employerInputBox.Text,
                                                    emailInputBox.Text, zipInputBox.Text, notesInputBox.Text, new byte(), new byte(),
                                                    new int());
                    this.cp = null;
                
                } else {
                    ok = this.sp.UpdateASupplierByID(Int32.Parse(this.sp.ID), fNameInputBox.Text, lNameInputBox.Text, phNumInputBox.Text,
                                                     countryInputBox.Text, provStateInputBox.Text, employerInputBox.Text,
                                                     emailInputBox.Text, zipInputBox.Text, notesInputBox.Text, new int(), new int(), 
                                                     new int());
                    this.sp = null;
                
                }

                if(ok<0) {
                    //Clear Controls
                    fNameInputBox.Clear();
                    lNameInputBox.Clear();
                    phNumInputBox.Clear();
                    countryInputBox.Clear();
                    provStateInputBox.Clear();
                    employerInputBox.Clear();
                    emailInputBox.Clear();
                    zipInputBox.Clear();
                    notesInputBox.Clear();
                    this.Close();
                }

            } else { 
                msg += " Fields, Please Re-Enter...";
                MessageBox.Show(msg);
            }

        } //end function

        //====================================================


    } //EOC

} //EON

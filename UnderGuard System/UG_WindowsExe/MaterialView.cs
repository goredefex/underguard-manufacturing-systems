﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class MaterialView : Form {


        //Stored Entry Objects
        private List<Supplier> dropDownVendors = null;
        private List<Material> allMaterialsFound = null;
        
        
        //==============================================
        //Constructs -----------------------------------
        //==============================================

        public MaterialView() {
            InitializeComponent();
            this.ShowAllVendors(vendorComboBox);
            this.ShowAllVendors(vendorSearchComboBox);
            this.ShowAllMaterials();
       
        } //end constructor

        //==============================================

        

        //==============================================
        //Funtions -------------------------------------
        //==============================================

        /// <summary>
        /// Display All Materials To Display
        /// </summary>
        private void ShowAllMaterials() {
            searchResultsListBox.Items.Clear();
            Material mm = new Material(); //<--Make Empty
            this.allMaterialsFound = mm.GetAllMaterialsObjs();

            for(int i=0; i<this.allMaterialsFound.Count; i++) {
                searchResultsListBox.Items.Add(this.allMaterialsFound[i].Name 
                    + "  ID: " + this.allMaterialsFound[i].MaterialId);
            }
        
        } //end function


        /// <summary>
        /// Display All Vendors In Drop Down
        /// </summary>
        public void ShowAllVendors(ComboBox inCC) {
            Supplier ss = new Supplier(); //<--Make Empty
            this.dropDownVendors = ss.GetAllSuppliersList();

            for(int i=0; i<dropDownVendors.Count; i++) {
                inCC.Items.Add(dropDownVendors[i].FName + " " + dropDownVendors[i].LName);
            }
        
        } //end function
               

        /// <summary>
        /// Translates Color Drop Downs
        /// Into String Types
        /// </summary>
        /// <param name="currIndex"></param>
        /// <returns></returns>
        public string GetColor(int currIndex) {
            string color = String.Empty;
            switch(currIndex) {
                case 0:
                    color = "Red";
                    break;
                case 1:
                    color = "Green";
                    break;
                case 2:
                    color = "Blue";
                    break;
                case 3:
                    color = "Yellow";
                    break;
                case 4:
                    color = "Chrome";
                    break;
                case 5:
                    color = "Black";
                    break;
                case 6:
                    color = "White";
                    break;
                default:
                    color = "None";
                    break;
            }
        
            return color;

        } //end function

        //==============================================

        

        //==============================================
        //Events ---------------------------------------
        //==============================================


        /// <summary>
        /// Will Find All Materials
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchAllButton_Click(object sender, EventArgs e) { 
            this.ShowAllMaterials(); 
        } //end event


        /// <summary>
        /// Deletes Selected Material
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void deleteButton_Click(object sender, EventArgs e) {
            if(searchResultsListBox.SelectedIndex>0) {
                try {
                    Material mm = new Material(); //<--Make Empty    
                    //DELETE MATERIAL!
                    mm.DeleteMaterial(this.allMaterialsFound[
                        searchResultsListBox.SelectedIndex].MaterialId);
                    MessageBox.Show("Material Deleted!");

                } catch { MessageBox.Show("Could Not Remove Material"); }

            } else { MessageBox.Show("Please Select A Material First!"); }
                        
        } //end function


        /// <summary>
        /// Finds Material By Name Input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nameSearchButton_Click(object sender, EventArgs e) {
            bool valid = true;
            
            if(nameSearchButton.Text.CompareTo("")==0) { valid = false; }

            if(valid) {
                Material mk = new Material(searchNameInputBox.Text);
                if(mk.Worked) {
                    searchResultsListBox.Items.Clear();
                    searchResultsListBox.Items.Add(mk.Name 
                        + "  ID: " + mk.MaterialId);                
                }

            } else { MessageBox.Show("Please Enter Text Into Name Search"); }

        } //end function
        

        /// <summary>
        /// Searchs For All Materials By Vendor ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchByVendor_Click(object sender, EventArgs e) {
            bool valid = true;
            
            if(vendorSearchComboBox.SelectedIndex<0) { valid = false; }

            if(valid) {
                searchResultsListBox.Items.Clear();
                Material mm = new Material(); //<--Make Empty
                this.allMaterialsFound = mm.GetAllMaterialsByVendorId(
                    Int32.Parse(
                        this.dropDownVendors[vendorSearchComboBox.SelectedIndex].SupplierId));
                
                for(int i=0; i<this.allMaterialsFound.Count; i++) {
                    searchResultsListBox.Items.Add(this.allMaterialsFound[i].Name 
                        + "  ID: " + this.allMaterialsFound[i].MaterialId);
                }

            } else { MessageBox.Show("Please Choose A Valid Vendor First!"); }

        } //end event


        /// <summary>
        /// Add A 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addMaterialButton_Click(object sender, EventArgs e) {

            bool valid = true;
            string msg = "You Are Missing: ";

            //Validation
            if(mNameInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Name |"; }
            if(vendorComboBox.SelectedIndex<0) { valid=false; msg+="| Vendor Id |"; }
            if(materialCostInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Cost |"; }
            if(materialInvInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Inventory Count |"; }
            if(materialWeightInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Weight |"; }
            if(materialColorComboBox.SelectedIndex<0) { valid=false; msg+="| Material Color |"; }
            if(partNumInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Part Number |"; }
            if(mDescriptionInputBox.Text.CompareTo("")==0) { valid=false; msg+="| Material Description |"; }
            
            //ADD CONTACT
            if(valid) {
                //Instantiate
                Material mm = new Material();
                //--

                //Update Material Info
                mm.Name = mNameInputBox.Text;
                mm.Description = mDescriptionInputBox.Text;
                mm.VendorId = Int32.Parse(this.dropDownVendors[vendorComboBox.SelectedIndex].SupplierId);
                mm.Cost = Int32.Parse(materialCostInputBox.Text);
                mm.Count = Int32.Parse(materialInvInputBox.Text);
                mm.Weight = Int32.Parse(materialWeightInputBox.Text);
                mm.Color = this.GetColor(materialColorComboBox.SelectedIndex);
                mm.PartNum = partNumInputBox.Text;
                                
                //INSERT MATERIAL!
                int ok = mm.AddMaterial();
                //--

                if(ok<0) {
                    mNameInputBox.Clear();
                    mDescriptionInputBox.Clear();
                    vendorComboBox.SelectedIndex = -1;
                    materialCostInputBox.Clear();
                    materialInvInputBox.Clear();
                    materialWeightInputBox.Clear();
                    materialColorComboBox.SelectedIndex = -1;
                    partNumInputBox.Clear();
                    MessageBox.Show("Supplier Added!"); //<--UI MSG

                } else { MessageBox.Show("Material Could Not Be Successfully Added... "+
                                            "Check Information Entered Is Correct"); }
                     
            } else {
                msg += " Fields, Please Enter And Continue";
                MessageBox.Show(msg);
            }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void updateButton_Click(object sender, EventArgs e) {

            if(searchResultsListBox.SelectedIndex>=0) {
                UpdateMaterialView uM = new UpdateMaterialView(
                    this.allMaterialsFound[searchResultsListBox.SelectedIndex]);
                uM.ShowDialog();

            } else { MessageBox.Show("Please Choose A Material First!"); }

        } //end event


        
        //==============================================


    } //EOC

} //EON

﻿namespace UG_WindowsExe {
    partial class PersonView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.addPersonTabControl = new System.Windows.Forms.TabControl();
            this.addContactTab = new System.Windows.Forms.TabPage();
            this.contactTabGroupBox = new System.Windows.Forms.GroupBox();
            this.staticHandlerIDLabel = new System.Windows.Forms.Label();
            this.handlerIDInputBox = new System.Windows.Forms.TextBox();
            this.ordersPendingLabel = new System.Windows.Forms.Label();
            this.orderPendingCheckBox = new System.Windows.Forms.CheckBox();
            this.isHandlerLabel = new System.Windows.Forms.Label();
            this.contactHandlerCheckBox = new System.Windows.Forms.CheckBox();
            this.addContactButton = new System.Windows.Forms.Button();
            this.staticNotesLabel = new System.Windows.Forms.Label();
            this.notesInputBox = new System.Windows.Forms.TextBox();
            this.staticEmailLabel = new System.Windows.Forms.Label();
            this.emailInputBox = new System.Windows.Forms.TextBox();
            this.staticZipLabel = new System.Windows.Forms.Label();
            this.zipInputBox = new System.Windows.Forms.TextBox();
            this.staticEmployerLabel = new System.Windows.Forms.Label();
            this.employerInputBox = new System.Windows.Forms.TextBox();
            this.staticPhoneLabel = new System.Windows.Forms.Label();
            this.phNumInputBox = new System.Windows.Forms.TextBox();
            this.staticProvStatLabel = new System.Windows.Forms.Label();
            this.provStateInputBox = new System.Windows.Forms.TextBox();
            this.staticCountryLabel = new System.Windows.Forms.Label();
            this.countryInputBox = new System.Windows.Forms.TextBox();
            this.staticLNameLabel = new System.Windows.Forms.Label();
            this.lNameInputBox = new System.Windows.Forms.TextBox();
            this.staticFNameLabel = new System.Windows.Forms.Label();
            this.fNameInputBox = new System.Windows.Forms.TextBox();
            this.addSupplierTab = new System.Windows.Forms.TabPage();
            this.supplierTabGroupBox = new System.Windows.Forms.GroupBox();
            this.noOrdersCheckBox = new System.Windows.Forms.CheckBox();
            this.failedCheckBox = new System.Windows.Forms.CheckBox();
            this.successCheckBox = new System.Windows.Forms.CheckBox();
            this.staticDeliveryLabel = new System.Windows.Forms.Label();
            this.staticOHSLabel = new System.Windows.Forms.Label();
            this.OHSInputBox = new System.Windows.Forms.TextBox();
            this.addSupplierButton = new System.Windows.Forms.Button();
            this.staticSupplierCommentsDisp = new System.Windows.Forms.Label();
            this.commentsSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierEmailDisp = new System.Windows.Forms.Label();
            this.emailSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierZipDisp = new System.Windows.Forms.Label();
            this.zipSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierEmployerDisp = new System.Windows.Forms.Label();
            this.employerSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierPhNumDisp = new System.Windows.Forms.Label();
            this.phNumSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierProvStateDisp = new System.Windows.Forms.Label();
            this.provStateSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierCountryDisp = new System.Windows.Forms.Label();
            this.countrySupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierLNameDisp = new System.Windows.Forms.Label();
            this.lNameSupplierInputBox = new System.Windows.Forms.TextBox();
            this.staticSupplierFNameDisp = new System.Windows.Forms.Label();
            this.fNameSupplierInputBox = new System.Windows.Forms.TextBox();
            this.searchingSplitContainer = new System.Windows.Forms.SplitContainer();
            this.searchResultsGroupBox = new System.Windows.Forms.GroupBox();
            this.searchResultsListBox = new System.Windows.Forms.ListBox();
            this.searchPersonGroupBox = new System.Windows.Forms.GroupBox();
            this.searchAllButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.staticSearchTypeLabel = new System.Windows.Forms.Label();
            this.searchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.staticLastNameInputLabel = new System.Windows.Forms.Label();
            this.staticFirstNameInputLabel = new System.Windows.Forms.Label();
            this.searchLNameInputBox = new System.Windows.Forms.TextBox();
            this.searchFNameInputBox = new System.Windows.Forms.TextBox();
            this.nameSearchButton = new System.Windows.Forms.Button();
            this.staticByNameLabel = new System.Windows.Forms.Label();
            this.idInputBox = new System.Windows.Forms.TextBox();
            this.idSearchButton = new System.Windows.Forms.Button();
            this.staticByIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.addPersonTabControl.SuspendLayout();
            this.addContactTab.SuspendLayout();
            this.contactTabGroupBox.SuspendLayout();
            this.addSupplierTab.SuspendLayout();
            this.supplierTabGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).BeginInit();
            this.searchingSplitContainer.Panel1.SuspendLayout();
            this.searchingSplitContainer.Panel2.SuspendLayout();
            this.searchingSplitContainer.SuspendLayout();
            this.searchResultsGroupBox.SuspendLayout();
            this.searchPersonGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.addPersonTabControl);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.searchingSplitContainer);
            this.mainSplitContainer.Size = new System.Drawing.Size(847, 473);
            this.mainSplitContainer.SplitterDistance = 474;
            this.mainSplitContainer.TabIndex = 0;
            // 
            // addPersonTabControl
            // 
            this.addPersonTabControl.Controls.Add(this.addContactTab);
            this.addPersonTabControl.Controls.Add(this.addSupplierTab);
            this.addPersonTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPersonTabControl.Location = new System.Drawing.Point(0, 0);
            this.addPersonTabControl.Name = "addPersonTabControl";
            this.addPersonTabControl.SelectedIndex = 0;
            this.addPersonTabControl.Size = new System.Drawing.Size(474, 473);
            this.addPersonTabControl.TabIndex = 19;
            // 
            // addContactTab
            // 
            this.addContactTab.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.addContactTab.Controls.Add(this.contactTabGroupBox);
            this.addContactTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addContactTab.Location = new System.Drawing.Point(4, 22);
            this.addContactTab.Name = "addContactTab";
            this.addContactTab.Padding = new System.Windows.Forms.Padding(3);
            this.addContactTab.Size = new System.Drawing.Size(466, 447);
            this.addContactTab.TabIndex = 0;
            this.addContactTab.Text = "Add Contact";
            // 
            // contactTabGroupBox
            // 
            this.contactTabGroupBox.Controls.Add(this.staticHandlerIDLabel);
            this.contactTabGroupBox.Controls.Add(this.handlerIDInputBox);
            this.contactTabGroupBox.Controls.Add(this.ordersPendingLabel);
            this.contactTabGroupBox.Controls.Add(this.orderPendingCheckBox);
            this.contactTabGroupBox.Controls.Add(this.isHandlerLabel);
            this.contactTabGroupBox.Controls.Add(this.contactHandlerCheckBox);
            this.contactTabGroupBox.Controls.Add(this.addContactButton);
            this.contactTabGroupBox.Controls.Add(this.staticNotesLabel);
            this.contactTabGroupBox.Controls.Add(this.notesInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticEmailLabel);
            this.contactTabGroupBox.Controls.Add(this.emailInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticZipLabel);
            this.contactTabGroupBox.Controls.Add(this.zipInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticEmployerLabel);
            this.contactTabGroupBox.Controls.Add(this.employerInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticPhoneLabel);
            this.contactTabGroupBox.Controls.Add(this.phNumInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticProvStatLabel);
            this.contactTabGroupBox.Controls.Add(this.provStateInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticCountryLabel);
            this.contactTabGroupBox.Controls.Add(this.countryInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticLNameLabel);
            this.contactTabGroupBox.Controls.Add(this.lNameInputBox);
            this.contactTabGroupBox.Controls.Add(this.staticFNameLabel);
            this.contactTabGroupBox.Controls.Add(this.fNameInputBox);
            this.contactTabGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contactTabGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contactTabGroupBox.Location = new System.Drawing.Point(3, 3);
            this.contactTabGroupBox.Name = "contactTabGroupBox";
            this.contactTabGroupBox.Size = new System.Drawing.Size(460, 441);
            this.contactTabGroupBox.TabIndex = 6;
            this.contactTabGroupBox.TabStop = false;
            this.contactTabGroupBox.Text = "Enter A Contact";
            // 
            // staticHandlerIDLabel
            // 
            this.staticHandlerIDLabel.AutoSize = true;
            this.staticHandlerIDLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticHandlerIDLabel.Location = new System.Drawing.Point(276, 248);
            this.staticHandlerIDLabel.Name = "staticHandlerIDLabel";
            this.staticHandlerIDLabel.Size = new System.Drawing.Size(28, 13);
            this.staticHandlerIDLabel.TabIndex = 26;
            this.staticHandlerIDLabel.Text = "ID#";
            // 
            // handlerIDInputBox
            // 
            this.handlerIDInputBox.Location = new System.Drawing.Point(310, 243);
            this.handlerIDInputBox.Name = "handlerIDInputBox";
            this.handlerIDInputBox.Size = new System.Drawing.Size(105, 21);
            this.handlerIDInputBox.TabIndex = 25;
            // 
            // ordersPendingLabel
            // 
            this.ordersPendingLabel.AutoSize = true;
            this.ordersPendingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ordersPendingLabel.Location = new System.Drawing.Point(46, 272);
            this.ordersPendingLabel.Name = "ordersPendingLabel";
            this.ordersPendingLabel.Size = new System.Drawing.Size(98, 13);
            this.ordersPendingLabel.TabIndex = 24;
            this.ordersPendingLabel.Text = "Orders Pending:";
            // 
            // orderPendingCheckBox
            // 
            this.orderPendingCheckBox.AutoSize = true;
            this.orderPendingCheckBox.Location = new System.Drawing.Point(225, 271);
            this.orderPendingCheckBox.Name = "orderPendingCheckBox";
            this.orderPendingCheckBox.Size = new System.Drawing.Size(49, 19);
            this.orderPendingCheckBox.TabIndex = 23;
            this.orderPendingCheckBox.Text = "Yes";
            this.orderPendingCheckBox.UseVisualStyleBackColor = true;
            // 
            // isHandlerLabel
            // 
            this.isHandlerLabel.AutoSize = true;
            this.isHandlerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.isHandlerLabel.Location = new System.Drawing.Point(46, 249);
            this.isHandlerLabel.Name = "isHandlerLabel";
            this.isHandlerLabel.Size = new System.Drawing.Size(136, 13);
            this.isHandlerLabel.TabIndex = 22;
            this.isHandlerLabel.Text = "Is A Company Contact:";
            // 
            // contactHandlerCheckBox
            // 
            this.contactHandlerCheckBox.AutoSize = true;
            this.contactHandlerCheckBox.Location = new System.Drawing.Point(224, 245);
            this.contactHandlerCheckBox.Name = "contactHandlerCheckBox";
            this.contactHandlerCheckBox.Size = new System.Drawing.Size(49, 19);
            this.contactHandlerCheckBox.TabIndex = 21;
            this.contactHandlerCheckBox.Text = "Yes";
            this.contactHandlerCheckBox.UseVisualStyleBackColor = true;
            // 
            // addContactButton
            // 
            this.addContactButton.BackColor = System.Drawing.Color.Aquamarine;
            this.addContactButton.Location = new System.Drawing.Point(59, 359);
            this.addContactButton.Name = "addContactButton";
            this.addContactButton.Size = new System.Drawing.Size(88, 48);
            this.addContactButton.TabIndex = 18;
            this.addContactButton.Text = "Add!";
            this.addContactButton.UseVisualStyleBackColor = false;
            this.addContactButton.Click += new System.EventHandler(this.addContactButton_Click);
            // 
            // staticNotesLabel
            // 
            this.staticNotesLabel.AutoSize = true;
            this.staticNotesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticNotesLabel.Location = new System.Drawing.Point(46, 297);
            this.staticNotesLabel.Name = "staticNotesLabel";
            this.staticNotesLabel.Size = new System.Drawing.Size(116, 13);
            this.staticNotesLabel.TabIndex = 17;
            this.staticNotesLabel.Text = "Related Comments:";
            // 
            // notesInputBox
            // 
            this.notesInputBox.Location = new System.Drawing.Point(168, 297);
            this.notesInputBox.Multiline = true;
            this.notesInputBox.Name = "notesInputBox";
            this.notesInputBox.Size = new System.Drawing.Size(248, 138);
            this.notesInputBox.TabIndex = 16;
            // 
            // staticEmailLabel
            // 
            this.staticEmailLabel.AutoSize = true;
            this.staticEmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticEmailLabel.Location = new System.Drawing.Point(46, 224);
            this.staticEmailLabel.Name = "staticEmailLabel";
            this.staticEmailLabel.Size = new System.Drawing.Size(41, 13);
            this.staticEmailLabel.TabIndex = 15;
            this.staticEmailLabel.Text = "Email:";
            // 
            // emailInputBox
            // 
            this.emailInputBox.Location = new System.Drawing.Point(222, 216);
            this.emailInputBox.Name = "emailInputBox";
            this.emailInputBox.Size = new System.Drawing.Size(194, 21);
            this.emailInputBox.TabIndex = 14;
            // 
            // staticZipLabel
            // 
            this.staticZipLabel.AutoSize = true;
            this.staticZipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticZipLabel.Location = new System.Drawing.Point(46, 197);
            this.staticZipLabel.Name = "staticZipLabel";
            this.staticZipLabel.Size = new System.Drawing.Size(85, 13);
            this.staticZipLabel.TabIndex = 13;
            this.staticZipLabel.Text = "PO/Zip Code:";
            // 
            // zipInputBox
            // 
            this.zipInputBox.Location = new System.Drawing.Point(222, 190);
            this.zipInputBox.Name = "zipInputBox";
            this.zipInputBox.Size = new System.Drawing.Size(194, 21);
            this.zipInputBox.TabIndex = 12;
            // 
            // staticEmployerLabel
            // 
            this.staticEmployerLabel.AutoSize = true;
            this.staticEmployerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticEmployerLabel.Location = new System.Drawing.Point(46, 169);
            this.staticEmployerLabel.Name = "staticEmployerLabel";
            this.staticEmployerLabel.Size = new System.Drawing.Size(62, 13);
            this.staticEmployerLabel.TabIndex = 11;
            this.staticEmployerLabel.Text = "Employer:";
            // 
            // employerInputBox
            // 
            this.employerInputBox.Location = new System.Drawing.Point(222, 162);
            this.employerInputBox.Name = "employerInputBox";
            this.employerInputBox.Size = new System.Drawing.Size(194, 21);
            this.employerInputBox.TabIndex = 10;
            // 
            // staticPhoneLabel
            // 
            this.staticPhoneLabel.AutoSize = true;
            this.staticPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPhoneLabel.Location = new System.Drawing.Point(46, 143);
            this.staticPhoneLabel.Name = "staticPhoneLabel";
            this.staticPhoneLabel.Size = new System.Drawing.Size(145, 13);
            this.staticPhoneLabel.TabIndex = 9;
            this.staticPhoneLabel.Text = "Phone Num (area code):";
            // 
            // phNumInputBox
            // 
            this.phNumInputBox.Location = new System.Drawing.Point(222, 136);
            this.phNumInputBox.Name = "phNumInputBox";
            this.phNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.phNumInputBox.TabIndex = 8;
            // 
            // staticProvStatLabel
            // 
            this.staticProvStatLabel.AutoSize = true;
            this.staticProvStatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticProvStatLabel.Location = new System.Drawing.Point(46, 116);
            this.staticProvStatLabel.Name = "staticProvStatLabel";
            this.staticProvStatLabel.Size = new System.Drawing.Size(101, 13);
            this.staticProvStatLabel.TabIndex = 7;
            this.staticProvStatLabel.Text = "Province/State: ";
            // 
            // provStateInputBox
            // 
            this.provStateInputBox.Location = new System.Drawing.Point(222, 109);
            this.provStateInputBox.Name = "provStateInputBox";
            this.provStateInputBox.Size = new System.Drawing.Size(194, 21);
            this.provStateInputBox.TabIndex = 6;
            // 
            // staticCountryLabel
            // 
            this.staticCountryLabel.AutoSize = true;
            this.staticCountryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticCountryLabel.Location = new System.Drawing.Point(46, 90);
            this.staticCountryLabel.Name = "staticCountryLabel";
            this.staticCountryLabel.Size = new System.Drawing.Size(58, 13);
            this.staticCountryLabel.TabIndex = 5;
            this.staticCountryLabel.Text = "Country: ";
            // 
            // countryInputBox
            // 
            this.countryInputBox.Location = new System.Drawing.Point(222, 83);
            this.countryInputBox.Name = "countryInputBox";
            this.countryInputBox.Size = new System.Drawing.Size(194, 21);
            this.countryInputBox.TabIndex = 4;
            // 
            // staticLNameLabel
            // 
            this.staticLNameLabel.AutoSize = true;
            this.staticLNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticLNameLabel.Location = new System.Drawing.Point(46, 62);
            this.staticLNameLabel.Name = "staticLNameLabel";
            this.staticLNameLabel.Size = new System.Drawing.Size(71, 13);
            this.staticLNameLabel.TabIndex = 3;
            this.staticLNameLabel.Text = "last Name: ";
            // 
            // lNameInputBox
            // 
            this.lNameInputBox.Location = new System.Drawing.Point(222, 55);
            this.lNameInputBox.Name = "lNameInputBox";
            this.lNameInputBox.Size = new System.Drawing.Size(194, 21);
            this.lNameInputBox.TabIndex = 2;
            // 
            // staticFNameLabel
            // 
            this.staticFNameLabel.AutoSize = true;
            this.staticFNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticFNameLabel.Location = new System.Drawing.Point(46, 35);
            this.staticFNameLabel.Name = "staticFNameLabel";
            this.staticFNameLabel.Size = new System.Drawing.Size(75, 13);
            this.staticFNameLabel.TabIndex = 1;
            this.staticFNameLabel.Text = "First Name: ";
            // 
            // fNameInputBox
            // 
            this.fNameInputBox.Location = new System.Drawing.Point(222, 28);
            this.fNameInputBox.Name = "fNameInputBox";
            this.fNameInputBox.Size = new System.Drawing.Size(194, 21);
            this.fNameInputBox.TabIndex = 0;
            // 
            // addSupplierTab
            // 
            this.addSupplierTab.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.addSupplierTab.Controls.Add(this.supplierTabGroupBox);
            this.addSupplierTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSupplierTab.Location = new System.Drawing.Point(4, 22);
            this.addSupplierTab.Name = "addSupplierTab";
            this.addSupplierTab.Padding = new System.Windows.Forms.Padding(3);
            this.addSupplierTab.Size = new System.Drawing.Size(466, 447);
            this.addSupplierTab.TabIndex = 1;
            this.addSupplierTab.Text = "Add Supplier";
            // 
            // supplierTabGroupBox
            // 
            this.supplierTabGroupBox.Controls.Add(this.noOrdersCheckBox);
            this.supplierTabGroupBox.Controls.Add(this.failedCheckBox);
            this.supplierTabGroupBox.Controls.Add(this.successCheckBox);
            this.supplierTabGroupBox.Controls.Add(this.staticDeliveryLabel);
            this.supplierTabGroupBox.Controls.Add(this.staticOHSLabel);
            this.supplierTabGroupBox.Controls.Add(this.OHSInputBox);
            this.supplierTabGroupBox.Controls.Add(this.addSupplierButton);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierCommentsDisp);
            this.supplierTabGroupBox.Controls.Add(this.commentsSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierEmailDisp);
            this.supplierTabGroupBox.Controls.Add(this.emailSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierZipDisp);
            this.supplierTabGroupBox.Controls.Add(this.zipSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierEmployerDisp);
            this.supplierTabGroupBox.Controls.Add(this.employerSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierPhNumDisp);
            this.supplierTabGroupBox.Controls.Add(this.phNumSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierProvStateDisp);
            this.supplierTabGroupBox.Controls.Add(this.provStateSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierCountryDisp);
            this.supplierTabGroupBox.Controls.Add(this.countrySupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierLNameDisp);
            this.supplierTabGroupBox.Controls.Add(this.lNameSupplierInputBox);
            this.supplierTabGroupBox.Controls.Add(this.staticSupplierFNameDisp);
            this.supplierTabGroupBox.Controls.Add(this.fNameSupplierInputBox);
            this.supplierTabGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.supplierTabGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplierTabGroupBox.Location = new System.Drawing.Point(3, 3);
            this.supplierTabGroupBox.Name = "supplierTabGroupBox";
            this.supplierTabGroupBox.Size = new System.Drawing.Size(460, 441);
            this.supplierTabGroupBox.TabIndex = 7;
            this.supplierTabGroupBox.TabStop = false;
            this.supplierTabGroupBox.Text = "Enter A Supplier";
            // 
            // noOrdersCheckBox
            // 
            this.noOrdersCheckBox.AutoSize = true;
            this.noOrdersCheckBox.Location = new System.Drawing.Point(243, 299);
            this.noOrdersCheckBox.Name = "noOrdersCheckBox";
            this.noOrdersCheckBox.Size = new System.Drawing.Size(148, 19);
            this.noOrdersCheckBox.TabIndex = 25;
            this.noOrdersCheckBox.Text = "No Order Made Yet";
            this.noOrdersCheckBox.UseVisualStyleBackColor = true;
            // 
            // failedCheckBox
            // 
            this.failedCheckBox.AutoSize = true;
            this.failedCheckBox.Location = new System.Drawing.Point(319, 274);
            this.failedCheckBox.Name = "failedCheckBox";
            this.failedCheckBox.Size = new System.Drawing.Size(97, 19);
            this.failedCheckBox.TabIndex = 24;
            this.failedCheckBox.Text = "Not-Arrived";
            this.failedCheckBox.UseVisualStyleBackColor = true;
            // 
            // successCheckBox
            // 
            this.successCheckBox.AutoSize = true;
            this.successCheckBox.Location = new System.Drawing.Point(222, 274);
            this.successCheckBox.Name = "successCheckBox";
            this.successCheckBox.Size = new System.Drawing.Size(70, 19);
            this.successCheckBox.TabIndex = 23;
            this.successCheckBox.Text = "Arrived";
            this.successCheckBox.UseVisualStyleBackColor = true;
            // 
            // staticDeliveryLabel
            // 
            this.staticDeliveryLabel.AutoSize = true;
            this.staticDeliveryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticDeliveryLabel.Location = new System.Drawing.Point(46, 274);
            this.staticDeliveryLabel.Name = "staticDeliveryLabel";
            this.staticDeliveryLabel.Size = new System.Drawing.Size(97, 13);
            this.staticDeliveryLabel.TabIndex = 22;
            this.staticDeliveryLabel.Text = "Delivery Status:";
            // 
            // staticOHSLabel
            // 
            this.staticOHSLabel.AutoSize = true;
            this.staticOHSLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticOHSLabel.Location = new System.Drawing.Point(46, 250);
            this.staticOHSLabel.Name = "staticOHSLabel";
            this.staticOHSLabel.Size = new System.Drawing.Size(72, 13);
            this.staticOHSLabel.TabIndex = 21;
            this.staticOHSLabel.Text = "OHS Level:";
            // 
            // OHSInputBox
            // 
            this.OHSInputBox.Location = new System.Drawing.Point(222, 243);
            this.OHSInputBox.Name = "OHSInputBox";
            this.OHSInputBox.Size = new System.Drawing.Size(194, 21);
            this.OHSInputBox.TabIndex = 20;
            // 
            // addSupplierButton
            // 
            this.addSupplierButton.BackColor = System.Drawing.Color.Aquamarine;
            this.addSupplierButton.Location = new System.Drawing.Point(55, 350);
            this.addSupplierButton.Name = "addSupplierButton";
            this.addSupplierButton.Size = new System.Drawing.Size(88, 48);
            this.addSupplierButton.TabIndex = 19;
            this.addSupplierButton.Text = "Add!";
            this.addSupplierButton.UseVisualStyleBackColor = false;
            this.addSupplierButton.Click += new System.EventHandler(this.addSupplierButton_Click);
            // 
            // staticSupplierCommentsDisp
            // 
            this.staticSupplierCommentsDisp.AutoSize = true;
            this.staticSupplierCommentsDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierCommentsDisp.Location = new System.Drawing.Point(46, 324);
            this.staticSupplierCommentsDisp.Name = "staticSupplierCommentsDisp";
            this.staticSupplierCommentsDisp.Size = new System.Drawing.Size(116, 13);
            this.staticSupplierCommentsDisp.TabIndex = 17;
            this.staticSupplierCommentsDisp.Text = "Related Comments:";
            // 
            // commentsSupplierInputBox
            // 
            this.commentsSupplierInputBox.Location = new System.Drawing.Point(168, 324);
            this.commentsSupplierInputBox.Multiline = true;
            this.commentsSupplierInputBox.Name = "commentsSupplierInputBox";
            this.commentsSupplierInputBox.Size = new System.Drawing.Size(248, 94);
            this.commentsSupplierInputBox.TabIndex = 16;
            // 
            // staticSupplierEmailDisp
            // 
            this.staticSupplierEmailDisp.AutoSize = true;
            this.staticSupplierEmailDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierEmailDisp.Location = new System.Drawing.Point(46, 223);
            this.staticSupplierEmailDisp.Name = "staticSupplierEmailDisp";
            this.staticSupplierEmailDisp.Size = new System.Drawing.Size(41, 13);
            this.staticSupplierEmailDisp.TabIndex = 15;
            this.staticSupplierEmailDisp.Text = "Email:";
            // 
            // emailSupplierInputBox
            // 
            this.emailSupplierInputBox.Location = new System.Drawing.Point(222, 216);
            this.emailSupplierInputBox.Name = "emailSupplierInputBox";
            this.emailSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.emailSupplierInputBox.TabIndex = 14;
            // 
            // staticSupplierZipDisp
            // 
            this.staticSupplierZipDisp.AutoSize = true;
            this.staticSupplierZipDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierZipDisp.Location = new System.Drawing.Point(46, 197);
            this.staticSupplierZipDisp.Name = "staticSupplierZipDisp";
            this.staticSupplierZipDisp.Size = new System.Drawing.Size(85, 13);
            this.staticSupplierZipDisp.TabIndex = 13;
            this.staticSupplierZipDisp.Text = "PO/Zip Code:";
            // 
            // zipSupplierInputBox
            // 
            this.zipSupplierInputBox.Location = new System.Drawing.Point(222, 190);
            this.zipSupplierInputBox.Name = "zipSupplierInputBox";
            this.zipSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.zipSupplierInputBox.TabIndex = 12;
            // 
            // staticSupplierEmployerDisp
            // 
            this.staticSupplierEmployerDisp.AutoSize = true;
            this.staticSupplierEmployerDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierEmployerDisp.Location = new System.Drawing.Point(46, 169);
            this.staticSupplierEmployerDisp.Name = "staticSupplierEmployerDisp";
            this.staticSupplierEmployerDisp.Size = new System.Drawing.Size(62, 13);
            this.staticSupplierEmployerDisp.TabIndex = 11;
            this.staticSupplierEmployerDisp.Text = "Employer:";
            // 
            // employerSupplierInputBox
            // 
            this.employerSupplierInputBox.Location = new System.Drawing.Point(222, 162);
            this.employerSupplierInputBox.Name = "employerSupplierInputBox";
            this.employerSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.employerSupplierInputBox.TabIndex = 10;
            // 
            // staticSupplierPhNumDisp
            // 
            this.staticSupplierPhNumDisp.AutoSize = true;
            this.staticSupplierPhNumDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierPhNumDisp.Location = new System.Drawing.Point(46, 143);
            this.staticSupplierPhNumDisp.Name = "staticSupplierPhNumDisp";
            this.staticSupplierPhNumDisp.Size = new System.Drawing.Size(145, 13);
            this.staticSupplierPhNumDisp.TabIndex = 9;
            this.staticSupplierPhNumDisp.Text = "Phone Num (area code):";
            // 
            // phNumSupplierInputBox
            // 
            this.phNumSupplierInputBox.Location = new System.Drawing.Point(222, 136);
            this.phNumSupplierInputBox.Name = "phNumSupplierInputBox";
            this.phNumSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.phNumSupplierInputBox.TabIndex = 8;
            // 
            // staticSupplierProvStateDisp
            // 
            this.staticSupplierProvStateDisp.AutoSize = true;
            this.staticSupplierProvStateDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierProvStateDisp.Location = new System.Drawing.Point(46, 116);
            this.staticSupplierProvStateDisp.Name = "staticSupplierProvStateDisp";
            this.staticSupplierProvStateDisp.Size = new System.Drawing.Size(101, 13);
            this.staticSupplierProvStateDisp.TabIndex = 7;
            this.staticSupplierProvStateDisp.Text = "Province/State: ";
            // 
            // provStateSupplierInputBox
            // 
            this.provStateSupplierInputBox.Location = new System.Drawing.Point(222, 109);
            this.provStateSupplierInputBox.Name = "provStateSupplierInputBox";
            this.provStateSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.provStateSupplierInputBox.TabIndex = 6;
            // 
            // staticSupplierCountryDisp
            // 
            this.staticSupplierCountryDisp.AutoSize = true;
            this.staticSupplierCountryDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierCountryDisp.Location = new System.Drawing.Point(46, 90);
            this.staticSupplierCountryDisp.Name = "staticSupplierCountryDisp";
            this.staticSupplierCountryDisp.Size = new System.Drawing.Size(58, 13);
            this.staticSupplierCountryDisp.TabIndex = 5;
            this.staticSupplierCountryDisp.Text = "Country: ";
            // 
            // countrySupplierInputBox
            // 
            this.countrySupplierInputBox.Location = new System.Drawing.Point(222, 83);
            this.countrySupplierInputBox.Name = "countrySupplierInputBox";
            this.countrySupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.countrySupplierInputBox.TabIndex = 4;
            // 
            // staticSupplierLNameDisp
            // 
            this.staticSupplierLNameDisp.AutoSize = true;
            this.staticSupplierLNameDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierLNameDisp.Location = new System.Drawing.Point(46, 62);
            this.staticSupplierLNameDisp.Name = "staticSupplierLNameDisp";
            this.staticSupplierLNameDisp.Size = new System.Drawing.Size(71, 13);
            this.staticSupplierLNameDisp.TabIndex = 3;
            this.staticSupplierLNameDisp.Text = "last Name: ";
            // 
            // lNameSupplierInputBox
            // 
            this.lNameSupplierInputBox.Location = new System.Drawing.Point(222, 55);
            this.lNameSupplierInputBox.Name = "lNameSupplierInputBox";
            this.lNameSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.lNameSupplierInputBox.TabIndex = 2;
            // 
            // staticSupplierFNameDisp
            // 
            this.staticSupplierFNameDisp.AutoSize = true;
            this.staticSupplierFNameDisp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSupplierFNameDisp.Location = new System.Drawing.Point(46, 35);
            this.staticSupplierFNameDisp.Name = "staticSupplierFNameDisp";
            this.staticSupplierFNameDisp.Size = new System.Drawing.Size(75, 13);
            this.staticSupplierFNameDisp.TabIndex = 1;
            this.staticSupplierFNameDisp.Text = "First Name: ";
            // 
            // fNameSupplierInputBox
            // 
            this.fNameSupplierInputBox.Location = new System.Drawing.Point(222, 28);
            this.fNameSupplierInputBox.Name = "fNameSupplierInputBox";
            this.fNameSupplierInputBox.Size = new System.Drawing.Size(194, 21);
            this.fNameSupplierInputBox.TabIndex = 0;
            // 
            // searchingSplitContainer
            // 
            this.searchingSplitContainer.Location = new System.Drawing.Point(1, 0);
            this.searchingSplitContainer.Name = "searchingSplitContainer";
            this.searchingSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // searchingSplitContainer.Panel1
            // 
            this.searchingSplitContainer.Panel1.Controls.Add(this.searchResultsGroupBox);
            // 
            // searchingSplitContainer.Panel2
            // 
            this.searchingSplitContainer.Panel2.Controls.Add(this.searchPersonGroupBox);
            this.searchingSplitContainer.Size = new System.Drawing.Size(368, 473);
            this.searchingSplitContainer.SplitterDistance = 279;
            this.searchingSplitContainer.TabIndex = 0;
            // 
            // searchResultsGroupBox
            // 
            this.searchResultsGroupBox.Controls.Add(this.searchResultsListBox);
            this.searchResultsGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchResultsGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchResultsGroupBox.Name = "searchResultsGroupBox";
            this.searchResultsGroupBox.Size = new System.Drawing.Size(368, 279);
            this.searchResultsGroupBox.TabIndex = 0;
            this.searchResultsGroupBox.TabStop = false;
            this.searchResultsGroupBox.Text = "Search Results:";
            // 
            // searchResultsListBox
            // 
            this.searchResultsListBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchResultsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchResultsListBox.FormattingEnabled = true;
            this.searchResultsListBox.HorizontalScrollbar = true;
            this.searchResultsListBox.ItemHeight = 15;
            this.searchResultsListBox.Location = new System.Drawing.Point(3, 17);
            this.searchResultsListBox.Name = "searchResultsListBox";
            this.searchResultsListBox.Size = new System.Drawing.Size(362, 259);
            this.searchResultsListBox.TabIndex = 1;
            // 
            // searchPersonGroupBox
            // 
            this.searchPersonGroupBox.Controls.Add(this.searchAllButton);
            this.searchPersonGroupBox.Controls.Add(this.deleteButton);
            this.searchPersonGroupBox.Controls.Add(this.updateButton);
            this.searchPersonGroupBox.Controls.Add(this.staticSearchTypeLabel);
            this.searchPersonGroupBox.Controls.Add(this.searchTypeComboBox);
            this.searchPersonGroupBox.Controls.Add(this.staticLastNameInputLabel);
            this.searchPersonGroupBox.Controls.Add(this.staticFirstNameInputLabel);
            this.searchPersonGroupBox.Controls.Add(this.searchLNameInputBox);
            this.searchPersonGroupBox.Controls.Add(this.searchFNameInputBox);
            this.searchPersonGroupBox.Controls.Add(this.nameSearchButton);
            this.searchPersonGroupBox.Controls.Add(this.staticByNameLabel);
            this.searchPersonGroupBox.Controls.Add(this.idInputBox);
            this.searchPersonGroupBox.Controls.Add(this.idSearchButton);
            this.searchPersonGroupBox.Controls.Add(this.staticByIdLabel);
            this.searchPersonGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPersonGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchPersonGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchPersonGroupBox.Name = "searchPersonGroupBox";
            this.searchPersonGroupBox.Size = new System.Drawing.Size(368, 190);
            this.searchPersonGroupBox.TabIndex = 19;
            this.searchPersonGroupBox.TabStop = false;
            this.searchPersonGroupBox.Text = "Search For Person";
            // 
            // searchAllButton
            // 
            this.searchAllButton.BackColor = System.Drawing.Color.Goldenrod;
            this.searchAllButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.searchAllButton.Location = new System.Drawing.Point(241, 142);
            this.searchAllButton.Name = "searchAllButton";
            this.searchAllButton.Size = new System.Drawing.Size(95, 36);
            this.searchAllButton.TabIndex = 17;
            this.searchAllButton.Text = "ALL!";
            this.searchAllButton.UseVisualStyleBackColor = false;
            this.searchAllButton.Click += new System.EventHandler(this.searchAllButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.DarkRed;
            this.deleteButton.ForeColor = System.Drawing.Color.AntiqueWhite;
            this.deleteButton.Location = new System.Drawing.Point(130, 142);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(95, 36);
            this.deleteButton.TabIndex = 16;
            this.deleteButton.Text = "REMOVE!";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.ForestGreen;
            this.updateButton.Location = new System.Drawing.Point(18, 142);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(95, 36);
            this.updateButton.TabIndex = 15;
            this.updateButton.Text = "UPDATE!";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // staticSearchTypeLabel
            // 
            this.staticSearchTypeLabel.AutoSize = true;
            this.staticSearchTypeLabel.Location = new System.Drawing.Point(276, 16);
            this.staticSearchTypeLabel.Name = "staticSearchTypeLabel";
            this.staticSearchTypeLabel.Size = new System.Drawing.Size(75, 15);
            this.staticSearchTypeLabel.TabIndex = 14;
            this.staticSearchTypeLabel.Text = "Search By:";
            // 
            // searchTypeComboBox
            // 
            this.searchTypeComboBox.FormattingEnabled = true;
            this.searchTypeComboBox.Items.AddRange(new object[] {
            "Contact",
            "Supplier"});
            this.searchTypeComboBox.Location = new System.Drawing.Point(270, 33);
            this.searchTypeComboBox.Name = "searchTypeComboBox";
            this.searchTypeComboBox.Size = new System.Drawing.Size(87, 23);
            this.searchTypeComboBox.TabIndex = 13;
            this.searchTypeComboBox.Text = "Choose:";
            // 
            // staticLastNameInputLabel
            // 
            this.staticLastNameInputLabel.AutoSize = true;
            this.staticLastNameInputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticLastNameInputLabel.Location = new System.Drawing.Point(208, 101);
            this.staticLastNameInputLabel.Name = "staticLastNameInputLabel";
            this.staticLastNameInputLabel.Size = new System.Drawing.Size(67, 15);
            this.staticLastNameInputLabel.TabIndex = 12;
            this.staticLastNameInputLabel.Text = "Last Name";
            // 
            // staticFirstNameInputLabel
            // 
            this.staticFirstNameInputLabel.AutoSize = true;
            this.staticFirstNameInputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticFirstNameInputLabel.Location = new System.Drawing.Point(97, 101);
            this.staticFirstNameInputLabel.Name = "staticFirstNameInputLabel";
            this.staticFirstNameInputLabel.Size = new System.Drawing.Size(67, 15);
            this.staticFirstNameInputLabel.TabIndex = 11;
            this.staticFirstNameInputLabel.Text = "First Name";
            // 
            // searchLNameInputBox
            // 
            this.searchLNameInputBox.Location = new System.Drawing.Point(189, 78);
            this.searchLNameInputBox.Name = "searchLNameInputBox";
            this.searchLNameInputBox.Size = new System.Drawing.Size(106, 21);
            this.searchLNameInputBox.TabIndex = 10;
            // 
            // searchFNameInputBox
            // 
            this.searchFNameInputBox.Location = new System.Drawing.Point(79, 77);
            this.searchFNameInputBox.Name = "searchFNameInputBox";
            this.searchFNameInputBox.Size = new System.Drawing.Size(106, 21);
            this.searchFNameInputBox.TabIndex = 9;
            // 
            // nameSearchButton
            // 
            this.nameSearchButton.Location = new System.Drawing.Point(301, 77);
            this.nameSearchButton.Name = "nameSearchButton";
            this.nameSearchButton.Size = new System.Drawing.Size(53, 23);
            this.nameSearchButton.TabIndex = 8;
            this.nameSearchButton.Text = "GO!";
            this.nameSearchButton.UseVisualStyleBackColor = true;
            this.nameSearchButton.Click += new System.EventHandler(this.nameSearchButton_Click);
            // 
            // staticByNameLabel
            // 
            this.staticByNameLabel.AutoSize = true;
            this.staticByNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByNameLabel.Location = new System.Drawing.Point(15, 80);
            this.staticByNameLabel.Name = "staticByNameLabel";
            this.staticByNameLabel.Size = new System.Drawing.Size(61, 13);
            this.staticByNameLabel.TabIndex = 7;
            this.staticByNameLabel.Text = "By Name:";
            // 
            // idInputBox
            // 
            this.idInputBox.Location = new System.Drawing.Point(82, 37);
            this.idInputBox.Name = "idInputBox";
            this.idInputBox.Size = new System.Drawing.Size(106, 21);
            this.idInputBox.TabIndex = 6;
            // 
            // idSearchButton
            // 
            this.idSearchButton.Location = new System.Drawing.Point(198, 35);
            this.idSearchButton.Name = "idSearchButton";
            this.idSearchButton.Size = new System.Drawing.Size(53, 23);
            this.idSearchButton.TabIndex = 5;
            this.idSearchButton.Text = "GO!";
            this.idSearchButton.UseVisualStyleBackColor = true;
            this.idSearchButton.Click += new System.EventHandler(this.idSearchButton_Click);
            // 
            // staticByIdLabel
            // 
            this.staticByIdLabel.AutoSize = true;
            this.staticByIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByIdLabel.Location = new System.Drawing.Point(37, 39);
            this.staticByIdLabel.Name = "staticByIdLabel";
            this.staticByIdLabel.Size = new System.Drawing.Size(42, 13);
            this.staticByIdLabel.TabIndex = 0;
            this.staticByIdLabel.Text = "By ID:";
            // 
            // PersonView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 473);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "PersonView";
            this.Text = "Peronsal Information Console";
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.addPersonTabControl.ResumeLayout(false);
            this.addContactTab.ResumeLayout(false);
            this.contactTabGroupBox.ResumeLayout(false);
            this.contactTabGroupBox.PerformLayout();
            this.addSupplierTab.ResumeLayout(false);
            this.supplierTabGroupBox.ResumeLayout(false);
            this.supplierTabGroupBox.PerformLayout();
            this.searchingSplitContainer.Panel1.ResumeLayout(false);
            this.searchingSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).EndInit();
            this.searchingSplitContainer.ResumeLayout(false);
            this.searchResultsGroupBox.ResumeLayout(false);
            this.searchPersonGroupBox.ResumeLayout(false);
            this.searchPersonGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer searchingSplitContainer;
        private System.Windows.Forms.GroupBox searchPersonGroupBox;
        private System.Windows.Forms.Label staticLastNameInputLabel;
        private System.Windows.Forms.Label staticFirstNameInputLabel;
        private System.Windows.Forms.TextBox searchLNameInputBox;
        private System.Windows.Forms.TextBox searchFNameInputBox;
        private System.Windows.Forms.Button nameSearchButton;
        private System.Windows.Forms.Label staticByNameLabel;
        private System.Windows.Forms.TextBox idInputBox;
        private System.Windows.Forms.Button idSearchButton;
        private System.Windows.Forms.Label staticByIdLabel;
        private System.Windows.Forms.Label staticSearchTypeLabel;
        private System.Windows.Forms.ComboBox searchTypeComboBox;
        private System.Windows.Forms.GroupBox searchResultsGroupBox;
        private System.Windows.Forms.ListBox searchResultsListBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button searchAllButton;
        private System.Windows.Forms.TabControl addPersonTabControl;
        private System.Windows.Forms.TabPage addContactTab;
        private System.Windows.Forms.GroupBox contactTabGroupBox;
        private System.Windows.Forms.Label staticHandlerIDLabel;
        private System.Windows.Forms.TextBox handlerIDInputBox;
        private System.Windows.Forms.Label ordersPendingLabel;
        private System.Windows.Forms.CheckBox orderPendingCheckBox;
        private System.Windows.Forms.Label isHandlerLabel;
        private System.Windows.Forms.CheckBox contactHandlerCheckBox;
        private System.Windows.Forms.Button addContactButton;
        private System.Windows.Forms.Label staticNotesLabel;
        private System.Windows.Forms.TextBox notesInputBox;
        private System.Windows.Forms.Label staticEmailLabel;
        private System.Windows.Forms.TextBox emailInputBox;
        private System.Windows.Forms.Label staticZipLabel;
        private System.Windows.Forms.TextBox zipInputBox;
        private System.Windows.Forms.Label staticEmployerLabel;
        private System.Windows.Forms.TextBox employerInputBox;
        private System.Windows.Forms.Label staticPhoneLabel;
        private System.Windows.Forms.TextBox phNumInputBox;
        private System.Windows.Forms.Label staticProvStatLabel;
        private System.Windows.Forms.TextBox provStateInputBox;
        private System.Windows.Forms.Label staticCountryLabel;
        private System.Windows.Forms.TextBox countryInputBox;
        private System.Windows.Forms.Label staticLNameLabel;
        private System.Windows.Forms.TextBox lNameInputBox;
        private System.Windows.Forms.Label staticFNameLabel;
        private System.Windows.Forms.TextBox fNameInputBox;
        private System.Windows.Forms.TabPage addSupplierTab;
        private System.Windows.Forms.GroupBox supplierTabGroupBox;
        private System.Windows.Forms.CheckBox noOrdersCheckBox;
        private System.Windows.Forms.CheckBox failedCheckBox;
        private System.Windows.Forms.CheckBox successCheckBox;
        private System.Windows.Forms.Label staticDeliveryLabel;
        private System.Windows.Forms.Label staticOHSLabel;
        private System.Windows.Forms.TextBox OHSInputBox;
        private System.Windows.Forms.Button addSupplierButton;
        private System.Windows.Forms.Label staticSupplierCommentsDisp;
        private System.Windows.Forms.TextBox commentsSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierEmailDisp;
        private System.Windows.Forms.TextBox emailSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierZipDisp;
        private System.Windows.Forms.TextBox zipSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierEmployerDisp;
        private System.Windows.Forms.TextBox employerSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierPhNumDisp;
        private System.Windows.Forms.TextBox phNumSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierProvStateDisp;
        private System.Windows.Forms.TextBox provStateSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierCountryDisp;
        private System.Windows.Forms.TextBox countrySupplierInputBox;
        private System.Windows.Forms.Label staticSupplierLNameDisp;
        private System.Windows.Forms.TextBox lNameSupplierInputBox;
        private System.Windows.Forms.Label staticSupplierFNameDisp;
        private System.Windows.Forms.TextBox fNameSupplierInputBox;

    }
}


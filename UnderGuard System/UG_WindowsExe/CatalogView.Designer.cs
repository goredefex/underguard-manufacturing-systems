﻿namespace UG_WindowsExe {
    partial class CatalogView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.catalogGroupBox = new System.Windows.Forms.GroupBox();
            this.allDoneGroupBox = new System.Windows.Forms.GroupBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.removingGroupBox = new System.Windows.Forms.GroupBox();
            this.removeButton = new System.Windows.Forms.Button();
            this.addingGroupBox = new System.Windows.Forms.GroupBox();
            this.addButton = new System.Windows.Forms.Button();
            this.addedListBox = new System.Windows.Forms.ListBox();
            this.notAddedListBox = new System.Windows.Forms.ListBox();
            this.catalogGroupBox.SuspendLayout();
            this.allDoneGroupBox.SuspendLayout();
            this.removingGroupBox.SuspendLayout();
            this.addingGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // catalogGroupBox
            // 
            this.catalogGroupBox.Controls.Add(this.allDoneGroupBox);
            this.catalogGroupBox.Controls.Add(this.removingGroupBox);
            this.catalogGroupBox.Controls.Add(this.addingGroupBox);
            this.catalogGroupBox.Controls.Add(this.addedListBox);
            this.catalogGroupBox.Controls.Add(this.notAddedListBox);
            this.catalogGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.catalogGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.catalogGroupBox.Location = new System.Drawing.Point(0, 0);
            this.catalogGroupBox.Name = "catalogGroupBox";
            this.catalogGroupBox.Size = new System.Drawing.Size(585, 307);
            this.catalogGroupBox.TabIndex = 1;
            this.catalogGroupBox.TabStop = false;
            this.catalogGroupBox.Text = "Add Products To Catalog:";
            // 
            // allDoneGroupBox
            // 
            this.allDoneGroupBox.Controls.Add(this.doneButton);
            this.allDoneGroupBox.Location = new System.Drawing.Point(239, 247);
            this.allDoneGroupBox.Name = "allDoneGroupBox";
            this.allDoneGroupBox.Size = new System.Drawing.Size(94, 46);
            this.allDoneGroupBox.TabIndex = 7;
            this.allDoneGroupBox.TabStop = false;
            // 
            // doneButton
            // 
            this.doneButton.BackColor = System.Drawing.Color.LimeGreen;
            this.doneButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.doneButton.Location = new System.Drawing.Point(18, 11);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(60, 31);
            this.doneButton.TabIndex = 5;
            this.doneButton.Text = "OK!";
            this.doneButton.UseVisualStyleBackColor = false;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // removingGroupBox
            // 
            this.removingGroupBox.Controls.Add(this.removeButton);
            this.removingGroupBox.Location = new System.Drawing.Point(239, 135);
            this.removingGroupBox.Name = "removingGroupBox";
            this.removingGroupBox.Size = new System.Drawing.Size(94, 117);
            this.removingGroupBox.TabIndex = 6;
            this.removingGroupBox.TabStop = false;
            this.removingGroupBox.Text = "Remove From Catalog:";
            // 
            // removeButton
            // 
            this.removeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeButton.Location = new System.Drawing.Point(18, 61);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(60, 35);
            this.removeButton.TabIndex = 4;
            this.removeButton.Text = "<<";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // addingGroupBox
            // 
            this.addingGroupBox.Controls.Add(this.addButton);
            this.addingGroupBox.Location = new System.Drawing.Point(239, 20);
            this.addingGroupBox.Name = "addingGroupBox";
            this.addingGroupBox.Size = new System.Drawing.Size(94, 109);
            this.addingGroupBox.TabIndex = 5;
            this.addingGroupBox.TabStop = false;
            this.addingGroupBox.Text = "Add To Catalog:";
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(18, 51);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(60, 35);
            this.addButton.TabIndex = 6;
            this.addButton.Text = ">>";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // addedListBox
            // 
            this.addedListBox.FormattingEnabled = true;
            this.addedListBox.ItemHeight = 15;
            this.addedListBox.Location = new System.Drawing.Point(339, 20);
            this.addedListBox.Name = "addedListBox";
            this.addedListBox.Size = new System.Drawing.Size(221, 274);
            this.addedListBox.TabIndex = 1;
            // 
            // notAddedListBox
            // 
            this.notAddedListBox.FormattingEnabled = true;
            this.notAddedListBox.ItemHeight = 15;
            this.notAddedListBox.Location = new System.Drawing.Point(12, 20);
            this.notAddedListBox.Name = "notAddedListBox";
            this.notAddedListBox.Size = new System.Drawing.Size(221, 274);
            this.notAddedListBox.TabIndex = 0;
            // 
            // CatalogView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 307);
            this.Controls.Add(this.catalogGroupBox);
            this.Name = "CatalogView";
            this.Text = "Catalog";
            this.catalogGroupBox.ResumeLayout(false);
            this.allDoneGroupBox.ResumeLayout(false);
            this.removingGroupBox.ResumeLayout(false);
            this.addingGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox catalogGroupBox;
        private System.Windows.Forms.GroupBox allDoneGroupBox;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.GroupBox removingGroupBox;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.GroupBox addingGroupBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ListBox addedListBox;
        private System.Windows.Forms.ListBox notAddedListBox;
    }
}
﻿namespace UG_WindowsExe {
    partial class UpdatePersonView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.updateGroupBox = new System.Windows.Forms.GroupBox();
            this.contactCompanyIdInputBox = new System.Windows.Forms.TextBox();
            this.staticAddContactCompanyLabel = new System.Windows.Forms.Label();
            this.updateButton = new System.Windows.Forms.Button();
            this.staticNotesLabel = new System.Windows.Forms.Label();
            this.notesInputBox = new System.Windows.Forms.TextBox();
            this.staticEmailLabel = new System.Windows.Forms.Label();
            this.emailInputBox = new System.Windows.Forms.TextBox();
            this.staticZipLabel = new System.Windows.Forms.Label();
            this.zipInputBox = new System.Windows.Forms.TextBox();
            this.staticEmployerLabel = new System.Windows.Forms.Label();
            this.employerInputBox = new System.Windows.Forms.TextBox();
            this.staticPhoneLabel = new System.Windows.Forms.Label();
            this.phNumInputBox = new System.Windows.Forms.TextBox();
            this.staticProvStatLabel = new System.Windows.Forms.Label();
            this.provStateInputBox = new System.Windows.Forms.TextBox();
            this.staticCountryLabel = new System.Windows.Forms.Label();
            this.countryInputBox = new System.Windows.Forms.TextBox();
            this.staticLNameLabel = new System.Windows.Forms.Label();
            this.lNameInputBox = new System.Windows.Forms.TextBox();
            this.staticFNameLabel = new System.Windows.Forms.Label();
            this.fNameInputBox = new System.Windows.Forms.TextBox();
            this.updateGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateGroupBox
            // 
            this.updateGroupBox.Controls.Add(this.contactCompanyIdInputBox);
            this.updateGroupBox.Controls.Add(this.staticAddContactCompanyLabel);
            this.updateGroupBox.Controls.Add(this.updateButton);
            this.updateGroupBox.Controls.Add(this.staticNotesLabel);
            this.updateGroupBox.Controls.Add(this.notesInputBox);
            this.updateGroupBox.Controls.Add(this.staticEmailLabel);
            this.updateGroupBox.Controls.Add(this.emailInputBox);
            this.updateGroupBox.Controls.Add(this.staticZipLabel);
            this.updateGroupBox.Controls.Add(this.zipInputBox);
            this.updateGroupBox.Controls.Add(this.staticEmployerLabel);
            this.updateGroupBox.Controls.Add(this.employerInputBox);
            this.updateGroupBox.Controls.Add(this.staticPhoneLabel);
            this.updateGroupBox.Controls.Add(this.phNumInputBox);
            this.updateGroupBox.Controls.Add(this.staticProvStatLabel);
            this.updateGroupBox.Controls.Add(this.provStateInputBox);
            this.updateGroupBox.Controls.Add(this.staticCountryLabel);
            this.updateGroupBox.Controls.Add(this.countryInputBox);
            this.updateGroupBox.Controls.Add(this.staticLNameLabel);
            this.updateGroupBox.Controls.Add(this.lNameInputBox);
            this.updateGroupBox.Controls.Add(this.staticFNameLabel);
            this.updateGroupBox.Controls.Add(this.fNameInputBox);
            this.updateGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.updateGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.updateGroupBox.Location = new System.Drawing.Point(0, 0);
            this.updateGroupBox.Name = "updateGroupBox";
            this.updateGroupBox.Size = new System.Drawing.Size(451, 431);
            this.updateGroupBox.TabIndex = 8;
            this.updateGroupBox.TabStop = false;
            this.updateGroupBox.Text = "Update Fields:";
            // 
            // contactCompanyIdInputBox
            // 
            this.contactCompanyIdInputBox.Location = new System.Drawing.Point(217, 236);
            this.contactCompanyIdInputBox.Name = "contactCompanyIdInputBox";
            this.contactCompanyIdInputBox.Size = new System.Drawing.Size(194, 21);
            this.contactCompanyIdInputBox.TabIndex = 20;
            // 
            // staticAddContactCompanyLabel
            // 
            this.staticAddContactCompanyLabel.AutoSize = true;
            this.staticAddContactCompanyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticAddContactCompanyLabel.Location = new System.Drawing.Point(41, 237);
            this.staticAddContactCompanyLabel.Name = "staticAddContactCompanyLabel";
            this.staticAddContactCompanyLabel.Size = new System.Drawing.Size(87, 13);
            this.staticAddContactCompanyLabel.TabIndex = 19;
            this.staticAddContactCompanyLabel.Text = "Company ID#:";
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.ForestGreen;
            this.updateButton.ForeColor = System.Drawing.Color.Beige;
            this.updateButton.Location = new System.Drawing.Point(54, 359);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(88, 48);
            this.updateButton.TabIndex = 18;
            this.updateButton.Text = "UPDATE!";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // staticNotesLabel
            // 
            this.staticNotesLabel.AutoSize = true;
            this.staticNotesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticNotesLabel.Location = new System.Drawing.Point(41, 316);
            this.staticNotesLabel.Name = "staticNotesLabel";
            this.staticNotesLabel.Size = new System.Drawing.Size(116, 13);
            this.staticNotesLabel.TabIndex = 17;
            this.staticNotesLabel.Text = "Related Comments:";
            // 
            // notesInputBox
            // 
            this.notesInputBox.Location = new System.Drawing.Point(163, 316);
            this.notesInputBox.Multiline = true;
            this.notesInputBox.Name = "notesInputBox";
            this.notesInputBox.Size = new System.Drawing.Size(248, 108);
            this.notesInputBox.TabIndex = 16;
            // 
            // staticEmailLabel
            // 
            this.staticEmailLabel.AutoSize = true;
            this.staticEmailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticEmailLabel.Location = new System.Drawing.Point(41, 216);
            this.staticEmailLabel.Name = "staticEmailLabel";
            this.staticEmailLabel.Size = new System.Drawing.Size(41, 13);
            this.staticEmailLabel.TabIndex = 15;
            this.staticEmailLabel.Text = "Email:";
            // 
            // emailInputBox
            // 
            this.emailInputBox.Location = new System.Drawing.Point(217, 209);
            this.emailInputBox.Name = "emailInputBox";
            this.emailInputBox.Size = new System.Drawing.Size(194, 21);
            this.emailInputBox.TabIndex = 14;
            // 
            // staticZipLabel
            // 
            this.staticZipLabel.AutoSize = true;
            this.staticZipLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticZipLabel.Location = new System.Drawing.Point(41, 190);
            this.staticZipLabel.Name = "staticZipLabel";
            this.staticZipLabel.Size = new System.Drawing.Size(85, 13);
            this.staticZipLabel.TabIndex = 13;
            this.staticZipLabel.Text = "PO/Zip Code:";
            // 
            // zipInputBox
            // 
            this.zipInputBox.Location = new System.Drawing.Point(217, 183);
            this.zipInputBox.Name = "zipInputBox";
            this.zipInputBox.Size = new System.Drawing.Size(194, 21);
            this.zipInputBox.TabIndex = 12;
            // 
            // staticEmployerLabel
            // 
            this.staticEmployerLabel.AutoSize = true;
            this.staticEmployerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticEmployerLabel.Location = new System.Drawing.Point(41, 162);
            this.staticEmployerLabel.Name = "staticEmployerLabel";
            this.staticEmployerLabel.Size = new System.Drawing.Size(62, 13);
            this.staticEmployerLabel.TabIndex = 11;
            this.staticEmployerLabel.Text = "Employer:";
            // 
            // employerInputBox
            // 
            this.employerInputBox.Location = new System.Drawing.Point(217, 155);
            this.employerInputBox.Name = "employerInputBox";
            this.employerInputBox.Size = new System.Drawing.Size(194, 21);
            this.employerInputBox.TabIndex = 10;
            // 
            // staticPhoneLabel
            // 
            this.staticPhoneLabel.AutoSize = true;
            this.staticPhoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPhoneLabel.Location = new System.Drawing.Point(41, 136);
            this.staticPhoneLabel.Name = "staticPhoneLabel";
            this.staticPhoneLabel.Size = new System.Drawing.Size(145, 13);
            this.staticPhoneLabel.TabIndex = 9;
            this.staticPhoneLabel.Text = "Phone Num (area code):";
            // 
            // phNumInputBox
            // 
            this.phNumInputBox.Location = new System.Drawing.Point(217, 129);
            this.phNumInputBox.Name = "phNumInputBox";
            this.phNumInputBox.Size = new System.Drawing.Size(194, 21);
            this.phNumInputBox.TabIndex = 8;
            // 
            // staticProvStatLabel
            // 
            this.staticProvStatLabel.AutoSize = true;
            this.staticProvStatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticProvStatLabel.Location = new System.Drawing.Point(41, 109);
            this.staticProvStatLabel.Name = "staticProvStatLabel";
            this.staticProvStatLabel.Size = new System.Drawing.Size(101, 13);
            this.staticProvStatLabel.TabIndex = 7;
            this.staticProvStatLabel.Text = "Province/State: ";
            // 
            // provStateInputBox
            // 
            this.provStateInputBox.Location = new System.Drawing.Point(217, 102);
            this.provStateInputBox.Name = "provStateInputBox";
            this.provStateInputBox.Size = new System.Drawing.Size(194, 21);
            this.provStateInputBox.TabIndex = 6;
            // 
            // staticCountryLabel
            // 
            this.staticCountryLabel.AutoSize = true;
            this.staticCountryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticCountryLabel.Location = new System.Drawing.Point(41, 83);
            this.staticCountryLabel.Name = "staticCountryLabel";
            this.staticCountryLabel.Size = new System.Drawing.Size(58, 13);
            this.staticCountryLabel.TabIndex = 5;
            this.staticCountryLabel.Text = "Country: ";
            // 
            // countryInputBox
            // 
            this.countryInputBox.Location = new System.Drawing.Point(217, 76);
            this.countryInputBox.Name = "countryInputBox";
            this.countryInputBox.Size = new System.Drawing.Size(194, 21);
            this.countryInputBox.TabIndex = 4;
            // 
            // staticLNameLabel
            // 
            this.staticLNameLabel.AutoSize = true;
            this.staticLNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticLNameLabel.Location = new System.Drawing.Point(41, 55);
            this.staticLNameLabel.Name = "staticLNameLabel";
            this.staticLNameLabel.Size = new System.Drawing.Size(71, 13);
            this.staticLNameLabel.TabIndex = 3;
            this.staticLNameLabel.Text = "last Name: ";
            // 
            // lNameInputBox
            // 
            this.lNameInputBox.Location = new System.Drawing.Point(217, 48);
            this.lNameInputBox.Name = "lNameInputBox";
            this.lNameInputBox.Size = new System.Drawing.Size(194, 21);
            this.lNameInputBox.TabIndex = 2;
            // 
            // staticFNameLabel
            // 
            this.staticFNameLabel.AutoSize = true;
            this.staticFNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticFNameLabel.Location = new System.Drawing.Point(41, 28);
            this.staticFNameLabel.Name = "staticFNameLabel";
            this.staticFNameLabel.Size = new System.Drawing.Size(75, 13);
            this.staticFNameLabel.TabIndex = 1;
            this.staticFNameLabel.Text = "First Name: ";
            // 
            // fNameInputBox
            // 
            this.fNameInputBox.Location = new System.Drawing.Point(217, 21);
            this.fNameInputBox.Name = "fNameInputBox";
            this.fNameInputBox.Size = new System.Drawing.Size(194, 21);
            this.fNameInputBox.TabIndex = 0;
            // 
            // UpdateView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 431);
            this.Controls.Add(this.updateGroupBox);
            this.Name = "UpdateView";
            this.Text = "UpdatePersonUI";
            this.updateGroupBox.ResumeLayout(false);
            this.updateGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox updateGroupBox;
        private System.Windows.Forms.TextBox contactCompanyIdInputBox;
        private System.Windows.Forms.Label staticAddContactCompanyLabel;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label staticNotesLabel;
        private System.Windows.Forms.TextBox notesInputBox;
        private System.Windows.Forms.Label staticEmailLabel;
        private System.Windows.Forms.TextBox emailInputBox;
        private System.Windows.Forms.Label staticZipLabel;
        private System.Windows.Forms.TextBox zipInputBox;
        private System.Windows.Forms.Label staticEmployerLabel;
        private System.Windows.Forms.TextBox employerInputBox;
        private System.Windows.Forms.Label staticPhoneLabel;
        private System.Windows.Forms.TextBox phNumInputBox;
        private System.Windows.Forms.Label staticProvStatLabel;
        private System.Windows.Forms.TextBox provStateInputBox;
        private System.Windows.Forms.Label staticCountryLabel;
        private System.Windows.Forms.TextBox countryInputBox;
        private System.Windows.Forms.Label staticLNameLabel;
        private System.Windows.Forms.TextBox lNameInputBox;
        private System.Windows.Forms.Label staticFNameLabel;
        private System.Windows.Forms.TextBox fNameInputBox;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class CatalogView : Form {

        private List<Product> shownProducts = null;
        private List<Product> pickedProds = new List<Product>();
        private List<CatalogPage> pagesFound = null;


        public CatalogView() {
            InitializeComponent();
            this.ShowAllProds();
            this.ShowCurrentPicks();
            this.SweepForSimilars();
        }


        public void ShowCurrentPicks() {
            addedListBox.Items.Clear();
            CatalogPage c = new CatalogPage();
            this.pagesFound = c.GetAllPagesList();

            for(int i=0; i<this.pagesFound.Count; i++) {
                addedListBox.Items.Add(this.pagesFound[i].Prod.Name);
                this.pickedProds.Add(this.pagesFound[i].Prod);
            }
        
        } //end function


        public void ShowAllProds() {
            notAddedListBox.Items.Clear();
            Product p = new Product();
            this.shownProducts = p.GetAllProductsList();

            for(int i=0; i<this.shownProducts.Count; i++) {
                notAddedListBox.Items.Add(this.shownProducts[i].Name);
            }
        
        }


        public void SweepForSimilars() {
            for(int i=0; i<this.pickedProds.Count; i++) {
                for(int x=0; x<this.shownProducts.Count; x++) {
                    if(this.pickedProds[i].Id == this.shownProducts[x].Id) {
                        notAddedListBox.Items.RemoveAt(x);
                        this.shownProducts.RemoveAt(x);
                    }
                }
            }

        } //end function

        
        private void addButton_Click(object sender, EventArgs e) {
            int found = notAddedListBox.SelectedIndex;
            if(found>=0) {
                this.pickedProds.Add(this.shownProducts[found]);
                addedListBox.Items.Add(this.pickedProds[this.pickedProds.Count-1].Name);
                notAddedListBox.Items.RemoveAt(found);
                this.shownProducts.RemoveAt(found);
                
                //Insert            
                CatalogPage cc = new CatalogPage();
                int ok = cc.AddPage(this.pickedProds[this.pickedProds.Count-1].Id, 0, 1);

            } else { MessageBox.Show("Please Choose A Material First"); }

        }

        private void doneButton_Click(object sender, EventArgs e) {
            this.Close();

        }

        private void removeButton_Click(object sender, EventArgs e) {
            int found = addedListBox.SelectedIndex;
            if(found>=0) {
                    //Delete
                    CatalogPage cc = new CatalogPage();
                    int ok = cc.DeletePage(this.pagesFound[addedListBox.SelectedIndex].PageId);

                    this.shownProducts.Add(this.pickedProds[found]);
                    notAddedListBox.Items.Add(this.shownProducts[this.shownProducts.Count-1].Name);
                    addedListBox.Items.RemoveAt(found);
                    this.pickedProds.RemoveAt(found);
                            
            } else { MessageBox.Show("Please Choose A Material First"); }

        } //end function




    } //EOC

} //EON

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class OrdersView : Form {

        
        //Order Objects
        private List<Order> shownOrders = null;
        private List<Contact> contactDropDown = null;
        private List<Supplier> supplierDropDown = null;
        private Order orderViewer = new Order();
        
        //Member Vars
        private int matCost = 0;
        private int preTax = 0;
        private int taxPercent = 0;
        private int shippFees = 0;
        private double totalCost = 0;


        //==============================================
        //Constructs -----------------------------------
        //==============================================

        public OrdersView() {
            InitializeComponent();
            this.ShowAllOrders();
            this.contactDropDown = this.BuildContactDropDown(senderContactComboBox);
            this.supplierDropDown = this.BuildSupplierDropDown(senderSupplierComboBox);
            this.BuildContactDropDown(receiverContactComboBox);
            this.BuildSupplierDropDown(receiverSupplierComboBox);
            this.BuildContactDropDown(ownerContactComboBox);
            this.BuildSupplierDropDown(ownerSupplierComboBox);
        
        } //end constructor

        //==============================================



        //==============================================
        //Functions ------------------------------------
        //==============================================
        
        /// <summary>
        /// Displays All List Items To Orders
        /// </summary>
        public void ShowAllOrders() {
            ordersListBox.Items.Clear();
            Order o = new Order();
            this.shownOrders = o.GetAllOrdersList();

            for(int i=0; i<this.shownOrders.Count; i++) {
                ordersListBox.Items.Add("Order: " + this.shownOrders[i].OrderId 
                    + " To: " + this.shownOrders[i].Sender + " From: " 
                        + this.shownOrders[i].Receiver);
            }

        } //end function


        /// <summary>
        /// Searches for Order By ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void idSearchButton_Click(object sender, EventArgs e) {
            ordersListBox.Items.Clear();
            
            if(idInputBox.Text.CompareTo(String.Empty)!=0) {
                Order o = new Order(Int32.Parse(idInputBox.Text));
                if(o.Worked) {
                    this.shownOrders.Clear();
                    this.shownOrders.Add(o);
                    ordersListBox.Items.Add("Order: " + this.shownOrders[0].OrderId 
                        + " To: " + this.shownOrders[0].Sender + " From: " 
                            + this.shownOrders[0].Receiver);
                
                } 
            } else { MessageBox.Show("Please Enter An ID First!"); }

        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchAllButton_Click(object sender, EventArgs e) {
            this.ShowAllOrders();
        
        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ordersListBox_SelectedIndexChanged(object sender, EventArgs e) {
            if(ordersListBox.SelectedIndex>=0) {
                Order o = new Order(this.shownOrders[ordersListBox.SelectedIndex].OrderId);
                this.orderViewer = o;
                
                orderOutputBox.Text = "Order Id: " + this.orderViewer.OrderId + Environment.NewLine;
                orderOutputBox.Text += "Sender: " + this.orderViewer.Sender + Environment.NewLine;
                orderOutputBox.Text += "Receiver: " + this.orderViewer.Receiver + Environment.NewLine;
                orderOutputBox.Text += "Method of Payment: " + this.orderViewer.Payment + Environment.NewLine;
                orderOutputBox.Text += "Shipping Fees: " + this.orderViewer.ShippingFees + Environment.NewLine;
                orderOutputBox.Text += "Tax Percent: " + this.orderViewer.TaxPercent + Environment.NewLine;
                orderOutputBox.Text += "Before Tax: " + this.orderViewer.PreTax + Environment.NewLine;
                orderOutputBox.Text += "Total Order Price: " + this.orderViewer.Total + Environment.NewLine;
                orderOutputBox.Text += "----------------------------------------" + Environment.NewLine;
                orderOutputBox.Text += "Order Owner: " + this.orderViewer.Person.FName + " " 
                    + this.orderViewer.Person.LName + Environment.NewLine;
                orderOutputBox.Text += "Order Owner ID: " + this.orderViewer.Person.ID + Environment.NewLine;
                orderOutputBox.Text += "Order Owner Email: " + this.orderViewer.Person.Email + Environment.NewLine;
                orderOutputBox.Text += "Order Owner Location: " + this.orderViewer.Person.Country + ", " 
                    + this.orderViewer.Person.ProvState + Environment.NewLine;
            }

        } //end function
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string GetPaymentMethod(int index) {
            string pay = String.Empty;
            switch(index) {
                case 0:
                    pay = "VISA";
                    break;
                case 1:
                    pay = "Mastercard";
                    break;
                case 2:
                    pay = "AMEX";
                    break;
                case 3:
                    pay = "Paypal";
                    break;
               case 4:
                    pay = "Vanilla Mastercard";
                    break;
                case 5:
                    pay = "Company Credit Account";
                    break;
                case 6:
                    pay = "Cash";
                    break;
                case 7:
                    pay = "Cheque";
                    break;
                default:
                    pay = "None";
                    break;
            }
        
            return pay;

        } //end function
        

        /// <summary>
        /// Builds Given ComboBox With All Found
        /// Contacts From Database
        /// </summary>
        /// <param name="c"></param>
        private List<Contact> BuildContactDropDown(ComboBox c) {
            Contact cc = new Contact();
            List<Contact> foundContacts = cc.GetAllContactsList();
            
            for(int i=0; i<foundContacts.Count; i++) {
                c.Items.Add(foundContacts[i].FName + " " 
                    + foundContacts[i].LName);

            }
        
            return foundContacts;

        } //end function


        /// <summary>
        /// Builds Given ComboBox With All Found
        /// Suppliers From Database
        /// </summary>
        /// <param name="c"></param>
        private List<Supplier> BuildSupplierDropDown(ComboBox c) {
            Supplier cc = new Supplier();
            List<Supplier> foundSupplier = cc.GetAllSuppliersList();
            
            for(int i=0; i<foundSupplier.Count; i++) {
                c.Items.Add(foundSupplier[i].FName + " " 
                    + foundSupplier[i].LName);

            }
        
            return foundSupplier;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addOrderButton_Click(object sender, EventArgs e) {
            bool valid = true;
            string msg = "In order to proceed you must enter the ";

            //Validate
            if(senderContactComboBox.SelectedIndex<0 
                && senderSupplierComboBox.SelectedIndex<0) { valid = false; msg+=" | Supplier | "; }
            if(receiverContactComboBox.SelectedIndex<0 
                && receiverSupplierComboBox.SelectedIndex<0) { valid = false; msg+=" | Receiver | "; }
            if(ownerContactComboBox.SelectedIndex<0 
                && ownerSupplierComboBox.SelectedIndex<0) { valid = false; msg+=" | Owner | "; }
            if(paymentComboBox.SelectedIndex<0) { valid=false; msg+=" | Method Of Payment | "; }
            if(shippingFeesInputBox.Text.CompareTo("")==0) { valid=false; msg+=" | Shipping Fee's | "; }
            if(taxPercentInputBox.Text.CompareTo("")==0) { valid=false; msg+=" | Tax Percentage | "; }
            try { DateTime dd = Convert.ToDateTime(dateInputBox); } catch { valid=false; msg += ""; }

            //If Valid...
            if(valid) {
                

            
            } else { MessageBox.Show(msg + " fields. Please Re-enter."); }

        } //end function


        /// <summary>
        /// Returns Total Before Tax
        /// </summary>
        /// <returns></returns>
        public int GetPreTaxTotal() {
            return (this.shippFees + this.matCost);
        
        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetTotalCost() {
            return (this.preTax * (this.taxPercent/100));

        } //end function
       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shippingFeesInputBox_KeyPress(object sender, KeyPressEventArgs e) {
            try {
                if(shippingFeesInputBox.Text.CompareTo("")!=0) {
                    this.shippFees = Int32.Parse(shippingFeesInputBox.Text);
                    this.preTax = this.GetPreTaxTotal();
                    preTaxDispLabel.Text = this.preTax.ToString();
                }
            
            } catch { MessageBox.Show("Not a valid number"); }

        } //end event


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void taxPercentInputBox_KeyPress(object sender, KeyPressEventArgs e) {
            try {
                this.taxPercent = Int32.Parse(taxPercentInputBox.Text);
                this.totalCost = this.GetTotalCost();
                orderTotalDisplayLabel.Text = this.totalCost.ToString();
            
            } catch { MessageBox.Show("Not a valid number"); }

        } //end event
        

        //==============================================



    } //EOC

} //EON

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class ProductView : Form {


        //Product Objects
        private List<Product> shownProducts = null;
        private List<Material> pickedMaterials = null;
        private Product prodViewer = null;
        private List<int> quantities = null;
        

        //==============================================
        //Constructs -----------------------------------
        //==============================================

        public ProductView() {
            InitializeComponent();
            this.ShowAllProducts();
        
        } //end constructor

        //==============================================
        


        //==============================================
        //Functions ------------------------------------
        //==============================================

        private void ShowAllProducts() {
            productSearchListBox.Items.Clear();
            Product p =new Product(); //Make Empty
            this.shownProducts = p.GetAllProductsList();

            for(int i=0; i<this.shownProducts.Count; i++) {
                productSearchListBox.Items.Add(this.shownProducts[i].Name 
                    + "   Model:" + this.shownProducts[i].ModelNum);
            }
        
        } //end function


        /// <summary>
        /// Translates Color Drop Downs
        /// Into String Types
        /// </summary>
        /// <param name="currIndex"></param>
        /// <returns></returns>
        public string GetColor(int currIndex) {
            string color = String.Empty;
            switch(currIndex) {
                case 0:
                    color = "Red";
                    break;
                case 1:
                    color = "Green";
                    break;
                case 2:
                    color = "Blue";
                    break;
                case 3:
                    color = "Yellow";
                    break;
                case 4:
                    color = "Chrome";
                    break;
                case 5:
                    color = "Black";
                    break;
                case 6:
                    color = "White";
                    break;
                default:
                    color = "None";
                    break;
            }
        
            return color;

        } //end function

        //==============================================
        


        //==============================================
        //Events ---------------------------------------
        //==============================================
        
        /// <summary>
        /// Opens Material View UI
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editMaterialsButton_Click(object sender, EventArgs e) {
            MaterialView mV = new MaterialView();
            mV.ShowDialog();

        } //end event


        /// <summary>
        /// Changes The Product Viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productSearchListBox_SelectedIndexChanged(object sender, EventArgs e) {
            if(productSearchListBox.SelectedIndex>=0) {
                this.prodViewer = this.shownProducts[productSearchListBox.SelectedIndex];
            
            } else {this.prodViewer=null;}

            //Show Product
            if(this.prodViewer!=null) {
                productOutputBox.Text = String.Empty;
                productOutputBox.Text += "Name: " + this.prodViewer.Name + Environment.NewLine;
                productOutputBox.Text += "Safety Code: " + this.prodViewer.SafetyCode + Environment.NewLine;
                productOutputBox.Text += "Weight in Kgs: " + this.prodViewer.Weight + Environment.NewLine;
                productOutputBox.Text += "Cost: " + this.prodViewer.Price + Environment.NewLine;
                productOutputBox.Text += "Model Number: " + this.prodViewer.ModelNum + Environment.NewLine;
                productOutputBox.Text += "Color: " + this.prodViewer.Color + Environment.NewLine;
                productOutputBox.Text += "Shipping Zones: " + this.prodViewer.Zones + Environment.NewLine;
                
                //Find Img Path And Enter To Pic Box
                string dir = Path.GetDirectoryName(Application.StartupPath);
                string filename = Path.Combine(dir, @"..\" + this.prodViewer.Img);
                productPictureBox.Image = Image.FromFile(filename);


                productOutputBox.Text += "---------------------" + Environment.NewLine;
                productOutputBox.Text += "Materials Used:" + Environment.NewLine;
                productOutputBox.Text += "---------------------" + Environment.NewLine;
                //Show Materials Used
                if(this.prodViewer.Sheets!=null) {
                    for(int i=0; i<this.prodViewer.Sheets.Count(); i++) {
                        Material m = new Material(this.prodViewer.Sheets[i].MaterialID);
                        productOutputBox.Text += m.Name + Environment.NewLine;
                    }
                }
            
            } else {
                productOutputBox.Text = String.Empty;
                productPictureBox.Image = null;
            }

        } //end event


        /// <summary>
        /// Adds A Product To The Product
        /// Table Then Adds All Materials
        /// To BuildSheets Table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addProductButton_Click(object sender, EventArgs e) {
            bool valid = true;
            string msg = String.Empty;

            if(pNameInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Name | "; }
            if(pSafetyInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Safety Code | "; }
            if(pWeightInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Weight | "; }
            if(pPriceInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Retail Price | "; }
            if(pModelNumInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Model Number | "; }
            if(pShipZonesInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Shipping Zones | "; }
            if(pImgPathInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Image Path | "; }
            if(pColorComboBox.SelectedIndex<0) { valid=false; msg+=" | Choose Color | "; }
            if(this.pickedMaterials==null || this.pickedMaterials.Count<=0) { valid=false; msg+=" | Pick Materials | "; }

            if(valid) {
                
                //Instantiate
                Product p = new Product();
                //--

                //Fill Members
                p.Name = pNameInputBox.Text;
                p.SafetyCode = pSafetyInputBox.Text;
                p.Weight = Int32.Parse(pWeightInputBox.Text);
                p.Price = Int32.Parse(pPriceInputBox.Text);
                p.ModelNum = pModelNumInputBox.Text;
                p.Zones = pShipZonesInputBox.Text;
                p.Img = pImgPathInputBox.Text;
                p.Color = this.GetColor(pColorComboBox.SelectedIndex);
                p.Description = pDescriptionInputBox.Text;
                int ok = p.AddProduct();

                if(ok<0) {
                    bool sheetsOk = true;
                    //Find & Build Sheets
                    for(int i=0; i<this.pickedMaterials.Count; i++) {
                        BuildSheet b = new BuildSheet();
                        b.MaterialID = this.pickedMaterials[i].MaterialId;
                        b.Quantity = this.quantities[i];
                        b.ProductID = p.LastID();
                        int tester = b.AddSheet();
                        if(tester>=0) { sheetsOk = false; }
                    }
                    if(sheetsOk) {
                        //Clear Inputs
                        pNameInputBox.Text = String.Empty;
                        pSafetyInputBox.Text = String.Empty;
                        pWeightInputBox.Text = String.Empty;
                        pPriceInputBox.Text = String.Empty;
                        pModelNumInputBox.Text = String.Empty;
                        pShipZonesInputBox.Text = String.Empty;
                        pImgPathInputBox.Text = String.Empty;
                        pColorComboBox.SelectedIndex = -1;
                        pDescriptionInputBox.Text = String.Empty;
                        MessageBox.Show("Product Added!");
                    
                    } else { MessageBox.Show("Product Could Not Add Materials"); }

                } else { MessageBox.Show("Product Could Not Be Added!"); }
            
            } else { MessageBox.Show("Could Not Add! You Missed The " + msg + " Input Fields"); }

        } //end event


        /// <summary>
        /// Opens & Captures Materials Menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pAddMaterialsButton_Click(object sender, EventArgs e) {
            AddProductChangeMaterialView aa = new AddProductChangeMaterialView();
            aa.ShowDialog();
            this.pickedMaterials = aa.PickedMaterials;
            if(this.pickedMaterials.Count<=0) {
                this.pickedMaterials = null;
                this.quantities = null;
            
            } else { this.quantities = aa.Quant; }
            
            if(this.pickedMaterials!=null && this.pickedMaterials.Count>0) {
                pAddMaterialsButton.BackColor = Color.DarkBlue;

            } else { pAddMaterialsButton.BackColor = Color.DarkRed; }

        } //end event


        /// <summary>
        /// Deletes A Product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void removeProductButton_Click(object sender, EventArgs e) {
            if(productSearchListBox.SelectedIndex>=0) {
                Product p = new Product(); //<--Make Empty
                int ok = p.DeleteProduct(this.shownProducts[productSearchListBox.SelectedIndex].Id); //<--Delete
                if(ok<0) {
                    productSearchListBox.Items.RemoveAt(productSearchListBox.SelectedIndex);
                    MessageBox.Show("Product Deleted!");
                
                } else { MessageBox.Show("Could Not Be Deleted!"); }

            } else { MessageBox.Show("Please Choose A Product To Delete!"); }

        }


        /// <summary>
        /// Opens The Product Update View
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editProductButton_Click(object sender, EventArgs e) {
            if(this.prodViewer!=null) {
                UpdateProductView uP = 
                    new UpdateProductView(this.prodViewer);
                uP.Show();
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchAllButton_Click(object sender, EventArgs e) {
            this.ShowAllProducts();

        }

        private void searchButton_Click(object sender, EventArgs e) {
            productSearchListBox.Items.Clear();
            Product p = new Product(Int32.Parse(searchIdInputBox.Text));
            if(p.Worked) { productSearchListBox.Items.Add(p.Name + "   Model:" + p.ModelNum); }
            
        } //end event
        
        //==============================================




    } //EOC

} //EON

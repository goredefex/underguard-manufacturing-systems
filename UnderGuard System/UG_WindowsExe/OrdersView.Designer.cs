﻿namespace UG_WindowsExe {
    partial class OrdersView {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.searchingSplitContainer = new System.Windows.Forms.SplitContainer();
            this.searchingGroupBox = new System.Windows.Forms.GroupBox();
            this.ordersListBox = new System.Windows.Forms.ListBox();
            this.searchGroupBox = new System.Windows.Forms.GroupBox();
            this.searchAllButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.staticSearchTypeLabel = new System.Windows.Forms.Label();
            this.searchTypeComboBox = new System.Windows.Forms.ComboBox();
            this.staticLastNameInputLabel = new System.Windows.Forms.Label();
            this.staticFirstNameInputLabel = new System.Windows.Forms.Label();
            this.searchLNameInputBox = new System.Windows.Forms.TextBox();
            this.searchFNameInputBox = new System.Windows.Forms.TextBox();
            this.nameSearchButton = new System.Windows.Forms.Button();
            this.staticByNameLabel = new System.Windows.Forms.Label();
            this.idInputBox = new System.Windows.Forms.TextBox();
            this.idSearchButton = new System.Windows.Forms.Button();
            this.staticByIdLabel = new System.Windows.Forms.Label();
            this.resultsEntrySplitContainer = new System.Windows.Forms.SplitContainer();
            this.orderShowGroupBox = new System.Windows.Forms.GroupBox();
            this.orderOutputBox = new System.Windows.Forms.TextBox();
            this.orderEntryGroupBox = new System.Windows.Forms.GroupBox();
            this.addOrderTabControl = new System.Windows.Forms.TabControl();
            this.addBuyOrderTab = new System.Windows.Forms.TabPage();
            this.buyOrderGroupBox = new System.Windows.Forms.GroupBox();
            this.orderTotalDisplayLabel = new System.Windows.Forms.Label();
            this.staticOrderTotalLabel = new System.Windows.Forms.Label();
            this.preTaxDispLabel = new System.Windows.Forms.Label();
            this.addOrderButton = new System.Windows.Forms.Button();
            this.orderedProdsBtn = new System.Windows.Forms.Button();
            this.materialsCostDisplayLabel = new System.Windows.Forms.Label();
            this.staticMaterialsCostLabel = new System.Windows.Forms.Label();
            this.staticTaxPercentLabel = new System.Windows.Forms.Label();
            this.taxPercentInputBox = new System.Windows.Forms.TextBox();
            this.staticShippingFeesLabel = new System.Windows.Forms.Label();
            this.shippingFeesInputBox = new System.Windows.Forms.TextBox();
            this.staticPreTaxLabel = new System.Windows.Forms.Label();
            this.ownerSupplierComboBox = new System.Windows.Forms.ComboBox();
            this.ownerContactComboBox = new System.Windows.Forms.ComboBox();
            this.staticOwnerLabel = new System.Windows.Forms.Label();
            this.paymentComboBox = new System.Windows.Forms.ComboBox();
            this.staticPaymentLabel = new System.Windows.Forms.Label();
            this.receiverSupplierComboBox = new System.Windows.Forms.ComboBox();
            this.receiverContactComboBox = new System.Windows.Forms.ComboBox();
            this.senderSupplierComboBox = new System.Windows.Forms.ComboBox();
            this.senderContactComboBox = new System.Windows.Forms.ComboBox();
            this.staticDateLabel = new System.Windows.Forms.Label();
            this.dateInputBox = new System.Windows.Forms.TextBox();
            this.staticReceiverLabel = new System.Windows.Forms.Label();
            this.staticSenderLabel = new System.Windows.Forms.Label();
            this.addSupplyOrderTab = new System.Windows.Forms.TabPage();
            this.supplyOrderGroupBox = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).BeginInit();
            this.searchingSplitContainer.Panel1.SuspendLayout();
            this.searchingSplitContainer.Panel2.SuspendLayout();
            this.searchingSplitContainer.SuspendLayout();
            this.searchingGroupBox.SuspendLayout();
            this.searchGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultsEntrySplitContainer)).BeginInit();
            this.resultsEntrySplitContainer.Panel1.SuspendLayout();
            this.resultsEntrySplitContainer.Panel2.SuspendLayout();
            this.resultsEntrySplitContainer.SuspendLayout();
            this.orderShowGroupBox.SuspendLayout();
            this.orderEntryGroupBox.SuspendLayout();
            this.addOrderTabControl.SuspendLayout();
            this.addBuyOrderTab.SuspendLayout();
            this.buyOrderGroupBox.SuspendLayout();
            this.addSupplyOrderTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.searchingSplitContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.resultsEntrySplitContainer);
            this.mainSplitContainer.Size = new System.Drawing.Size(1044, 407);
            this.mainSplitContainer.SplitterDistance = 390;
            this.mainSplitContainer.TabIndex = 0;
            // 
            // searchingSplitContainer
            // 
            this.searchingSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchingSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.searchingSplitContainer.Name = "searchingSplitContainer";
            this.searchingSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // searchingSplitContainer.Panel1
            // 
            this.searchingSplitContainer.Panel1.Controls.Add(this.searchingGroupBox);
            // 
            // searchingSplitContainer.Panel2
            // 
            this.searchingSplitContainer.Panel2.Controls.Add(this.searchGroupBox);
            this.searchingSplitContainer.Size = new System.Drawing.Size(390, 407);
            this.searchingSplitContainer.SplitterDistance = 208;
            this.searchingSplitContainer.TabIndex = 0;
            // 
            // searchingGroupBox
            // 
            this.searchingGroupBox.Controls.Add(this.ordersListBox);
            this.searchingGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchingGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchingGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchingGroupBox.Name = "searchingGroupBox";
            this.searchingGroupBox.Size = new System.Drawing.Size(390, 208);
            this.searchingGroupBox.TabIndex = 1;
            this.searchingGroupBox.TabStop = false;
            this.searchingGroupBox.Text = "Orders:";
            // 
            // ordersListBox
            // 
            this.ordersListBox.FormattingEnabled = true;
            this.ordersListBox.HorizontalScrollbar = true;
            this.ordersListBox.ItemHeight = 15;
            this.ordersListBox.Location = new System.Drawing.Point(6, 20);
            this.ordersListBox.Name = "ordersListBox";
            this.ordersListBox.Size = new System.Drawing.Size(369, 184);
            this.ordersListBox.TabIndex = 0;
            this.ordersListBox.SelectedIndexChanged += new System.EventHandler(this.ordersListBox_SelectedIndexChanged);
            // 
            // searchGroupBox
            // 
            this.searchGroupBox.Controls.Add(this.searchAllButton);
            this.searchGroupBox.Controls.Add(this.deleteButton);
            this.searchGroupBox.Controls.Add(this.updateButton);
            this.searchGroupBox.Controls.Add(this.staticSearchTypeLabel);
            this.searchGroupBox.Controls.Add(this.searchTypeComboBox);
            this.searchGroupBox.Controls.Add(this.staticLastNameInputLabel);
            this.searchGroupBox.Controls.Add(this.staticFirstNameInputLabel);
            this.searchGroupBox.Controls.Add(this.searchLNameInputBox);
            this.searchGroupBox.Controls.Add(this.searchFNameInputBox);
            this.searchGroupBox.Controls.Add(this.nameSearchButton);
            this.searchGroupBox.Controls.Add(this.staticByNameLabel);
            this.searchGroupBox.Controls.Add(this.idInputBox);
            this.searchGroupBox.Controls.Add(this.idSearchButton);
            this.searchGroupBox.Controls.Add(this.staticByIdLabel);
            this.searchGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchGroupBox.Location = new System.Drawing.Point(0, 0);
            this.searchGroupBox.Name = "searchGroupBox";
            this.searchGroupBox.Size = new System.Drawing.Size(390, 195);
            this.searchGroupBox.TabIndex = 0;
            this.searchGroupBox.TabStop = false;
            this.searchGroupBox.Text = "Search By:";
            // 
            // searchAllButton
            // 
            this.searchAllButton.BackColor = System.Drawing.Color.Goldenrod;
            this.searchAllButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.searchAllButton.Location = new System.Drawing.Point(253, 147);
            this.searchAllButton.Name = "searchAllButton";
            this.searchAllButton.Size = new System.Drawing.Size(95, 36);
            this.searchAllButton.TabIndex = 31;
            this.searchAllButton.Text = "ALL!";
            this.searchAllButton.UseVisualStyleBackColor = false;
            this.searchAllButton.Click += new System.EventHandler(this.searchAllButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.DarkRed;
            this.deleteButton.ForeColor = System.Drawing.Color.AntiqueWhite;
            this.deleteButton.Location = new System.Drawing.Point(142, 147);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(95, 36);
            this.deleteButton.TabIndex = 30;
            this.deleteButton.Text = "REMOVE!";
            this.deleteButton.UseVisualStyleBackColor = false;
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.ForestGreen;
            this.updateButton.Location = new System.Drawing.Point(30, 147);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(95, 36);
            this.updateButton.TabIndex = 29;
            this.updateButton.Text = "UPDATE!";
            this.updateButton.UseVisualStyleBackColor = false;
            // 
            // staticSearchTypeLabel
            // 
            this.staticSearchTypeLabel.AutoSize = true;
            this.staticSearchTypeLabel.Location = new System.Drawing.Point(280, 24);
            this.staticSearchTypeLabel.Name = "staticSearchTypeLabel";
            this.staticSearchTypeLabel.Size = new System.Drawing.Size(75, 15);
            this.staticSearchTypeLabel.TabIndex = 28;
            this.staticSearchTypeLabel.Text = "Search By:";
            // 
            // searchTypeComboBox
            // 
            this.searchTypeComboBox.FormattingEnabled = true;
            this.searchTypeComboBox.Items.AddRange(new object[] {
            "Contact",
            "Supplier"});
            this.searchTypeComboBox.Location = new System.Drawing.Point(274, 41);
            this.searchTypeComboBox.Name = "searchTypeComboBox";
            this.searchTypeComboBox.Size = new System.Drawing.Size(87, 23);
            this.searchTypeComboBox.TabIndex = 27;
            this.searchTypeComboBox.Text = "Choose:";
            // 
            // staticLastNameInputLabel
            // 
            this.staticLastNameInputLabel.AutoSize = true;
            this.staticLastNameInputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticLastNameInputLabel.Location = new System.Drawing.Point(212, 112);
            this.staticLastNameInputLabel.Name = "staticLastNameInputLabel";
            this.staticLastNameInputLabel.Size = new System.Drawing.Size(67, 15);
            this.staticLastNameInputLabel.TabIndex = 26;
            this.staticLastNameInputLabel.Text = "Last Name";
            // 
            // staticFirstNameInputLabel
            // 
            this.staticFirstNameInputLabel.AutoSize = true;
            this.staticFirstNameInputLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticFirstNameInputLabel.Location = new System.Drawing.Point(101, 112);
            this.staticFirstNameInputLabel.Name = "staticFirstNameInputLabel";
            this.staticFirstNameInputLabel.Size = new System.Drawing.Size(67, 15);
            this.staticFirstNameInputLabel.TabIndex = 25;
            this.staticFirstNameInputLabel.Text = "First Name";
            // 
            // searchLNameInputBox
            // 
            this.searchLNameInputBox.Location = new System.Drawing.Point(193, 89);
            this.searchLNameInputBox.Name = "searchLNameInputBox";
            this.searchLNameInputBox.Size = new System.Drawing.Size(106, 21);
            this.searchLNameInputBox.TabIndex = 24;
            // 
            // searchFNameInputBox
            // 
            this.searchFNameInputBox.Location = new System.Drawing.Point(83, 88);
            this.searchFNameInputBox.Name = "searchFNameInputBox";
            this.searchFNameInputBox.Size = new System.Drawing.Size(106, 21);
            this.searchFNameInputBox.TabIndex = 23;
            // 
            // nameSearchButton
            // 
            this.nameSearchButton.Location = new System.Drawing.Point(305, 88);
            this.nameSearchButton.Name = "nameSearchButton";
            this.nameSearchButton.Size = new System.Drawing.Size(53, 23);
            this.nameSearchButton.TabIndex = 22;
            this.nameSearchButton.Text = "GO!";
            this.nameSearchButton.UseVisualStyleBackColor = true;
            // 
            // staticByNameLabel
            // 
            this.staticByNameLabel.AutoSize = true;
            this.staticByNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByNameLabel.Location = new System.Drawing.Point(19, 91);
            this.staticByNameLabel.Name = "staticByNameLabel";
            this.staticByNameLabel.Size = new System.Drawing.Size(61, 13);
            this.staticByNameLabel.TabIndex = 21;
            this.staticByNameLabel.Text = "By Name:";
            // 
            // idInputBox
            // 
            this.idInputBox.Location = new System.Drawing.Point(86, 45);
            this.idInputBox.Name = "idInputBox";
            this.idInputBox.Size = new System.Drawing.Size(106, 21);
            this.idInputBox.TabIndex = 20;
            // 
            // idSearchButton
            // 
            this.idSearchButton.Location = new System.Drawing.Point(202, 43);
            this.idSearchButton.Name = "idSearchButton";
            this.idSearchButton.Size = new System.Drawing.Size(53, 23);
            this.idSearchButton.TabIndex = 19;
            this.idSearchButton.Text = "GO!";
            this.idSearchButton.UseVisualStyleBackColor = true;
            this.idSearchButton.Click += new System.EventHandler(this.idSearchButton_Click);
            // 
            // staticByIdLabel
            // 
            this.staticByIdLabel.AutoSize = true;
            this.staticByIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticByIdLabel.Location = new System.Drawing.Point(41, 47);
            this.staticByIdLabel.Name = "staticByIdLabel";
            this.staticByIdLabel.Size = new System.Drawing.Size(42, 13);
            this.staticByIdLabel.TabIndex = 18;
            this.staticByIdLabel.Text = "By ID:";
            // 
            // resultsEntrySplitContainer
            // 
            this.resultsEntrySplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.resultsEntrySplitContainer.Location = new System.Drawing.Point(0, 0);
            this.resultsEntrySplitContainer.Name = "resultsEntrySplitContainer";
            // 
            // resultsEntrySplitContainer.Panel1
            // 
            this.resultsEntrySplitContainer.Panel1.Controls.Add(this.orderShowGroupBox);
            // 
            // resultsEntrySplitContainer.Panel2
            // 
            this.resultsEntrySplitContainer.Panel2.Controls.Add(this.orderEntryGroupBox);
            this.resultsEntrySplitContainer.Size = new System.Drawing.Size(650, 407);
            this.resultsEntrySplitContainer.SplitterDistance = 250;
            this.resultsEntrySplitContainer.TabIndex = 0;
            // 
            // orderShowGroupBox
            // 
            this.orderShowGroupBox.Controls.Add(this.orderOutputBox);
            this.orderShowGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderShowGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderShowGroupBox.Location = new System.Drawing.Point(0, 0);
            this.orderShowGroupBox.Name = "orderShowGroupBox";
            this.orderShowGroupBox.Size = new System.Drawing.Size(250, 407);
            this.orderShowGroupBox.TabIndex = 0;
            this.orderShowGroupBox.TabStop = false;
            this.orderShowGroupBox.Text = "Selected Order:";
            // 
            // orderOutputBox
            // 
            this.orderOutputBox.Location = new System.Drawing.Point(6, 20);
            this.orderOutputBox.Multiline = true;
            this.orderOutputBox.Name = "orderOutputBox";
            this.orderOutputBox.Size = new System.Drawing.Size(238, 375);
            this.orderOutputBox.TabIndex = 0;
            // 
            // orderEntryGroupBox
            // 
            this.orderEntryGroupBox.Controls.Add(this.addOrderTabControl);
            this.orderEntryGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.orderEntryGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderEntryGroupBox.Location = new System.Drawing.Point(0, 0);
            this.orderEntryGroupBox.Name = "orderEntryGroupBox";
            this.orderEntryGroupBox.Size = new System.Drawing.Size(396, 407);
            this.orderEntryGroupBox.TabIndex = 0;
            this.orderEntryGroupBox.TabStop = false;
            this.orderEntryGroupBox.Text = "Enter An Order:";
            // 
            // addOrderTabControl
            // 
            this.addOrderTabControl.Controls.Add(this.addBuyOrderTab);
            this.addOrderTabControl.Controls.Add(this.addSupplyOrderTab);
            this.addOrderTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addOrderTabControl.Location = new System.Drawing.Point(3, 17);
            this.addOrderTabControl.Name = "addOrderTabControl";
            this.addOrderTabControl.SelectedIndex = 0;
            this.addOrderTabControl.Size = new System.Drawing.Size(390, 387);
            this.addOrderTabControl.TabIndex = 20;
            // 
            // addBuyOrderTab
            // 
            this.addBuyOrderTab.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.addBuyOrderTab.Controls.Add(this.buyOrderGroupBox);
            this.addBuyOrderTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addBuyOrderTab.Location = new System.Drawing.Point(4, 24);
            this.addBuyOrderTab.Name = "addBuyOrderTab";
            this.addBuyOrderTab.Padding = new System.Windows.Forms.Padding(3);
            this.addBuyOrderTab.Size = new System.Drawing.Size(382, 359);
            this.addBuyOrderTab.TabIndex = 0;
            this.addBuyOrderTab.Text = "Buy Order";
            // 
            // buyOrderGroupBox
            // 
            this.buyOrderGroupBox.Controls.Add(this.orderTotalDisplayLabel);
            this.buyOrderGroupBox.Controls.Add(this.staticOrderTotalLabel);
            this.buyOrderGroupBox.Controls.Add(this.preTaxDispLabel);
            this.buyOrderGroupBox.Controls.Add(this.addOrderButton);
            this.buyOrderGroupBox.Controls.Add(this.orderedProdsBtn);
            this.buyOrderGroupBox.Controls.Add(this.materialsCostDisplayLabel);
            this.buyOrderGroupBox.Controls.Add(this.staticMaterialsCostLabel);
            this.buyOrderGroupBox.Controls.Add(this.staticTaxPercentLabel);
            this.buyOrderGroupBox.Controls.Add(this.taxPercentInputBox);
            this.buyOrderGroupBox.Controls.Add(this.staticShippingFeesLabel);
            this.buyOrderGroupBox.Controls.Add(this.shippingFeesInputBox);
            this.buyOrderGroupBox.Controls.Add(this.staticPreTaxLabel);
            this.buyOrderGroupBox.Controls.Add(this.ownerSupplierComboBox);
            this.buyOrderGroupBox.Controls.Add(this.ownerContactComboBox);
            this.buyOrderGroupBox.Controls.Add(this.staticOwnerLabel);
            this.buyOrderGroupBox.Controls.Add(this.paymentComboBox);
            this.buyOrderGroupBox.Controls.Add(this.staticPaymentLabel);
            this.buyOrderGroupBox.Controls.Add(this.receiverSupplierComboBox);
            this.buyOrderGroupBox.Controls.Add(this.receiverContactComboBox);
            this.buyOrderGroupBox.Controls.Add(this.senderSupplierComboBox);
            this.buyOrderGroupBox.Controls.Add(this.senderContactComboBox);
            this.buyOrderGroupBox.Controls.Add(this.staticDateLabel);
            this.buyOrderGroupBox.Controls.Add(this.dateInputBox);
            this.buyOrderGroupBox.Controls.Add(this.staticReceiverLabel);
            this.buyOrderGroupBox.Controls.Add(this.staticSenderLabel);
            this.buyOrderGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buyOrderGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buyOrderGroupBox.Location = new System.Drawing.Point(3, 3);
            this.buyOrderGroupBox.Name = "buyOrderGroupBox";
            this.buyOrderGroupBox.Size = new System.Drawing.Size(376, 353);
            this.buyOrderGroupBox.TabIndex = 6;
            this.buyOrderGroupBox.TabStop = false;
            this.buyOrderGroupBox.Text = "Enter A Buy Order";
            // 
            // orderTotalDisplayLabel
            // 
            this.orderTotalDisplayLabel.AutoSize = true;
            this.orderTotalDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderTotalDisplayLabel.Location = new System.Drawing.Point(254, 278);
            this.orderTotalDisplayLabel.Name = "orderTotalDisplayLabel";
            this.orderTotalDisplayLabel.Size = new System.Drawing.Size(14, 13);
            this.orderTotalDisplayLabel.TabIndex = 36;
            this.orderTotalDisplayLabel.Text = "0";
            // 
            // staticOrderTotalLabel
            // 
            this.staticOrderTotalLabel.AutoSize = true;
            this.staticOrderTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticOrderTotalLabel.Location = new System.Drawing.Point(15, 280);
            this.staticOrderTotalLabel.Name = "staticOrderTotalLabel";
            this.staticOrderTotalLabel.Size = new System.Drawing.Size(75, 13);
            this.staticOrderTotalLabel.TabIndex = 35;
            this.staticOrderTotalLabel.Text = "Order Total:";
            // 
            // preTaxDispLabel
            // 
            this.preTaxDispLabel.AutoSize = true;
            this.preTaxDispLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preTaxDispLabel.Location = new System.Drawing.Point(254, 256);
            this.preTaxDispLabel.Name = "preTaxDispLabel";
            this.preTaxDispLabel.Size = new System.Drawing.Size(14, 13);
            this.preTaxDispLabel.TabIndex = 34;
            this.preTaxDispLabel.Text = "0";
            // 
            // addOrderButton
            // 
            this.addOrderButton.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.addOrderButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addOrderButton.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.addOrderButton.Location = new System.Drawing.Point(232, 308);
            this.addOrderButton.Name = "addOrderButton";
            this.addOrderButton.Size = new System.Drawing.Size(96, 39);
            this.addOrderButton.TabIndex = 33;
            this.addOrderButton.Text = "Order!";
            this.addOrderButton.UseVisualStyleBackColor = false;
            this.addOrderButton.Click += new System.EventHandler(this.addOrderButton_Click);
            // 
            // orderedProdsBtn
            // 
            this.orderedProdsBtn.BackColor = System.Drawing.Color.DarkRed;
            this.orderedProdsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.orderedProdsBtn.ForeColor = System.Drawing.Color.AntiqueWhite;
            this.orderedProdsBtn.Location = new System.Drawing.Point(62, 308);
            this.orderedProdsBtn.Name = "orderedProdsBtn";
            this.orderedProdsBtn.Size = new System.Drawing.Size(96, 39);
            this.orderedProdsBtn.TabIndex = 32;
            this.orderedProdsBtn.Text = "Products";
            this.orderedProdsBtn.UseVisualStyleBackColor = false;
            // 
            // materialsCostDisplayLabel
            // 
            this.materialsCostDisplayLabel.AutoSize = true;
            this.materialsCostDisplayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.materialsCostDisplayLabel.Location = new System.Drawing.Point(254, 175);
            this.materialsCostDisplayLabel.Name = "materialsCostDisplayLabel";
            this.materialsCostDisplayLabel.Size = new System.Drawing.Size(14, 13);
            this.materialsCostDisplayLabel.TabIndex = 23;
            this.materialsCostDisplayLabel.Text = "0";
            // 
            // staticMaterialsCostLabel
            // 
            this.staticMaterialsCostLabel.AutoSize = true;
            this.staticMaterialsCostLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticMaterialsCostLabel.Location = new System.Drawing.Point(15, 175);
            this.staticMaterialsCostLabel.Name = "staticMaterialsCostLabel";
            this.staticMaterialsCostLabel.Size = new System.Drawing.Size(91, 13);
            this.staticMaterialsCostLabel.TabIndex = 22;
            this.staticMaterialsCostLabel.Text = "Materials Cost:";
            // 
            // staticTaxPercentLabel
            // 
            this.staticTaxPercentLabel.AutoSize = true;
            this.staticTaxPercentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticTaxPercentLabel.Location = new System.Drawing.Point(15, 229);
            this.staticTaxPercentLabel.Name = "staticTaxPercentLabel";
            this.staticTaxPercentLabel.Size = new System.Drawing.Size(101, 13);
            this.staticTaxPercentLabel.TabIndex = 21;
            this.staticTaxPercentLabel.Text = "Tax Percentage:";
            // 
            // taxPercentInputBox
            // 
            this.taxPercentInputBox.Location = new System.Drawing.Point(179, 224);
            this.taxPercentInputBox.Name = "taxPercentInputBox";
            this.taxPercentInputBox.Size = new System.Drawing.Size(177, 21);
            this.taxPercentInputBox.TabIndex = 20;
            this.taxPercentInputBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.taxPercentInputBox_KeyPress);
            // 
            // staticShippingFeesLabel
            // 
            this.staticShippingFeesLabel.AutoSize = true;
            this.staticShippingFeesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticShippingFeesLabel.Location = new System.Drawing.Point(15, 202);
            this.staticShippingFeesLabel.Name = "staticShippingFeesLabel";
            this.staticShippingFeesLabel.Size = new System.Drawing.Size(94, 13);
            this.staticShippingFeesLabel.TabIndex = 19;
            this.staticShippingFeesLabel.Text = "Shipping Fee\'s:";
            // 
            // shippingFeesInputBox
            // 
            this.shippingFeesInputBox.ImeMode = System.Windows.Forms.ImeMode.On;
            this.shippingFeesInputBox.Location = new System.Drawing.Point(179, 197);
            this.shippingFeesInputBox.Name = "shippingFeesInputBox";
            this.shippingFeesInputBox.Size = new System.Drawing.Size(177, 21);
            this.shippingFeesInputBox.TabIndex = 18;
            this.shippingFeesInputBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.shippingFeesInputBox_KeyPress);
            // 
            // staticPreTaxLabel
            // 
            this.staticPreTaxLabel.AutoSize = true;
            this.staticPreTaxLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPreTaxLabel.Location = new System.Drawing.Point(15, 256);
            this.staticPreTaxLabel.Name = "staticPreTaxLabel";
            this.staticPreTaxLabel.Size = new System.Drawing.Size(123, 13);
            this.staticPreTaxLabel.TabIndex = 17;
            this.staticPreTaxLabel.Text = "Pre-Tax Order Total:";
            // 
            // ownerSupplierComboBox
            // 
            this.ownerSupplierComboBox.FormattingEnabled = true;
            this.ownerSupplierComboBox.Location = new System.Drawing.Point(232, 86);
            this.ownerSupplierComboBox.Name = "ownerSupplierComboBox";
            this.ownerSupplierComboBox.Size = new System.Drawing.Size(124, 23);
            this.ownerSupplierComboBox.TabIndex = 15;
            this.ownerSupplierComboBox.Text = "Suppliers:";
            // 
            // ownerContactComboBox
            // 
            this.ownerContactComboBox.FormattingEnabled = true;
            this.ownerContactComboBox.Location = new System.Drawing.Point(102, 86);
            this.ownerContactComboBox.Name = "ownerContactComboBox";
            this.ownerContactComboBox.Size = new System.Drawing.Size(124, 23);
            this.ownerContactComboBox.TabIndex = 14;
            this.ownerContactComboBox.Text = "Contacts:";
            // 
            // staticOwnerLabel
            // 
            this.staticOwnerLabel.AutoSize = true;
            this.staticOwnerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticOwnerLabel.Location = new System.Drawing.Point(15, 91);
            this.staticOwnerLabel.Name = "staticOwnerLabel";
            this.staticOwnerLabel.Size = new System.Drawing.Size(47, 13);
            this.staticOwnerLabel.TabIndex = 13;
            this.staticOwnerLabel.Text = "Owner:";
            // 
            // paymentComboBox
            // 
            this.paymentComboBox.FormattingEnabled = true;
            this.paymentComboBox.Items.AddRange(new object[] {
            "VISA",
            "Mastercard",
            "AMEX",
            "Paypal",
            "Vanilla Mastercard",
            "Company Credit Account",
            "Cash",
            "Cheque"});
            this.paymentComboBox.Location = new System.Drawing.Point(179, 142);
            this.paymentComboBox.Name = "paymentComboBox";
            this.paymentComboBox.Size = new System.Drawing.Size(177, 23);
            this.paymentComboBox.TabIndex = 12;
            this.paymentComboBox.Text = "Choose Payment:";
            // 
            // staticPaymentLabel
            // 
            this.staticPaymentLabel.AutoSize = true;
            this.staticPaymentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticPaymentLabel.Location = new System.Drawing.Point(15, 147);
            this.staticPaymentLabel.Name = "staticPaymentLabel";
            this.staticPaymentLabel.Size = new System.Drawing.Size(122, 13);
            this.staticPaymentLabel.TabIndex = 11;
            this.staticPaymentLabel.Text = "Method Of Payment:";
            // 
            // receiverSupplierComboBox
            // 
            this.receiverSupplierComboBox.FormattingEnabled = true;
            this.receiverSupplierComboBox.Location = new System.Drawing.Point(232, 57);
            this.receiverSupplierComboBox.Name = "receiverSupplierComboBox";
            this.receiverSupplierComboBox.Size = new System.Drawing.Size(124, 23);
            this.receiverSupplierComboBox.TabIndex = 9;
            this.receiverSupplierComboBox.Text = "Suppliers:";
            // 
            // receiverContactComboBox
            // 
            this.receiverContactComboBox.FormattingEnabled = true;
            this.receiverContactComboBox.Location = new System.Drawing.Point(102, 57);
            this.receiverContactComboBox.Name = "receiverContactComboBox";
            this.receiverContactComboBox.Size = new System.Drawing.Size(124, 23);
            this.receiverContactComboBox.TabIndex = 8;
            this.receiverContactComboBox.Text = "Contacts:";
            // 
            // senderSupplierComboBox
            // 
            this.senderSupplierComboBox.FormattingEnabled = true;
            this.senderSupplierComboBox.Location = new System.Drawing.Point(232, 30);
            this.senderSupplierComboBox.Name = "senderSupplierComboBox";
            this.senderSupplierComboBox.Size = new System.Drawing.Size(124, 23);
            this.senderSupplierComboBox.TabIndex = 7;
            this.senderSupplierComboBox.Text = "Suppliers:";
            // 
            // senderContactComboBox
            // 
            this.senderContactComboBox.FormattingEnabled = true;
            this.senderContactComboBox.Location = new System.Drawing.Point(102, 30);
            this.senderContactComboBox.Name = "senderContactComboBox";
            this.senderContactComboBox.Size = new System.Drawing.Size(124, 23);
            this.senderContactComboBox.TabIndex = 6;
            this.senderContactComboBox.Text = "Contacts:";
            // 
            // staticDateLabel
            // 
            this.staticDateLabel.AutoSize = true;
            this.staticDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticDateLabel.Location = new System.Drawing.Point(15, 120);
            this.staticDateLabel.Name = "staticDateLabel";
            this.staticDateLabel.Size = new System.Drawing.Size(88, 13);
            this.staticDateLabel.TabIndex = 5;
            this.staticDateLabel.Text = "Date of Order:";
            // 
            // dateInputBox
            // 
            this.dateInputBox.Location = new System.Drawing.Point(179, 115);
            this.dateInputBox.Name = "dateInputBox";
            this.dateInputBox.Size = new System.Drawing.Size(177, 21);
            this.dateInputBox.TabIndex = 4;
            this.dateInputBox.Text = "(YYYY-MM-DD HH:MM:SS)";
            // 
            // staticReceiverLabel
            // 
            this.staticReceiverLabel.AutoSize = true;
            this.staticReceiverLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticReceiverLabel.Location = new System.Drawing.Point(15, 62);
            this.staticReceiverLabel.Name = "staticReceiverLabel";
            this.staticReceiverLabel.Size = new System.Drawing.Size(62, 13);
            this.staticReceiverLabel.TabIndex = 3;
            this.staticReceiverLabel.Text = "Receiver:";
            // 
            // staticSenderLabel
            // 
            this.staticSenderLabel.AutoSize = true;
            this.staticSenderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticSenderLabel.Location = new System.Drawing.Point(15, 35);
            this.staticSenderLabel.Name = "staticSenderLabel";
            this.staticSenderLabel.Size = new System.Drawing.Size(51, 13);
            this.staticSenderLabel.TabIndex = 1;
            this.staticSenderLabel.Text = "Sender:";
            // 
            // addSupplyOrderTab
            // 
            this.addSupplyOrderTab.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.addSupplyOrderTab.Controls.Add(this.supplyOrderGroupBox);
            this.addSupplyOrderTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addSupplyOrderTab.Location = new System.Drawing.Point(4, 24);
            this.addSupplyOrderTab.Name = "addSupplyOrderTab";
            this.addSupplyOrderTab.Padding = new System.Windows.Forms.Padding(3);
            this.addSupplyOrderTab.Size = new System.Drawing.Size(382, 359);
            this.addSupplyOrderTab.TabIndex = 1;
            this.addSupplyOrderTab.Text = "Supply Order";
            // 
            // supplyOrderGroupBox
            // 
            this.supplyOrderGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.supplyOrderGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supplyOrderGroupBox.Location = new System.Drawing.Point(3, 3);
            this.supplyOrderGroupBox.Name = "supplyOrderGroupBox";
            this.supplyOrderGroupBox.Size = new System.Drawing.Size(376, 353);
            this.supplyOrderGroupBox.TabIndex = 7;
            this.supplyOrderGroupBox.TabStop = false;
            this.supplyOrderGroupBox.Text = "Enter A Supply Order";
            // 
            // OrdersView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 407);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "OrdersView";
            this.Text = "Orders";
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.searchingSplitContainer.Panel1.ResumeLayout(false);
            this.searchingSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchingSplitContainer)).EndInit();
            this.searchingSplitContainer.ResumeLayout(false);
            this.searchingGroupBox.ResumeLayout(false);
            this.searchGroupBox.ResumeLayout(false);
            this.searchGroupBox.PerformLayout();
            this.resultsEntrySplitContainer.Panel1.ResumeLayout(false);
            this.resultsEntrySplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.resultsEntrySplitContainer)).EndInit();
            this.resultsEntrySplitContainer.ResumeLayout(false);
            this.orderShowGroupBox.ResumeLayout(false);
            this.orderShowGroupBox.PerformLayout();
            this.orderEntryGroupBox.ResumeLayout(false);
            this.addOrderTabControl.ResumeLayout(false);
            this.addBuyOrderTab.ResumeLayout(false);
            this.buyOrderGroupBox.ResumeLayout(false);
            this.buyOrderGroupBox.PerformLayout();
            this.addSupplyOrderTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer searchingSplitContainer;
        private System.Windows.Forms.GroupBox searchingGroupBox;
        private System.Windows.Forms.ListBox ordersListBox;
        private System.Windows.Forms.GroupBox searchGroupBox;
        private System.Windows.Forms.Button searchAllButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Label staticSearchTypeLabel;
        private System.Windows.Forms.ComboBox searchTypeComboBox;
        private System.Windows.Forms.Label staticLastNameInputLabel;
        private System.Windows.Forms.Label staticFirstNameInputLabel;
        private System.Windows.Forms.TextBox searchLNameInputBox;
        private System.Windows.Forms.TextBox searchFNameInputBox;
        private System.Windows.Forms.Button nameSearchButton;
        private System.Windows.Forms.Label staticByNameLabel;
        private System.Windows.Forms.TextBox idInputBox;
        private System.Windows.Forms.Button idSearchButton;
        private System.Windows.Forms.Label staticByIdLabel;
        private System.Windows.Forms.SplitContainer resultsEntrySplitContainer;
        private System.Windows.Forms.GroupBox orderShowGroupBox;
        private System.Windows.Forms.GroupBox orderEntryGroupBox;
        private System.Windows.Forms.TabControl addOrderTabControl;
        private System.Windows.Forms.TabPage addBuyOrderTab;
        private System.Windows.Forms.GroupBox buyOrderGroupBox;
        private System.Windows.Forms.Label staticDateLabel;
        private System.Windows.Forms.TextBox dateInputBox;
        private System.Windows.Forms.Label staticReceiverLabel;
        private System.Windows.Forms.Label staticSenderLabel;
        private System.Windows.Forms.TabPage addSupplyOrderTab;
        private System.Windows.Forms.GroupBox supplyOrderGroupBox;
        private System.Windows.Forms.TextBox orderOutputBox;
        private System.Windows.Forms.ComboBox senderContactComboBox;
        private System.Windows.Forms.ComboBox senderSupplierComboBox;
        private System.Windows.Forms.ComboBox receiverSupplierComboBox;
        private System.Windows.Forms.ComboBox receiverContactComboBox;
        private System.Windows.Forms.ComboBox paymentComboBox;
        private System.Windows.Forms.Label staticPaymentLabel;
        private System.Windows.Forms.ComboBox ownerSupplierComboBox;
        private System.Windows.Forms.ComboBox ownerContactComboBox;
        private System.Windows.Forms.Label staticOwnerLabel;
        private System.Windows.Forms.Label staticTaxPercentLabel;
        private System.Windows.Forms.TextBox taxPercentInputBox;
        private System.Windows.Forms.Label staticShippingFeesLabel;
        private System.Windows.Forms.TextBox shippingFeesInputBox;
        private System.Windows.Forms.Label staticPreTaxLabel;
        private System.Windows.Forms.Label materialsCostDisplayLabel;
        private System.Windows.Forms.Label staticMaterialsCostLabel;
        private System.Windows.Forms.Button addOrderButton;
        private System.Windows.Forms.Button orderedProdsBtn;
        private System.Windows.Forms.Label preTaxDispLabel;
        private System.Windows.Forms.Label orderTotalDisplayLabel;
        private System.Windows.Forms.Label staticOrderTotalLabel;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UG_Business;

namespace UG_WindowsExe {
    public partial class UpdateProductView : Form {

        //Product Objects
        private List<Material> pickedMaterials = null;
        private List<int> quantities = null;
        private Product currProd = null;


        public UpdateProductView(Product newP) {
            InitializeComponent();
            this.currProd = newP;
            this.PopulateControls();

        }


        /// <summary>
        /// Updates All Controls To 
        /// Newly Given Member Product
        /// </summary>
        public void PopulateControls() {
            if(this.currProd!=null) {
                pNameInputBox.Text = this.currProd.Name;
                pSafetyInputBox.Text = this.currProd.SafetyCode;
                pWeightInputBox.Text = this.currProd.Weight.ToString();
                pPriceInputBox.Text = this.currProd.Price.ToString();
                pModelNumInputBox.Text = this.currProd.ModelNum;
                pShipZonesInputBox.Text = this.currProd.Zones;
                pImgPathInputBox.Text = this.currProd.Img;
                pDescriptionInputBox.Text = this.currProd.Description;
                pColorComboBox.SelectedIndex = this.ColorIndex(this.currProd.Color);
            }
        
        } //end function



        /// <summary>
        /// Searches For Given Color Index
        /// </summary>
        /// <returns></returns>
        public int ColorIndex(string color) {
            int index = -1;
            switch(color.ToLower()) {
                case "red":
                    index = 0;
                    break;
                case "green":
                    index = 1;
                    break;
                case "blue":
                    index = 2;
                    break;
                case "yellow":
                    index = 3;
                    break;
                case "Chrome":
                    index = 4;
                    break;
                case "black":
                    index = 5;
                    break;
                case "white":
                    index = 6;
                    break;
            }
        
            return index;

        } //end function



        /// <summary>
        /// Translates Color Drop Downs
        /// Into String Types
        /// </summary>
        /// <param name="currIndex"></param>
        /// <returns></returns>
        public string GetColor(int currIndex) {
            string color = String.Empty;
            switch(currIndex) {
                case 0:
                    color = "Red";
                    break;
                case 1:
                    color = "Green";
                    break;
                case 2:
                    color = "Blue";
                    break;
                case 3:
                    color = "Yellow";
                    break;
                case 4:
                    color = "Chrome";
                    break;
                case 5:
                    color = "Black";
                    break;
                case 6:
                    color = "White";
                    break;
                default:
                    color = "None";
                    break;
            }
        
            return color;

        } //end function


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pAddMaterialsButton_Click(object sender, EventArgs e) {
            AddProductChangeMaterialView aa = new AddProductChangeMaterialView();
            aa.ShowDialog();
            this.pickedMaterials = aa.PickedMaterials;
            if(this.pickedMaterials.Count<=0) {
                this.pickedMaterials = null;
                this.quantities = null;
            
            } else { this.quantities = aa.Quant; }
            
            if(this.pickedMaterials!=null && this.pickedMaterials.Count>0) {
                pAddMaterialsButton.BackColor = Color.DarkBlue;

            } else { pAddMaterialsButton.BackColor = Color.DarkRed; }
        
        }

        private void updateProductButton_Click(object sender, EventArgs e) {
            bool valid = true;
            string msg = String.Empty;

            if(pNameInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Name | "; }
            if(pSafetyInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Safety Code | "; }
            if(pWeightInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Weight | "; }
            if(pPriceInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Retail Price | "; }
            if(pModelNumInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Model Number | "; }
            if(pShipZonesInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Shipping Zones | "; }
            if(pImgPathInputBox.Text.CompareTo(String.Empty)==0) { valid=false; msg+=" | Image Path | "; }
            if(pColorComboBox.SelectedIndex<0) { valid=false; msg+=" | Choose Color | "; }
            if(this.pickedMaterials==null || this.pickedMaterials.Count<=0) { valid=false; msg+=" | Pick Materials | "; }

            if(valid) {
                
                //Instantiate
                Product p = new Product();
                //--

                //Update By Inputs
                int ok = p.UpdateProduct(this.currProd.Id, pDescriptionInputBox.Text, pSafetyInputBox.Text, 
                                         pNameInputBox.Text, Int32.Parse(pWeightInputBox.Text), Int32.Parse(pPriceInputBox.Text),
                                         pModelNumInputBox.Text, this.GetColor(pColorComboBox.SelectedIndex), pShipZonesInputBox.Text,
                                         pImgPathInputBox.Text);

                if(ok<0) {
                    //bool sheetsOk = true;
                    ////Find & Build Sheets
                    //for(int i=0; i<this.pickedMaterials.Count; i++) {
                    //    BuildSheet b = new BuildSheet();
                    //    b.MaterialID = this.pickedMaterials[i].MaterialId;
                    //    b.Quantity = this.quantities[i];
                    //    b.ProductID = p.LastID();
                    //    int tester = b.AddSheet();
                    //    if(tester<0) { sheetsOk = false; }
                    //}
                    //if(sheetsOk) {
                    //    //Clear Inputs
                    //    pNameInputBox.Text = String.Empty;
                    //    pSafetyInputBox.Text = String.Empty;
                    //    pWeightInputBox.Text = String.Empty;
                    //    pPriceInputBox.Text = String.Empty;
                    //    pModelNumInputBox.Text = String.Empty;
                    //    pShipZonesInputBox.Text = String.Empty;
                    //    pImgPathInputBox.Text = String.Empty;
                    //    pColorComboBox.SelectedIndex = -1;
                    //    pDescriptionInputBox.Text = String.Empty;
                        MessageBox.Show("Product Updated!");
                        this.Close();
                    //} else { MessageBox.Show("Product Could Not Add Materials"); }

                } else { MessageBox.Show("Product Could Not Be Updated!"); }
            
            } else { MessageBox.Show("Could Not Add! You Missed The " + msg + " Input Fields"); }

        } //end event


    }
}

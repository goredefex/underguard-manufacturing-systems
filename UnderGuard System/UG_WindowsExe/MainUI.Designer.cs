﻿namespace UG_WindowsExe {
    partial class MainUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.controlBtnGroupBox = new System.Windows.Forms.GroupBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.productManageButton = new System.Windows.Forms.Button();
            this.manageContactBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.staticTitleLabel = new System.Windows.Forms.Label();
            this.controlBtnGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // controlBtnGroupBox
            // 
            this.controlBtnGroupBox.Controls.Add(this.exitBtn);
            this.controlBtnGroupBox.Controls.Add(this.button2);
            this.controlBtnGroupBox.Controls.Add(this.button1);
            this.controlBtnGroupBox.Controls.Add(this.productManageButton);
            this.controlBtnGroupBox.Controls.Add(this.manageContactBtn);
            this.controlBtnGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.controlBtnGroupBox.ForeColor = System.Drawing.Color.OrangeRed;
            this.controlBtnGroupBox.Location = new System.Drawing.Point(12, 12);
            this.controlBtnGroupBox.Name = "controlBtnGroupBox";
            this.controlBtnGroupBox.Size = new System.Drawing.Size(202, 339);
            this.controlBtnGroupBox.TabIndex = 0;
            this.controlBtnGroupBox.TabStop = false;
            this.controlBtnGroupBox.Text = "Controls:";
            // 
            // exitBtn
            // 
            this.exitBtn.BackColor = System.Drawing.Color.SandyBrown;
            this.exitBtn.FlatAppearance.BorderSize = 0;
            this.exitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.exitBtn.Location = new System.Drawing.Point(30, 264);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(137, 31);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "EXIT";
            this.exitBtn.UseVisualStyleBackColor = false;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button2.Location = new System.Drawing.Point(30, 213);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 31);
            this.button2.TabIndex = 7;
            this.button2.Text = "Manage Catalog";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(30, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 31);
            this.button1.TabIndex = 6;
            this.button1.Text = "Manage Orders";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // productManageButton
            // 
            this.productManageButton.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.productManageButton.FlatAppearance.BorderSize = 0;
            this.productManageButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.productManageButton.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.productManageButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.productManageButton.Location = new System.Drawing.Point(30, 106);
            this.productManageButton.Name = "productManageButton";
            this.productManageButton.Size = new System.Drawing.Size(137, 31);
            this.productManageButton.TabIndex = 5;
            this.productManageButton.Text = "Manage Products";
            this.productManageButton.UseVisualStyleBackColor = false;
            this.productManageButton.Click += new System.EventHandler(this.productManageButton_Click);
            // 
            // manageContactBtn
            // 
            this.manageContactBtn.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.manageContactBtn.FlatAppearance.BorderSize = 0;
            this.manageContactBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.manageContactBtn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.manageContactBtn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.manageContactBtn.Location = new System.Drawing.Point(30, 57);
            this.manageContactBtn.Name = "manageContactBtn";
            this.manageContactBtn.Size = new System.Drawing.Size(137, 31);
            this.manageContactBtn.TabIndex = 4;
            this.manageContactBtn.Text = "Manage Contacts";
            this.manageContactBtn.UseVisualStyleBackColor = false;
            this.manageContactBtn.Click += new System.EventHandler(this.manageContactBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::UG_WindowsExe.Properties.Resources.fireLogo;
            this.pictureBox1.Location = new System.Drawing.Point(220, 85);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(307, 213);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // staticTitleLabel
            // 
            this.staticTitleLabel.AutoSize = true;
            this.staticTitleLabel.Font = new System.Drawing.Font("Stencil", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.staticTitleLabel.ForeColor = System.Drawing.Color.OrangeRed;
            this.staticTitleLabel.Location = new System.Drawing.Point(218, 30);
            this.staticTitleLabel.Name = "staticTitleLabel";
            this.staticTitleLabel.Size = new System.Drawing.Size(314, 48);
            this.staticTitleLabel.TabIndex = 2;
            this.staticTitleLabel.Text = "UnderGuard Manufacturing \r\n                       System";
            // 
            // MainUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(530, 363);
            this.Controls.Add(this.staticTitleLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.controlBtnGroupBox);
            this.Name = "MainUI";
            this.Text = "MainUI";
            this.controlBtnGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox controlBtnGroupBox;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label staticTitleLabel;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button manageContactBtn;
        private System.Windows.Forms.Button productManageButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;

    }
}
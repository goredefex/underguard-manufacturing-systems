﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using UG_Business;

namespace WebService
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public List<Product> getAll()
        {
            CatalogPage page = new CatalogPage();
            List<CatalogPage> cp = page.GetAllPagesList();
            List<Product> foundProds = new List<Product>();
            
            for(int i=0; i<cp.Count; i++) {
                foundProds.Add(cp[i].Prod);
            }

            return foundProds;
        }
    }
}

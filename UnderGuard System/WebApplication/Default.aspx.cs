﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebService;

namespace WebApplication
{
    public partial class _Default : System.Web.UI.Page
    {
        //get infor here
        protected List<UG_Business.Product> list;
        protected void Page_Load(object sender, EventArgs e)
        {

            WebService.WebService webService = new WebService.WebService();
            Product.DataSource = webService.getAll();
            Product.DataBind();
            list = webService.getAll();
        }
    }
}
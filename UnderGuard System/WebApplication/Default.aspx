﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Catalog</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:Repeater ID="Product" runat="server">
            <HeaderTemplate>
                <table border="1">
                <tr>
                    <th>Product ID</th>
                    <th>Description</th>
                    <th>Safety Code</th>
                    <th>Name</th>
                    <th>Weight</th>
                    <th>Retail Price</th>
                    <th>Model Number</th>
                    <th>Color</th>
                    <th>Shipping Zones</th>
                    <th>Image</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# DataBinder.Eval(Container.DataItem, "Id")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Description")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "SafetyCode")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Name")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Weight")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Price")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "ModelNum")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Color")%></td>
                    <td><%# DataBinder.Eval(Container.DataItem, "Zones")%></td>                
                    <td><img style="max-width: 150px;" src="<%# DataBinder.Eval(Container.DataItem, "Img") %>" alt="<%# DataBinder.Eval(Container.DataItem, "Name") %>" /></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
